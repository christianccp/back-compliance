import http from 'http'
import { env, mongo, port, ip, apiRoot } from './config'
import mongoose from './services/mongoose'
import express from './services/express'
import api from './api'
import mail from './api/mail'
import { success } from './services/response';
import {init_socket} from './services/socket'

const app = express(apiRoot, api)
const server = http.createServer(app)


mongoose.connect(mongo.uri)
mongoose.Promise = Promise

setImmediate(() => {
  var serverSocket = server.listen(port, ip, () => {
    console.log('Express server listening on http://%s:%d, in %s mode', ip, port, env)
  })
  init_socket(serverSocket)
})

export default app
