import {sendLink, sendEMail, setBlockUser} from './controller'
import { Router } from 'express'
import { middleware as body } from 'bodymen'
let detail, toMail, name, subject, correo, link, recovery = false
const router = new Router()

/* router.post('/mail',function(request, response){

    mail.sendMail(request.body)
    .then(()=>{
        response.status(200).json({message: 'Mail sent'})
    })
    .catch((error) => {
        response.status(500).json({message: 'Mail not sent'})        
    })
    
}) */

router.post('/sendLink',
  body({ correo, link, recovery }),
  sendLink)

router.post('/setBlockUser',
  body({ correo, link }),
  setBlockUser)

router.post('/send/mail',
  body({ detail, toMail, name, subject }),
  sendEMail)

export default router
