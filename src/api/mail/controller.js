import { validationError, validationResponse } from '../../services/response/responsesModules'
import {setExpiredPass, setExpiredPassinYesterday} from '../user/controller'
import { User } from '../user'
import { notFound } from '../../services/response/'
import { URL_FRONT_DASH } from '../../variableProd'
var nodemailer = require('nodemailer');

let ENDPOINT = URL_FRONT_DASH + '/'

export const sendLink = async ({ bodymen: { body } }, res, next) => {
  try {
    let mailBody = null
    let mailSubject = null
    if (body.recovery === true) {
      // mailBody = `<p>Haz click en el siguiente link para terminar el proceso de recuperación de contraseña:</p><br><a href='${body.link}'>LINK</a>`
      mailBody = 
      `<p style="text-align:center;font-size:1.3em;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
      <strong><i style="color:#285ea6;">PAD Compliance</i></strong>
      </p>

      <img style="img-align:center;display:block;margin:auto;border-radius:50%;" width="35%" height="35%" src="https://pad1-auditoria.s3.us-east-2.amazonaws.com/recoverPass.jpg"></img>

      <p style="text-align:center;color:#0f5dac;font-size:15px;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
      Estamos por reestablecer su acceso al sistema PAD. 
      </p>

      <p style="text-align:center;font-size:15px;color:#0f5dac;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
      Presione el siguiente botón para terminar el proceso de recuperación de su contraseña.
      </p><br>
      <a href='${body.link}' style="box-shadow: 0px 10px 14px -7px #276873;background-color:#285ea6;border-radius:8px;
      display:block;cursor:pointer;color:#ffffff;font-family:Helvetica;font-weight:bold;padding:13px 32px;text-decoration:none;text-shadow:0px 1px 0px #3d768a;text-align:center;
      margin: 0 auto;width:200px;">
      Recuperar Contraseña
      </a>
      <br><br>
      <p style="color: #285ea6;"> <br> 
      PRYVACY LAWYERS S.A.S. DE C.V <br>
      Concepción Mendez #49,
      Col. Atenor Salas, Benito Juarez,
      03010, Ciudad de México, CDMX. <br>
      Contacto: 55-45-00-68-74
      </p>
      `
      mailSubject = 'Recuperación de contraseña'
    } else {
      // mailBody = `<p>Haz click en el siguiente link para verificar tu cuenta de correo:</p><br><a href='${body.link}'>LINK</a>`
      mailBody =
      `<p style="text-align:center;font-size:1.3em;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
      <strong> <i style="color:#285ea6;">Bienvenido a PAD Compliance </i></strong>
      </p>
      <img style="img-align:center;display:block;margin:auto;border-radius:50%;" width="50%" height="50%" src="https://pad1-auditoria.s3.us-east-2.amazonaws.com/accountVerification.jpg">
      <p style="text-align:center;color:#0f5dac;font-size:15px;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
      Estamos a punto de terminar tu acceso a PAD. 
      </p>
      <p style="text-align:center;font-size:15px;color:#0f5dac;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
      Sistema que te brindará servicios de "Compliance Legal" en protección de datos personales, prevención de lavado de
      dinero, subcontratación laboral y anticorrupción.
      </p>
      <p style="text-align:center;font-size:15px;color:#0f5dac;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
      Para completar tu registro presiona el siguiente botón para confirmar tu correo: 
      </p><br>
      <a href='${body.link}' style="box-shadow: 0px 10px 14px -7px #285ea6;background-color:#285ea6;border-radius:8px;
      display:block;cursor:pointer;color:#ffffff;font-family:Helvetica;font-weight:bold;padding:13px 32px;text-decoration:none;text-shadow:0px 1px 0px #3d768a;text-align:center;
      margin: 0 auto;width:200px;">
      Confirmar correo
      </a>
      <br><br>
      <p style="color: #285ea6;"> <br> 
      PRYVACY LAWYERS S.A.S. DE C.V <br>
      Concepción Mendez #49,
      Col. Atenor Salas, Benito Juarez,
      03010, Ciudad de México, CDMX. <br>
      Contacto: 55-45-00-68-74
      </p>`
      mailSubject = 'Verificación de cuenta'
    }
    var transporter = nodemailer.createTransport({
      host: 'smtp.hostinger.com',
      port: 465,
      secure: true,
      auth: {
        user: 'non-reply@padcompliance.mx',
        pass: 'ck307S@J'
      }
    })
    var mailOptions = {
      from: 'PAD <non-reply@padcompliance.mx>',
      to: body.correo,
      subject: mailSubject,
      text: 'Este correo fue enviado desde PAD de manerá automática',
      html: mailBody
    }
    
    let resSend = await transporter.sendMail(mailOptions)

    if (resSend.accepted.length > 0){
      setExpiredPass({email: body.correo}, res)
    }
  } catch (err) {
    validationError(err, res)
  }
}

export const setBlockUser = async ({ bodymen: { body } }, res, next) => {
  try {
    var transporter = nodemailer.createTransport({
      host: 'smtp.hostinger.com',
      port: 465,
      secure: true,
      auth: {
        user: 'non-reply@padcompliance.mx',
        pass: 'ck307S@J'
      }
    })
    // var codigohtml = "<p>La cuenta de correo :</p><br>"+body.correo+", ha sido bloqueada por pasar los 10 intentos de inicio de sesion. Continue en <a href="+body.link+">LINK</a> para iniciar el proceso de recuperación de contraseña"
    var codigohtml =
    ` <p style="text-align:center;font-size:1.3em;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
      <strong style="color:#285ea6;"><i>PAD Compliance </i></strong>
      </p>
      <img style="img-align:center;display:block;margin:auto;border-radius:50%;" width="50%" height="50%" src="https://pad1-auditoria.s3.us-east-2.amazonaws.com/blockedAccount.jpg">
      <p style="color: #285ea6; text-align:center; font-size:17px;">Cuenta Bloqueada</p>
      <br>
      <p style="text-align:center;color:#0f5dac;font-size:15px;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
      La cuenta de correo: ${body.correo}, ha sido bloqueada por pasar los 10 intentos de inicio de sesión.
      </p>
      <p style="text-align:center;font-size:15px;color:#0f5dac;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
      Presiona el botón de abajo para iniciar el proceso de recuperación de contraseña.
      </p>
      <a href='${body.link}' style="box-shadow: 0px 10px 14px -7px #276873;background-color:#285ea6;border-radius:8px;
      display:block;cursor:pointer;color:#ffffff;font-family:Helvetica;font-weight:bold;padding:13px 32px;text-decoration:none;text-shadow:0px 1px 0px #3d768a;text-align:center;
      margin: 0 auto;width:200px;">
      Recuperar Contraseña
      </a>
      <br><br>
      <p style="color: #285ea6;"> <br> 
      PRYVACY LAWYERS S.A.S. DE C.V <br>
      Concepción Mendez #49,
      Col. Atenor Salas, Benito Juarez,
      03010, Ciudad de México, CDMX. <br>
      Contacto: 55-45-00-68-74
      </p>`
    var mailOptions = {
      from: 'PAD <non-reply@padcompliance.mx>',
      to: body.correo,
      subject: 'Cuenta de correo Bloqueada ',
      text: 'Cuenta de correo bloqueada por intentos consecutivos',
      html: codigohtml
    }
    await setExpiredPassinYesterday({email: body.correo}, res)

    let resSend = await transporter.sendMail(mailOptions)
      let resultValidation = validationResponse(resSend, res)

      resultValidation ? res.json(resultValidation) : null
    
  } catch (err) {
    validationError(err, res)
  }
}

export const sendEMail = async ({ bodymen: { body } }, res, next) => {
  try {
    var transporter = nodemailer.createTransport({
      host: 'smtp.hostinger.com',
      port: 465,
      secure: true,
      auth: {
        user:'contacto@padcompliance.mx',
        pass:'Yv7bZYuE'
      }
    })
    
    var mailOptions = {
      from: 'PAD <contacto@padcompliance.mx>',
      to: 'contacto@padcompliance.mx',
      subject: body.subject,
      text: 'Usuario: ' + body.name + ' con mail: ' + body.toMail + ' pregunto: \n' + body.detail
    }

    let resSend = await transporter.sendMail(mailOptions)
    let resultValidation = validationResponse(resSend, res)

    resultValidation ? res.json(resultValidation) : null
  } catch (err) {
    validationError(err, res)
  }
}

export const sendMailBuy = async ({ bodymen: { body } }, res, next) => {
  try {
    var transporter = nodemailer.createTransport({
      host: 'smtp.hostinger.com',
      port: 465,
      secure: true,
      auth: {
        user: 'non-reply@padcompliance.mx',
        pass: 'ck307S@J'
      }
    })
    // var codigohtml = "<p>Gracias por confiar en PAD Compliance, su compra se ha realizado con éxito </p><br>"
    var codigohtml =
      `<p style="text-align:center;font-size:1.3em;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
      <strong style="color:#0f5dac;"><i>PAD Compliance </i></strong>
      </p>
      <img style="img-align:center;display:block;margin:auto;border-radius:50%;" width="50%" height="50%" src="https://pad1-auditoria.s3.us-east-2.amazonaws.com/successfulPurchase.jpg">
      <p style="text-align:center;font-size:1.3em; color:#285ea6;">
      <b>¡Bienvenido! </b>
      </p>
      <p style="text-align:center;color:#0f5dac;font-size:1em;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
      <strong>Gracias por confiar en PAD Compliance.</strong>
      </p>
      <p style="text-align:center;font-size:13px;color:#0f5dac;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
      <strong>Su compra se ha realizado con éxito</strong></p>
      </p>
      <p style="text-align:center;font-size:13px;color:#0f5dac;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
      <strong>Ahora puedes comenzar a disfrutar del sistema PAD desde el siguiente link</strong></p>
      </p>
      <a href='${ENDPOINT}' style="box-shadow: 0px 10px 14px -7px #276873;background-color:#285ea6;border-radius:8px;
      display:block;cursor:pointer;color:#ffffff;font-family:Helvetica;font-weight:bold;padding:13px 32px;text-decoration:none;text-shadow:0px 1px 0px #3d768a;text-align:center;
      margin: 0 auto;width:200px;">
      ¡Disfrutar de PAD!
      </a>
      <br><br>
      <p style="color: #285ea6;"> <br> 
      PRYVACY LAWYERS S.A.S. DE C.V <br>
      Concepción Mendez #49,
      Col. Atenor Salas, Benito Juarez,
      03010, Ciudad de México, CDMX. <br>
      Contacto: 55-45-00-68-74
      </p>`
    var mailOptions = {
      from: 'PAD <non-reply@padcompliance.mx>',
      to: body.correo,
      subject: 'Compra exitosa PAD Compliance',
      text: 'Gracias por confiar en PAD Compliance, su compra se ha realizado con éxito Gracias por confiar en PAD Compliance, su compra se ha realizado con éxito ',
      html: codigohtml
    }
    
    let resSend = await transporter.sendMail(mailOptions)
    if(res === null){
      if(resSend.accepted.length > 0){
        return 200
      }else{
        return 500
      }
    }
    let resultValidation = validationResponse(resSend, res)

    resultValidation ? res.json(resultValidation) : null
  } catch (err) {
    validationError(err, res)
  }
}
