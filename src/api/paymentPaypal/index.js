import { Router } from 'express'
import  paypalService  from '../../services/paypal'
import {create as apiAddLogPayments} from '../logPayments/index'
import { schema } from '../organization/model'
const { premium } = schema.tree
const router = new Router()

router.post('/checkoutPaypal',function(req, res)
{
      var execute_payment_json = {
        "payer_id": req.body.data.payerID,  
      };
      const payment ={}
      payment.amount=req.body.data.amount
      const paymentID=req.body.data.paymentID
      paypalService.paymentPaypal(paymentID,execute_payment_json,payment,(err,result)=>{
        let paymentMethod = 'Paypal'
        let sendEmail = req.body.email
        if(err) 
        {
          let httpStatusCode = err.httpStatusCode
          let description = err.error_description
          let error = err.error
          let status = "unsuccessful"
          let message = {
            "status" : status,
            "httpStatusCode" : httpStatusCode,
            "description" : description,
            "error" : error
          }
          res.json({message})
        }
        else
        {
          let httpStatusCode = result.httpStatusCode
          let description = result.state
          let error = "N/A"
          let status = "successful"
          let message = {
            "status" : status,
            "httpStatusCode" : httpStatusCode,
            "description" : description,
            "error" : error
          }
          res.json({message})
          apiAddLogPayments({ bodymen: { body: { type: paymentMethod, email: sendEmail, info: result, premium: req.body.premium } } }, null, null)
        }
      })
      
})

export default router