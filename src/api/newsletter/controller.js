import { success, notFound } from '../../services/response/'
import { Newsletter } from '.'
import { validationError, validationResponse } from '../../services/response/responsesModules'

export const create = async ({ bodymen: { body } }, res, next) => {
  try {
    let findM = await Newsletter.count({ mail: body.mail })
    if (findM == 0) {
      let resp = await Newsletter.create(body)
      let resultValidation = validationResponse(resp, res)
      resultValidation ? res.json(resultValidation.view()) : null
    } else {
      res.status(409).json('Conflict email registred')
    }
  } catch (err) {
    validationError(err, res)
  }
}

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Newsletter.count(query)
    .then(count => Newsletter.find(query, select, cursor)
      .then((newsletters) => ({
        count,
        rows: newsletters.map((newsletter) => newsletter.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Newsletter.findById(params.id)
    .then(notFound(res))
    .then((newsletter) => newsletter ? newsletter.view() : null)
    .then(success(res))
    .catch(next)

export const update = async ({ bodymen: { body }, params }, res, next) =>
  Newsletter.findById(params.id)
    .then(notFound(res))
    .then((newsletter) => newsletter ? Object.assign(newsletter, body).save() : null)
    .then((newsletter) => newsletter ? newsletter.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Newsletter.findById(params.id)
    .then(notFound(res))
    .then((newsletter) => newsletter ? newsletter.remove() : null)
    .then(success(res, 204))
    .catch(next)
