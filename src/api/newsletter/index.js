import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Newsletter, { schema } from './model'

const router = new Router()
const { mail } = schema.tree

/**
 * @api {post} /newsletters Create newsletter
 * @apiName CreateNewsletter
 * @apiGroup Newsletter
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam mail Newsletter's mail.
 * @apiSuccess {Object} newsletter Newsletter's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Newsletter not found.
 * @apiError 401 user access only.
 */
router.post('/',
  body({ mail }),
  create)

/**
 * @api {get} /newsletters Retrieve newsletters
 * @apiName RetrieveNewsletters
 * @apiGroup Newsletter
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of newsletters.
 * @apiSuccess {Object[]} rows List of newsletters.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /newsletters/:id Retrieve newsletter
 * @apiName RetrieveNewsletter
 * @apiGroup Newsletter
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} newsletter Newsletter's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Newsletter not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /newsletters/:id Update newsletter
 * @apiName UpdateNewsletter
 * @apiGroup Newsletter
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam mail Newsletter's mail.
 * @apiSuccess {Object} newsletter Newsletter's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Newsletter not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ mail }),
  update)

/**
 * @api {delete} /newsletters/:id Delete newsletter
 * @apiName DeleteNewsletter
 * @apiGroup Newsletter
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Newsletter not found.
 * @apiError 401 user access only.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
