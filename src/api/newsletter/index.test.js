import request from 'supertest'
import { apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Newsletter } from '.'

const app = () => express(apiRoot, routes)

let userSession, newsletter

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  userSession = signSync(user.id)
  newsletter = await Newsletter.create({})
})

test('POST /newsletters 201 (user)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession, mail: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.mail).toEqual('test')
})

test('POST /newsletters 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /newsletters 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /newsletters 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /newsletters/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${newsletter.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(newsletter.id)
})

test('GET /newsletters/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${newsletter.id}`)
  expect(status).toBe(401)
})

test('GET /newsletters/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /newsletters/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${newsletter.id}`)
    .send({ access_token: userSession, mail: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(newsletter.id)
  expect(body.mail).toEqual('test')
})

test('PUT /newsletters/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${newsletter.id}`)
  expect(status).toBe(401)
})

test('PUT /newsletters/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: userSession, mail: 'test' })
  expect(status).toBe(404)
})

test('DELETE /newsletters/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${newsletter.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /newsletters/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${newsletter.id}`)
  expect(status).toBe(401)
})

test('DELETE /newsletters/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})
