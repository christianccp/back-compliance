import mongoose, { Schema } from 'mongoose'

const newsletterSchema = new Schema({
  mail: {
    type: String,
    match: /^\S+@\S+\.\S+$/,
    required: true,
    unique: true,
    trim: true,
    lowercase: true
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

newsletterSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      mail: this.mail,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Newsletter', newsletterSchema)

export const schema = model.schema
export default model
