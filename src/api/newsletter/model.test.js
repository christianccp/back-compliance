import { Newsletter } from '.'

let newsletter

beforeEach(async () => {
  newsletter = await Newsletter.create({ mail: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = newsletter.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(newsletter.id)
    expect(view.mail).toBe(newsletter.mail)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = newsletter.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(newsletter.id)
    expect(view.mail).toBe(newsletter.mail)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
