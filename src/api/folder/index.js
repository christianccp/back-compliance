import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Folder, { schema } from './model'

const router = new Router()
const { id, name, organization_id } = schema.tree

/**
 * @api {post} /folders Create folder
 * @apiName CreateFolder
 * @apiGroup Folder
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam name Folder's name.
 * @apiParam organization_id Folder's organization_id.
 * @apiSuccess {Object} folder Folder's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Folder not found.
 * @apiError 401 user access only.
 */
router.post('/',
  token({ required: true }),
  body({ name, organization_id }),
  create)

/**
 * @api {get} /folders Retrieve folders
 * @apiName RetrieveFolders
 * @apiGroup Folder
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of folders.
 * @apiSuccess {Object[]} rows List of folders.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /folders/:id Retrieve folder
 * @apiName RetrieveFolder
 * @apiGroup Folder
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} folder Folder's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Folder not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /folders/:id Update folder
 * @apiName UpdateFolder
 * @apiGroup Folder
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam name Folder's name.
 * @apiParam organization_id Folder's organization_id.
 * @apiSuccess {Object} folder Folder's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Folder not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ name }),
  update)

/**
 * @api {delete} /folders/:id Delete folder
 * @apiName DeleteFolder
 * @apiGroup Folder
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Folder not found.
 * @apiError 401 user access only.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
