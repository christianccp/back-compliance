import { success, notFound } from '../../services/response/'
import { Folder } from '.'
import { validationError, validationResponse } from '../../services/response/responsesModules'
import { File } from '../../../src/api/file'

export const create = async ({ bodymen: { body }, user }, res, next) => {
  try {
    let createFolder = await Folder.create({ ...body, organization_id: user.organization_id })
    let response = validationResponse(createFolder, res)
    response ? res.json(response.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}

export const index = async ({ user }, res, next) => {
  try {
    let counter = await Folder.count({ organization_id: user.organization_id })
    if (counter > 0) {
      let folders = await Folder.find({ organization_id: user.organization_id })
      let response = validationResponse(folders, res)
      response ? res.json(response) : null
    } else {
      let response = validationResponse(null, res)
      response ? res.json(response) : null
    }
  } catch (err) {
    validationError(err, res)
  }
}

export const show = ({ params }, res, next) =>
  Folder.findById(params.id)
    .then(notFound(res))
    .then((folder) => folder ? folder.view() : null)
    .then(success(res))
    .catch(next)

export const update = async ({ bodymen: { body }, params }, res, next) => {
  try {
    let setter = { $set: {} }

    if (body.name != null) {
      setter.$set['name'] = body.name
    }
    if (body.organization_id != null) {
      setter.$set['organization_id'] = body.organization_id
    }
    let responseUpdate = await Folder.findOneAndUpdate({ _id: params.id }, setter, { new: true })
    let response = validationResponse(responseUpdate, res)
    response ? res.json(response.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}

export const destroy = async ({ params }, res, next) => {
  try {
    let counterF = await File.count({ folder: params.id })
    console.log(counterF)
    if (counterF == 0) {
      let resDelete = await Folder.findOneAndDelete({_id: params.id})
      let response = validationResponse(resDelete, res)
      response ? res.json(response.view()) : null
    } else {
      res.status(409).json('No se puede eliminar, el folder contiene al menos un archivo')
    }
  } catch (err) {
    validationError(err, res)
  }
}
