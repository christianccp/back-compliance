import request from 'supertest'
import { apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Folder } from '.'

const app = () => express(apiRoot, routes)

let userSession, folder

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  userSession = signSync(user.id)
  folder = await Folder.create({})
})

test('POST /folders 201 (user)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession, name: 'test', organization_id: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.name).toEqual('test')
  expect(body.organization_id).toEqual('test')
})

test('POST /folders 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /folders 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /folders 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /folders/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${folder.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(folder.id)
})

test('GET /folders/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${folder.id}`)
  expect(status).toBe(401)
})

test('GET /folders/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /folders/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${folder.id}`)
    .send({ access_token: userSession, name: 'test', organization_id: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(folder.id)
  expect(body.name).toEqual('test')
  expect(body.organization_id).toEqual('test')
})

test('PUT /folders/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${folder.id}`)
  expect(status).toBe(401)
})

test('PUT /folders/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: userSession, name: 'test', organization_id: 'test' })
  expect(status).toBe(404)
})

test('DELETE /folders/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${folder.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /folders/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${folder.id}`)
  expect(status).toBe(401)
})

test('DELETE /folders/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})
