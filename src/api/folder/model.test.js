import { Folder } from '.'

let folder

beforeEach(async () => {
  folder = await Folder.create({ name: 'test', organization_id: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = folder.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(folder.id)
    expect(view.name).toBe(folder.name)
    expect(view.organization_id).toBe(folder.organization_id)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = folder.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(folder.id)
    expect(view.name).toBe(folder.name)
    expect(view.organization_id).toBe(folder.organization_id)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
