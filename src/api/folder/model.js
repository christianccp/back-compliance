import mongoose, { Schema } from 'mongoose'

const folderSchema = new Schema({
  name: {
    type: String
  },
  organization_id: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

folderSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      name: this.name,
      organization_id: this.organization_id,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Folder', folderSchema)

export const schema = model.schema
export default model
