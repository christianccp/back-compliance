import { accessTokenMercadoPagoProduction, accessTokenMercadoPagoSandbox } from '../../config'
import {create as apiAddLogPayments} from '../logPayments/index'
import { Facturation } from '../facturation'
import { validationError, validationResponse } from '../../services/response/responsesModules'
import { URL_FRONT_DASH, URL_FRONT_CLIENT, URL_BACK } from '../../variableProd'
var nodemailer = require('nodemailer')
let mercadopago = require('mercadopago')
let endpointBackClient = URL_FRONT_CLIENT + '/PaymentMethods'
let accessTokenMercadoPago = null
if (process.env.NODE_ENV === 'production') {
  accessTokenMercadoPago = accessTokenMercadoPagoProduction
} else {
  accessTokenMercadoPago = accessTokenMercadoPagoSandbox
}
const urlPayment = URL_FRONT_DASH + '/login'
// mercadopago.configurations.setAccessToken(accessTokenMercadoPago)
export const paymentMercado = async ({ querymen: { query } }, res, next) => {
  try {
    var i
    let arrayfields = []
    let arrayfields2 = []
    let premiumObj = {}
    var fecha
    // console.log(('2020-01-01').indexOf('-', 0))
    let arraypremium = query.premium2.split(',')
    for (i = 0; i < (arraypremium.length); i += 2) {
      arrayfields.push([arraypremium[i] + ',' + arraypremium[i + 1]])
    }
    arrayfields.forEach(subarray => {
      var div = subarray[0].split(',')
      let field = div[0]
      // console.log(div[1])
      if ((div[1]).indexOf('-', 0) != -1) {
        fecha = new Date(div[1])
        // console.log(fecha)
        let value = fecha
        var objetField = { [ field ]: value }
        arrayfields2.push(objetField)
      } else {
        let value = JSON.parse(div[1])
        var objetField2 = { [ field ]: value }
        arrayfields2.push(objetField2)
      }
    })
    arrayfields2.forEach(subobjs => {
      Object.assign(premiumObj, subobjs)
    })
   // console.log(arrayfields2.splice(0, arrayfields2.length))
    if (query.rfc != '') {
      let result, result2
      result = await apiAddLogPayments({ bodymen: { body: {type: 'mercadoPago', email: query.email, info: {id: 47472783}, premium: premiumObj} } }, null, null)
      let createFacturation = await Facturation.create({nombre: query.name, correo: query.email, calle: query.calle, razon: query.razon, uso: query.tipoUso, int: query.num, ext: query.ext, cp: query.cp, estado: query.estado, municipio: query.municipio, rfc: query.rfc})
      res.redirect(urlPayment)
    } else {
      let result
      result = await apiAddLogPayments({ bodymen: { body: {type: 'mercadoPago', email: query.email, info: {id: 47472783}, premium: premiumObj} } }, null, null)
      res.redirect(urlPayment)
    }
  } catch (err) {
    // console.log(err)
  }
}

export const configMP = async ({ bodymen: { body } }, res, next) => {
  console.log('BODY: ', body)
  let array = []
  array.push(body.premium)
  mercadopago.configure({
    access_token: accessTokenMercadoPago
  })
  if (body.rfc != undefined) {
  // Crea un objeto de preferencia
    let preference = {
      items: [
        {
          title: 'Compra Módulo PAD',
          unit_price: parseFloat(body.price),
          quantity: 1
        }
      ],
      // notification_url: URL_BACK + '/mercadopagos/notificationV2/ipn',
      back_urls: {
        success: URL_BACK + '/mercadopagos/mercado/pago/paymen?email=' + body.email + '&name=' + body.name + '&razon=' + body.razon + '&tipoUso=' + body.tipoUso + '&calle=' + body.calle + '&num=' + body.num + '&ext=' + body.ext + '&cp=' + body.cp + '&estado=' + body.estado + '&municipio=' + body.municipio + '&rfc=' + body.rfc + '&premium2=' + Object.entries(body.premium),
        pending: URL_BACK + '/mercadopagos/mercado/pago/paymen?email=' + body.email + '&name=' + body.name + '&razon=' + body.razon + '&tipoUso=' + body.tipoUso + '&calle=' + body.calle + '&num=' + body.num + '&ext=' + body.ext + '&cp=' + body.cp + '&estado=' + body.estado + '&municipio=' + body.municipio + '&rfc=' + body.rfc + '&premium2=' + Object.entries(body.premium),
        failure: endpointBackClient
      },
      auto_return: 'approved'
    }
    mercadopago.preferences.create(preference)
      .then(function (response) {
        res.json(response)
        console.log('INIT', response.body.init_point)
        // Este valor reemplazará el string "$$init_point$$" en tu HTML
        global.init_point = response.body.init_point
      }).catch(function (error) {
        console.log(JSON.stringify(error, null, 2))
      })
  } else {
    let preference = {
      items: [
        {
          title: 'Compra Módulo PAD',
          unit_price: parseInt(body.price),
          quantity: 1
        }
      ],
      // notification_url: URL_BACK + '/mercadopagos/notificationV2/ipn',
      back_urls: {
        success: URL_BACK + '/mercadopagos/mercado/pago/paymen?email=' + body.email,
        pending: 'https://www.mercadolibre.com.mx/',
        failure: endpointBackClient
      },
      auto_return: 'approved'
    }
    mercadopago.preferences.create(preference)
      .then(function (response) {
        res.json(response)
        console.log('INIT', response.body.init_point)
        // Este valor reemplazará el string "$$init_point$$" en tu HTML
        global.init_point = response.body.init_point
      }).catch(function (error) {
        console.log(JSON.stringify(error, null, 2))
      })
  }
}

export const notifications = async ({ bodymen: { body } }, res, next) => {
  try {
    if (body.topic != null && body.id != null) {
      var transporter = nodemailer.createTransport({
        host: 'smtp.hostinger.com',
        port: 465,
        secure: true,
        auth: {
          user: 'non-reply@padcompliance.mx',
          pass: 'ck307S@J'
        }
      })
      var codigohtml = "<p>Gracias por confiar en PAD Compliance, su compra se ha realizado con éxito </p><br>"
      var mailOptions = {
        from: 'PAD <non-reply@padcompliance.mx>',
        to: 'christian.93.ccp@gmail.com',
        subject: 'Prueba IPN',
        text: 'EMAIL Prueba de IPN ',
        html: codigohtml
      }
      
      let resSend = await transporter.sendMail(mailOptions)
      if (res === null) {
        if (resSend.accepted.length > 0) {
          return 200
        } else {
          return 500
        }
      }
      let resultValidation = validationResponse(resSend, res)

      resultValidation ? res.json(resultValidation) : null
    } else {
      res.status(400).send('Something broke!')
    }
  } catch (err) {
    validationError(err, res)
  }
}
