import { Router } from 'express'
import { destroy } from './controller'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { paymentMercado, configMP, notifications } from './controller'
import { schema } from '../organization/model'
let mercadopago = require('mercadopago')
const router = new Router()
var res, razon, tipoUso, calle, num, ext, cp, estado, municipio, rfc, email, nombre, token, payment_method_id, installments, issuer_id, name, monto, mail, title, price, unit, payment_status, topic, id
const { premium } = schema.tree
var premium2
/**
 * @api {delete} /deleteDomains/:id Delete delete domain
 * @apiName DeleteDeleteDomain
 * @apiGroup DeleteDomain
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Delete domain not found.
 */
// router.delete('/:domain',
// destroy)
router.get('/mercado/pago/paymen',
  query({razon, tipoUso, name, email, calle, num, ext, cp, estado, municipio, rfc, premium2}),
  paymentMercado)

router.post('/config/mp',
  body({ razon, tipoUso, price, name, email, calle, num, ext, cp, estado, municipio, rfc, premium }),
  configMP)

router.post('/notification',
  query({ email, nombre }),
  notifications)

router.get('/notificationV2/ipn', function (req, res, next) {
  res.status(200)
  console.log('200.OK: ' + res)
  res.end()
})

export default router
