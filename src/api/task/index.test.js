import request from 'supertest'
import { apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Task } from '.'

const app = () => express(apiRoot, routes)

let userSession, task

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  userSession = signSync(user.id)
  task = await Task.create({})
})

test('POST /tasks 201 (user)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession, text: 'test', start_date: 'test', end_date: 'test', progress: 'test', assigned: 'test', comments: 'test', color: 'test', organization_id: 'test', moduleId: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.text).toEqual('test')
  expect(body.start_date).toEqual('test')
  expect(body.end_date).toEqual('test')
  expect(body.progress).toEqual('test')
  expect(body.assigned).toEqual('test')
  expect(body.comments).toEqual('test')
  expect(body.color).toEqual('test')
  expect(body.organization_id).toEqual('test')
  expect(body.moduleId).toEqual('test')
})

test('POST /tasks 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /tasks 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /tasks 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /tasks/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${task.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(task.id)
})

test('GET /tasks/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${task.id}`)
  expect(status).toBe(401)
})

test('GET /tasks/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /tasks/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${task.id}`)
    .send({ access_token: userSession, text: 'test', start_date: 'test', end_date: 'test', progress: 'test', assigned: 'test', comments: 'test', color: 'test', organization_id: 'test', moduleId: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(task.id)
  expect(body.text).toEqual('test')
  expect(body.start_date).toEqual('test')
  expect(body.end_date).toEqual('test')
  expect(body.progress).toEqual('test')
  expect(body.assigned).toEqual('test')
  expect(body.comments).toEqual('test')
  expect(body.color).toEqual('test')
  expect(body.organization_id).toEqual('test')
  expect(body.moduleId).toEqual('test')
})

test('PUT /tasks/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${task.id}`)
  expect(status).toBe(401)
})

test('PUT /tasks/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: userSession, text: 'test', start_date: 'test', end_date: 'test', progress: 'test', assigned: 'test', comments: 'test', color: 'test', organization_id: 'test', moduleId: 'test' })
  expect(status).toBe(404)
})

test('DELETE /tasks/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${task.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /tasks/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${task.id}`)
  expect(status).toBe(401)
})

test('DELETE /tasks/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})
