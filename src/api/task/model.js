import mongoose, { Schema } from 'mongoose'

const filesSchema = new Schema({
  fileName: {
    type: String
  },
  fileUrl: {
    type: String
  },
  size: {
    type: String
  },
  dateUpload: {
    type: Date,
    default: Date.now()
  },
  dateUpdate: {
    type: Date,
    default: Date.now()
  },
  origin: {
    type: String,
    lowercase: true
  },
  key: {
    type: String
  }
})
const auditSchema = new Schema({
  text: {
    type: String
  },
  assigned: {
    type: String
  },
  satisfy: {
    type: String
  },
  observations: {
    type: String
  },
  responsableArea: {
    type: String
  }
})

const dataSchema = new Schema({
  _id: false,
  id: {
    type: Number
  },
  typeOf: {
    type: String
  },
  text: {
    type: String
  },
  start_date: {
    type: Date,
    default: function() {
      if (this.type === 'task') {
        return Date.now()
      }
      return undefined
    },
  },
  end_date: {
    type: Date,
    default: function() {
      if (this.type === 'task') {
        return new Date(+new Date() + 1*24*60*60*1000)
      } 
      return undefined
    },
  },
  progress: {
    type: Number,
    default:0
  },
  priority: {
    type: Number,
    default:0
  },
  assigned: {
    type: String,
    default: ''
  },
  comments: {
    type: String,
    default:''
  },
  color: {
    type: String,
    default:'#0098D5'  
  },
  open: {
    type: Boolean,
    default: true  
  },
  type: {
    type: String,
    default: 'task'  
  },
  parent: {
    type: Number
  },
  linkedSubrisks: {
    type: Array
  },
  newFields: {
    type: Array
  },
  responsibleArea: {
    type: String,
    default: ' '
  },
  controlFrecuency: {
    type: String,
    default: ' '
  },
  evaluationResult: {
    type: String,
    default: ' '
  },
  measureIndicator: {
    type: String,
    default: ' '
  },
  originType: {
    type: String,
    default: ' '
  },
  processOwner: {
    type: String,
    default: ' '
  },
  proceder: {
    type: String,
    default: ' '
  },
  filesIdDocument: {
    type: [Schema.Types.ObjectId],
    ref: 'File'
  },
  next_day_monitory: {
    type: Date
  },
  audit: {
    type: {auditSchema}
  },
  stepAudit:{
    type: Number,
    default: 1
  },
  observations: {
    type: String
  },
  satisfy: {
    type: String
  }
})

const linksSchema = new Schema({
  _id: false,
  id: {
    type: Number,
  },
  source: {
    type: Number
  },
  target: {
    type: Number
  },
  type: {
    type: Number
  }
})

const taskSchema = new Schema({
  tasks: {
    data: {
      type: [dataSchema]
    },
    links: {
      type: [linksSchema]
    }
  },
  organization_id: {
    type: Schema.Types.ObjectId,
    ref: 'Organization'
  },
  moduleId: {
    type: String,
    required: true
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

taskSchema.post('findOne', function (result) {
  if (result != null) { 
    result.tasks.data.forEach(element => {
      if (element.type !== 'task'){
        element.start_date = undefined
        element.end_date = undefined
      }
    })
  }
})

taskSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this._id,
      organization_id: this.organization_id,
      tasks: {
        data: this.tasks.data,
        links: this.tasks.links
      },
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view,
      // add properties for a full view
      id: this.tasks.data._id
    } : view
  },
  viewData () {
    const view = {
      data: this.tasks.data
    }
    return view
  }
}

const model = mongoose.model('Task', taskSchema)

export const schema = model.schema
export default model
