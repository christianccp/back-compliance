import { success, notFound } from '../../services/response/'
import { validationError, validationResponse } from '../../services/response/responsesModules'
import { Task } from '.'
import { File } from '../../../src/api/file'
import { secretAccessKey, accessKeyId } from '../../config'
import cron from 'node-cron'
import { Notifications } from '../../../src/api/notifications'
import { User } from '../../../src/api/user'
const multer = require('multer')
const multerS3 = require('multer-s3')
const aws = require('aws-sdk')

aws.config.update({
  secretAccessKey: secretAccessKey,
  accessKeyId: accessKeyId,
  region: 'us-east-1'
})

cron.schedule('0 0 1 * * *', async () => {
  Task.find({})
  .then(tasks =>{
    let myDateToday = new Date()
    myDateToday.setHours(0,0,0,500)
    tasks.map(task =>{
      task.tasks.data.map(myTask => {
        if(myTask.next_day_monitory !== null && myTask.next_day_monitory !== undefined && myTask.next_day_monitory !== ' ' && myTask.next_day_monitory !== ''){
          let myDateTask = new Date(myTask.next_day_monitory)
          myDateTask.setMilliseconds(0)
          if(myDateTask < myDateToday){
            Notifications.create({title: 'Monitorear tarea' , bodyN: `La tarea ${myTask.text} debe ser monitoreada`, link: 'Monitoreo', organization_id: task.organization_id, target: [myTask.assigned], 'typeN': 'checkTask'})
            .then(()=>{})
            .catch(()=>{})  
          }
        }
      })
    })
  })
})

// Hubo cambios. OK!
export const create =  async ({ bodymen: { body }, user, params }, res, next) => {  
  try {
    // find document by organization_id, If already exist we update all the document 
    let responseInsert = await Task.findOneAndUpdate(
      {'organization_id': user.organization_id, moduleId: body.moduleId },
      {tasks: body.tasks},
      {new: true, upsert: true}
    )

    let response = validationResponse(responseInsert, res)

    response ? res.json(response.view()) : null

  } catch (err) {
    validationError(err, res)
  }
}

// Hubo cambios. OK!
export const addData = async ({ bodymen: { body }, user }, res, next) => {
  try {
    let lengthArr = 0
    let cont = 0
    let arraynf = []
    let findData = await Task.findOne({ organization_id: user.organization_id, moduleId: body.moduleId })

    if (findData.tasks.data.length != 0) {
      findData.tasks.data.forEach(task => {
        if (task.newFields.length != 0 && task.newFields.length > lengthArr) {
          lengthArr = task.newFields.length
          if (arraynf.length != 0) {
            arraynf.pop()
            arraynf.push(task.newFields)
          } else {
            arraynf.push(task.newFields)
          }
        }
        console.log('Contador: ', cont++, task.newFields)
      })
      console.log(arraynf)
      body.data.newFields = arraynf.pop()
      let taskDoc = await Task.findOneAndUpdate({organization_id: user.organization_id, moduleId: body.moduleId},
        { $push: { 'tasks.data': body.data, $slice: -1 } },
        { new: true, upsert: true }
      )

      let resultValidation = validationResponse(taskDoc, res)

      resultValidation ? res.json(resultValidation.view()) : null    
    } else {
      let taskDoc = await Task.findOneAndUpdate({organization_id: user.organization_id, moduleId: body.moduleId},
        { $push: { 'tasks.data': body.data, $slice: -1 } },
        { new: true, upsert: true }
      )
      let resultValidation = validationResponse(taskDoc, res)

      resultValidation ? res.json(resultValidation.view()) : null
    }
  } catch (err) {
    validationError(err, res)
  }
}

// Hubo cambios. OK!
export const updateData = async ({ bodymen: { body }, user , params }, res, next) => {
  try {
    
    let setter = { $set: {} }

    if (body.data.text != null) {
      setter.$set['tasks.data.$.text'] = body.data.text
    }
    if (body.data.start_date != null) {
      setter.$set['tasks.data.$.start_date'] = body.data.start_date
    }
    if (body.data.end_date != null) {
      setter.$set['tasks.data.$.end_date'] = body.data.end_date
    }
    if (body.data.progress != null) {
      setter.$set['tasks.data.$.progress'] = body.data.progress
    }
    if (body.data.priority != null) {
      setter.$set['tasks.data.$.priority'] = body.data.priority
    }
    if (body.data.assigned != null) {
      setter.$set['tasks.data.$.assigned'] = body.data.assigned
    }
    if (body.data.comments != null) {
      setter.$set['tasks.data.$.comments'] = body.data.comments
    }
    if (body.data.color != null) {
      setter.$set['tasks.data.$.color'] = body.data.color
    }
    if (body.data.parent != null) {
      setter.$set['tasks.data.$.parent'] = body.data.parent
    }
    if (body.data.type != null) {
      setter.$set['tasks.data.$.type'] = body.data.type
    }

    if (body.data.responsibleArea != null) {
      setter.$set['tasks.data.$.responsibleArea'] = body.data.responsibleArea
    }
    if (body.data.controlFrecuency != null) {
      setter.$set['tasks.data.$.controlFrecuency'] = body.data.controlFrecuency
    }
    if (body.data.evaluationResult != null) {
      setter.$set['tasks.data.$.evaluationResult'] = body.data.evaluationResult
    }
    if (body.data.measureIndicator != null) {
      setter.$set['tasks.data.$.measureIndicator'] = body.data.measureIndicator
    }
    if (body.data.originType != null) {
      setter.$set['tasks.data.$.originType'] = body.data.originType
    }
    if (body.data.processOwner != null) {
      setter.$set['tasks.data.$.processOwner'] = body.data.processOwner
    }
    if (body.data.proceder != null) {
      setter.$set['tasks.data.$.proceder'] = body.data.proceder
    }
    if (body.data.next_day_monitory != null) {
      setter.$set['tasks.data.$.next_day_monitory'] = body.data.next_day_monitory
    }
    if (body.data.stepAudit != null) {
      setter.$set['tasks.data.$.stepAudit'] = body.data.stepAudit
    }


    let taskDoc = await Task.findOneAndUpdate({'organization_id': user.organization_id, moduleId: body.moduleId, 'tasks.data.id': params.idTask},
      setter,
      { new: true }
    )
    let resultValidation = validationResponse(taskDoc, res)

    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}


//  OK!
export const deleteData = async ({ bodymen: { body }, user, params }, res, next) => {
  try {
    let taskDoc = await Task.findOneAndUpdate({organization_id: user.organization_id, moduleId: body.moduleId, 'tasks.data.id': params.id}, { "$pull": { "tasks.data": { "id": params.id } }}, { safe: true, multi: true, new: true })
    let resultValidation = validationResponse(taskDoc, res)

    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}

// OK!
export const getTasks = async ({ querymen: { query }, user }, res, next) => {
  query.organization_id = user.organization_id
  try {
    let task = await Task.findOne(query).populate({path: 'tasks.data.filesIdDocument', model: 'File'})
    //let popu = await File.populate('tasks.data.filesIdDocument')
    console.log(task)
    let response = validationResponse(task, res)
    response ? res.json(response.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}

//  OK!
export const addLinks = async ({ bodymen: { body }, user, params }, res, next) => {
  try {
    let taskDoc = await Task.findOneAndUpdate(
      {organization_id: user.organization_id, moduleId: body.moduleId},
      { '$push': { 'tasks.links': body.ganttLinksParams } },
      { new: true }
    )
    let resultValidation = validationResponse(taskDoc, res)

    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}

export const updateLinks = async ({ bodymen: { body },user, params }, res, next) => {
  try {
    let taskDoc = await Task.findOneAndUpdate(
      { organization_id: user.organization_id, moduleId: body.moduleId, 'tasks.links.id': params.id },
      { $set:
        {
          'tasks.links.$.source': body.source,
          'tasks.links.$.target': body.target,
          'tasks.links.$.type': body.type
        }
      },
      {new: true}
    )

    let resultValidation = validationResponse(taskDoc, res)

    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}

// OK!
export const deleteLinks = async ({ bodymen: { body }, user, params }, res, next) => {
  try {
    let taskDoc = await Task.findOneAndUpdate(
      { organization_id: user.organization_id, moduleId: body.moduleId, 'tasks.links.id': params.id },
      { '$pull': { 'tasks.links': { 'id': params.id } } },
      { safe: true, new: true }
    )

    let resultValidation = validationResponse(taskDoc, res)

    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}

  
export const destroy = ({ params }, res, next) =>
  Task.findById(params.id)
    .then(notFound(res))
    .then((task) => task ? task.remove() : null)
    .then(success(res, 204))
    .catch(next)

export const createAudit = async ({ bodymen: { body }, params, user }, res, next) => {
  try {
    let setter = { $set: {} }
    let setterExtra = { $set: {} }
    if (body.assigned != null) {
      setter.$set['tasks.data.$.assigned'] = body.assigned
    }
    if (body.satisfy != null) {
      setter.$set['tasks.data.$.satisfy'] = body.satisfy
    }
    if (body.observations != null) {
      setter.$set['tasks.data.$.observations'] = body.observations
    }
    if (body.processOwner != null) {
      setter.$set['tasks.data.$.processOwner'] = body.processOwner
    }
    if (body.stepAudit != null) {
      if(body.stepAudit < 3){
        body.stepAudit++
      }
      setter.$set['tasks.data.$.stepAudit'] = body.stepAudit 
    }
    if (body.infoFieldsExtras != null) {
      setterExtra.$set['tasks.data.$.newfields'] = body.infoFieldsExtras
    }
    let resAudit = await Task.findOneAndUpdate({'organization_id': user.organization_id, moduleId: body.moduleId, 'tasks.data.id': params.id},
      setter,
      {new: true})

    let infoNFields = await Task.findOneAndUpdate({'organization_id': user.organization_id, moduleId: body.moduleId, 'tasks.data.id': params.id},
      setterExtra,
      {new: true, strict: true})

    let resultValidation = validationResponse(infoNFields, res)

    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}

const s3 = new aws.S3()
export const subirArchivo = multer({
  storage: multerS3({
    s3: s3,
    bucket: 'pda-auditoria',
    acl: 'public-read',
    contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata: function (req, file, cb) {
      cb(null, {fieldName: file.originalname, contentType: file.mimetype})
    },
    key: function (req, file, cb) {
      cb(null, Date.now().toString())
    }
  })
})

export const deleteFiles = ({ bodymen: { body }, params, user }, res, next) => {
  var s3 = new aws.S3()
  var param = { Bucket: 'pda-auditoria', Key: body.keyFile }
  s3.deleteObject(param, function (err, data) {
    if (err) {
      res.status(409).json(err.stack)
    } else {
      File.findOneAndDelete({key: body.keyFile})
        .then((resp) => {
          res.status(204).json('Bien')
        })
    }
  })
}

export const deleteVersionFile = ({ bodymen: { body }, params, user }, res, next) => {
  var s3 = new aws.S3()
  var param = {
    Bucket: 'pda-auditoria',
    Key: body.keyFile,
    VersionId: body.versionId
  }
  s3.deleteObject(param, function (err, data) {
    if (err) {
      res.status(409).json(err.stack)
    } else {
      File.findOneAndDelete({key: body.keyFile})
        .then((resp) => {
          res.status(204).json('Bien')
        })
    }
  })
}

export const saveDataFiles = async (array, taskId, OrgId, origin, moduleId, userId, folderName, res, next) => {
  try {
    let data = []
    array.map(elem => {
      data.push({'fileName': elem.originalname,
        'fileUrl': elem.location,
        'size': elem.size,
        'origin': origin,
        'key': elem.key,
        'version': [{
          'versionNumber': 1,
          'versionId': elem.versionId
        }],
        'task_id': taskId,
        'organization_id': OrgId,
        'moduleId': moduleId,
        'logUsersAccess': [{
          'userId': userId,
          'activity': 'add'
        }],
        'status': 'uploaded',
        'folder': folderName
      })
    })

    data.forEach(async item => {
      let docFiles = await File.create(item)
      console.log(docFiles)
    })
  } catch (err) {
    console.log(err)
  }
}

export const savedFiles = ({ bodymen: { body }, user, params }, res, next) => {
  var arrfiles = []
  Task.findOne({ 'organization_id': user.organization_id, moduleId: body.moduleId })
    .then((resp) => {
      resp.tasks.data.map(element1 => {
        if (element1.files != 0) {
          element1.files.forEach(element2 => {
            if (element2.origin == body.origin.toLowerCase()) {
              var idT = {'id': element1.id}
              arrfiles.push(idT, element2)
            }
          })
        }
      })
      res.status(200).json(arrfiles)
    })
    .catch(next)
}

export const getFile = async ({bodymen: { body }, params}, res, next) => {
  var s3 = new aws.S3()
  var param = {
    Bucket: 'pda-auditoria',
    Key: params.id,
    VersionId: body.versionId
  }
  s3.getObject(param)
    .on('httpHeaders', function (statusCode, headers) {
      res.set('Content-Length', headers['content-length'])
      res.set('Content-Type', headers['x-amz-meta-contenttype'])
      res.attachment(headers['x-amz-meta-fieldname'])
      this.response.httpResponse.createUnbufferedStream()
        .pipe(res)
    })
    .send()
}

export const addColumn = async ({ bodymen: { body }, user, params }, res, next) => {
  try {
    console.log(body.fieldName)
    var fName = body.fieldName
    var newF = { [fName]: 'default' } // brackets allows to assingne a name dynamically to a flied in a javascript object
    let verifData = await Task.findOne({ organization_id: user.organization_id, moduleId: body.moduleId })
    let tsks = verifData.tasks.data.length
    console.log(tsks)
    if (tsks != 0) {
      let taskDoc = await Task.findOneAndUpdate(
        { organization_id: user.organization_id, moduleId: body.moduleId },
        { $push: {'tasks.data.$[].newFields': newF} },
        {new: true, multi: true, strict: false}
      )
      let resultValidation = validationResponse(taskDoc, res)
      resultValidation ? res.json(resultValidation.view()) : null
    } else {
      res.status(409).json('Debe existir al menos una tarea')
    }
  } catch (err) {
    validationError(err, res)
  }
}

export const deleteColumn = async ({ bodymen: { body }, user }, res, next) => {
  try {
    console.log(body.fieldName)
    var fName = body.fieldName
   // var newF = { [fName]: 'default' } // brackets allows to assingne a name dynamically to a flied in a javascript object

    let taskDoc = await Task.findOneAndUpdate(
      { organization_id: user.organization_id, moduleId: body.moduleId },
      { $pull: {'tasks.data.$[].newFields': { [fName]: { $exists: true } }} },
      {new: true, multi: true}
    )
    let resultValidation = validationResponse(taskDoc, res)

    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }  
}

export const uploadVersionFile = function (req, res, next) {
  console.log(req.file.mimetype)
  var s3 = new aws.S3()
  var params = {
    Bucket: 'pda-auditoria',
    ACL: 'public-read',
    Body: req.file.buffer,
    Key: req.body.key_file,
    ContentType: req.file.mimetype,
    Metadata: {
      contenttype: req.file.mimetype,
      fieldname: req.file.originalname
    }
  }
  s3.putObject(params, function (err, data) {
    if (err) console.log(err, err.stack) // an error occurred
    else     console.log(data)
  })
}

export const subirA = function (req) {
  console.log(req.file.mimetype)
  var s3 = new aws.S3()
  var params = {
    Bucket: 'pda-auditoria',
    ACL: 'public-read',
    Body: req.file.buffer,
    Key: req.body.key_file,
    ContentType: req.file.mimetype,
    Metadata: {
      'x-amz-meta-contenttype': req.file.mimetype,
      'x-amz-meta-fieldname': req.file.originalname
    }
  }
  s3.putObject(params, function (err, data) {
    if (err) console.log(err, err.stack) // an error occurred
    else     console.log(data)
  })
}