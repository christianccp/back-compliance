import { Task } from '.'

let task

beforeEach(async () => {
  task = await Task.create({ text: 'test', start_date: 'test', end_date: 'test', progress: 'test', assigned: 'test', comments: 'test', color: 'test', organization_id: 'test', moduleId: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = task.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(task.id)
    expect(view.text).toBe(task.text)
    expect(view.start_date).toBe(task.start_date)
    expect(view.end_date).toBe(task.end_date)
    expect(view.progress).toBe(task.progress)
    expect(view.assigned).toBe(task.assigned)
    expect(view.comments).toBe(task.comments)
    expect(view.color).toBe(task.color)
    expect(view.organization_id).toBe(task.organization_id)
    expect(view.moduleId).toBe(task.moduleId)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = task.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(task.id)
    expect(view.text).toBe(task.text)
    expect(view.start_date).toBe(task.start_date)
    expect(view.end_date).toBe(task.end_date)
    expect(view.progress).toBe(task.progress)
    expect(view.assigned).toBe(task.assigned)
    expect(view.comments).toBe(task.comments)
    expect(view.color).toBe(task.color)
    expect(view.organization_id).toBe(task.organization_id)
    expect(view.moduleId).toBe(task.moduleId)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
