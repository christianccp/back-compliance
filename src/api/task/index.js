import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import mongoose from 'mongoose'
import { token } from '../../services/passport'
import { subirA, deleteVersionFile, uploadVersionFile, deleteColumn, addColumn, getFile, create, destroy, addData, updateData, addLinks, deleteData, getTasks, updateLinks, deleteLinks, getLinks, updateMonitorInfo, subirArchivo, createAudit, saveDataFiles, savedFiles, getTask, deleteFiles } from './controller'
import { schema } from './model'
import { Task } from '.'
export Task, { schema } from './model'

let bodyParser = require('body-parser')
const multer = require('multer')
const upload = multer()

const router = new Router()
const { stepAudit, versionId, fieldName, processOwner, originType, next_day_monitory, key, id, origin, keyFile, idFile, typeOf, text, start_date, end_date, progress, priority, assigned, comments, color, moduleId, parent, open, data, links, source, satisfy, observations, target, type, responsibleArea, controlFrecuency, evaluationResult, measureIndicator,proceder } = schema.tree

/**
 * @api {get} /TODO Para documentar
 */
router.get('/get/tasks',
  token({ required: true }),
  query({
    moduleId
  }),
  getTasks)



//TODO: Actualizar documentación
/**
* @api {get} 
 */
router.post('/',
  token({ required: true }),
  body({ moduleId, typeOf, tasks: { data, links } }),
  create)

//TODO: Actualizar documentación
/** 
 * @api {put} /tasks/:id/addData Update Data task
 * @apiName Add Data To Task
 * @apiGroup Task
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam text Task's text.
 * @apiParam start_date Task's start_date.
 * @apiParam end_date Task's end_date.
 * @apiParam progress Task's progress.
 * @apiParam assigned Task's assigned.
 * @apiParam comments Task's comments.
 * @apiParam color Task's color.
 * @apiParam organization_id Task's organization_id.
 * @apiParam moduleId Task's moduleId.
 * @apiSuccess {Object} task Task's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Task not found.
 * @apiError 401 user access only.
 */
router.put('/addData',
token({ required: true }),
body({ moduleId, data: {id, type, open, text, start_date, end_date, progress, priority, assigned, comments, color, parent, processOwner, originType, stepAudit}}),
addData)


//TODO: Actualizar documentación
/**
 * @api {put} /tasks/:id/updateData Update Data task
 * @apiName Update Data
 * @apiGroup Task
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam text Task's text.
 * @apiParam start_date Task's start_date.
 * @apiParam end_date Task's end_date.
 * @apiParam progress Task's progress.
 * @apiParam assigned Task's assigned.
 * @apiParam comments Task's comments.
 * @apiParam color Task's color.
 * @apiParam organization_id Task's organization_id.
 * @apiParam moduleId Task's moduleId.
 * @apiSuccess {Object} task Task's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Task not found.
 * @apiError 401 user access only.
 */
router.put('/:idTask/updateData',
  token({ required: true }),
  body({moduleId, data: { text, start_date, end_date, progress, assigned, priority, comments, color, parent, type, responsibleArea, controlFrecuency, evaluationResult, measureIndicator,  proceder, next_day_monitory, processOwner, originType, stepAudit} }),
  updateData)


//TODO: Actualizar documentación
/**
 * @api {put} /tasks/:id/deleteData Update Data task
 * @apiName Delete Data
 * @apiGroup Task
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} task Task's data.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Task not found.
 * @apiError 401 user access only.
 */
router.delete('/:id/deleteData',
  token({ required: true }),
  body({ moduleId }),
  deleteData)


//TODO: Actualizar documentación
/**
 * @api {put} /tasks/:id/addLinks Update Data task
 * @apiName Add Links
 * @apiGroup Task
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam source Task source
 * @apiParam target Task target
 * @apiParam type Type.
 * @apiSuccess {Object} task link's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Task not found.
 * @apiError 401 user access only.
 */
router.put('/addLinks',
  token({ required: true }),
  body({moduleId, ganttLinksParams: {id, source, target, type}}),
  addLinks)


//TODO: Actualizar documentación
/**
 * @api {put} /tasks/:id/deleteLinks Update Data task
 * @apiName Delete Links
 * @apiGroup Task
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} task Task's data.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Task not found.
 * @apiError 401 user access only.
 */
router.delete('/:id/deleteLinks',
token({ required: true }),
body({ moduleId }),
deleteLinks)
 


//TODO: Actualizar documentación
/**
  * 
  */
const singleUpload = subirArchivo.array('file')
router.post('/upload/file', token({ required: true }), function (req, res) {
  singleUpload(req, res, function (err, some) {
    if (err) {
      return res.status(422).send({ errors: [{title: 'File Upload Error', detail: err.message}] })
    }
    if (req.files.length != 0) {
      saveDataFiles(req.files, req.body.task_id, req.body.organization_id, req.body.origin, req.body.moduleId, req.body.userId, req.body.folderName)
      return res.json({'uploadedFiles': req.files})
    } else {
      return res.status(409).json({message: 'Conflict upload'})
    }
  })
})











/**
 * @api {get} /tasks/:id Retrieve task
 * @apiName RetrieveTask
 * @apiGroup Task
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} task Task's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Task not found.
 * @apiError 401 user access only.
 */

router.get('/saved/files',
  body({ origin, moduleId }),
  token({ required: true }),
  savedFiles)

/**
 * @api {put} /tasks/:id Update task
 * @apiName UpdateTask
 * @apiGroup Task
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam text Task's text.
 * @apiParam start_date Task's start_date.
 * @apiParam end_date Task's end_date.
 * @apiParam progress Task's progress.
 * @apiParam assigned Task's assigned.
 * @apiParam comments Task's comments.
 * @apiParam color Task's color.
 * @apiParam organization_id Task's organization_id.
 * @apiParam moduleId Task's moduleId.
 * @apiSuccess {Object} task Task's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Task not found.
 * @apiError 401 user access only.
 */

router.put('/:id/createAudit',
  token({ required: true }),
  body({ assigned, satisfy, observations, responsibleArea, moduleId, infoFieldsExtras: [ ], stepAudit, processOwner }),
  createAudit)



/**
 * @api {put} /tasks/:id/updateLinks Update Data task
 * @apiName Update Links
 * @apiGroup Task
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam source Task source
 * @apiParam target Task target
 * @apiParam type Type.
 * @apiSuccess {Object} task link's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Task not found.
 * @apiError 401 user access only.
 */
router.put('/:id/updateLinks',
  token({ required: true }),
  body({source, target, type, moduleId}),
  updateLinks)


/**
 * @api {delete} /tasks/:id Delete task
 * @apiName DeleteTask
 * @apiGroup Task
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Task not found.
 * @apiError 401 user access only.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

router.delete('/delete/files',
  body({ keyFile }),
  token({ required: true }),
  deleteFiles)

router.get('/:id/get/File',
  token({ required: true }),
  body({versionId}),
  getFile)

router.put('/new/field',
  token({ required: true }),
  body({ fieldName, moduleId }),
  addColumn)

router.put('/delete/field',
  token({ required: true }),
  body({ fieldName, moduleId }),
  deleteColumn)

router.put('/update/file',
  token({ required: true }),
  upload.single('file'), (req, res) => {
    uploadVersionFile(req)
  }
)
router.delete('/delete/version/file',
  token({ required: true }),
  body({ versionId, keyFile }),
  deleteVersionFile)

router.post('/prueba/upload',
  token({ required: true }),
  upload.single('file'), (req, res) => {
    subirA(req)
  })
export default router
