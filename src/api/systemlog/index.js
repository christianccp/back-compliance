import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { index, create } from './controller'
import { schema } from './model'
export SystemLog, { schema } from './model'

const { moduleId, date, hour, user, domain, element, page, exceptionMessage, exceptionName, exceptionStack, description } = schema.tree
const router = new Router()

router.get('/',
  token({ required: true }),
  query({}),
  index)

router.post('/',
  body({ moduleId, date, hour, user, domain, element, page, exceptionMessage, exceptionName, exceptionStack, description }),
  create)


export default router