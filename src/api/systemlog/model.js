import mongoose, { Schema } from 'mongoose'

const SystemLogSchema = new Schema({
  moduleId:{
    type: String
  },
  date:{
    type: String
  },
  hour:{
    type: String
  },
  user:{
    type: String
  },
  domain:{
    type: String
  },
  element:{
    type: String
  },
  page:{
    type: String
  },
  exceptionMessage:{
    type: String
  },
  exceptionName:{
    type: String
  },
  exceptionStack:{
    type: String
  },
  description:{
    type: String
  },
},{
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

SystemLogSchema.methods  = {
  view (full) {
    const view = {
      id: this.id,
      moduleId: this.moduleId,
      date: this.date,
      hour: this.hour,
      user: this.user,
      domain: this.domain,
      element: this.element,
      page: this.page,
      exceptionMessage: this.exceptionMessage,
      exceptionName: this.exceptionName,
      exceptionStack: this.exceptionStack,
      description: this.description,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('SystemLog', SystemLogSchema)

export const schema = model.schema
export default model