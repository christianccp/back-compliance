import { success, notFound, notUpdate } from '../../services/response/'
import { SystemLog } from '.'

export const index = ({ querymen: { query, select, cursor }, user }, res, next) => {
  SystemLog.count(query)
    .then(count => SystemLog.find(query, select, cursor).sort({createdAt: 1}).limit(0)
      .then((systemslogs) => ({
        count,
        rows: systemslogs.map((systemslog) => systemslog.view(true))
      }))
    )
    .then(success(res))
    .catch(next)
}

export const create = ({ user, bodymen: { body } }, res, next) =>
SystemLog.create({ ...body})
    .then((doc) => doc.view(true))
    .then(success(res, 201))
    .catch(next)