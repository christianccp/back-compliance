import { Router } from "express";
import { middleware as query } from "querymen";
import { middleware as body } from "bodymen";
import { token } from "../../services/passport";
import {
  create,
  newSection,
  updateSection,
  index,
  show,
  update,
  setResponses,
  resetResponses,
  destroy,
  deleteSection
} from "./controller";
import { schema } from "./model";
import { schema as orgSchema } from "../organization";
import { Schema } from "mongoose";
import { CostExplorer } from "aws-sdk";
export Question, { schema } from "./model";

const router = new Router();

const {
  title,
  weight,
  moduleId,
  organization_id,
  typeOf,
  questionNumber,
  results,
  result,
  id,
  taskDescription,
  isCustomQuestion,
  impactVars
} = schema.tree;
const { description } = orgSchema.tree;
const { submodule, value } = schema.tree;

/**
 * @api {post} /questions Crear pregunta
 * @apiName CreateQa
 * @apiGroup Questions
 * @apiPermission admin
 * @apiParam {String} access_token Token de accesso de usuario.
 * @apiParam {String} title Título de la pregunta.
 * @apiParam {Number} weight Peso de la pregunta.
 * @apiParam {Number} result Resultado de la pregunta. Aplica para typeOf distintos de 'questionnaire'.
 * @apiParam {Object[]} results Results. Ver Object Results para mas detalles.
 * @apiParam (Object Results) {String} submodule Submodulo.
 * @apiParam (Object Results) {Numero} value Valor.
 * @apiParamExample [json] Resquest-ejemplo
 *  {
    "access_token": "123......",
    "title": "pregunta3",
    "weight": "3",
    "moduleId":"Anticorrupcion",
    "typeOf":"Tipo",
    "questionNumber": 1,
    "result": 1.1
    "results": {
        "submodule": "submodulo",
        "value": 2
    }
  }
 * @apiError {Object} 400  Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Question no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.post(
  "/",
  token({ required: true, roles: ["superadmin", "admin"] }),
  body({
    title,
    weight,
    moduleId,
    result,
    results: { submodule, value },
    typeOf,
    questionNumber,
    impactVars
  }),
  create
);

/**
 * @todo Agrega una nueva sección dentro la colección Organization; y agrega preguntas ligadas a cierta sección.
 */
router.post(
  "/new/section",
  token({ required: true }),
  body({
    description,
    title,
    moduleId,
    typeOf,
    questions: [
      { title, weight, moduleId, typeOf, questionNumber, taskDescription }
    ]
  }),
  newSection
);

/**
 * @todo Actualiza las preguntas que pertenecen a la sección que se envia en el campo typeOf. Si se eliminaron
 * algunas preguntas, estas se enviaran en el campo questionsToDelete.
 */
router.put(
  "/update/section",
  token({ required: true }),
  body({
    questionsToAdd: [
      {
        title,
        weight,
        moduleId,
        typeOf,
        questionNumber,
        taskDescription,
        isCustomQuestion,
        results
      }
    ],
    questionsToUpdate: [
      {
        title,
        weight,
        moduleId,
        typeOf,
        questionNumber,
        taskDescription,
        isCustomQuestion
      }
    ],
    questionsToDelete: [id]
  }),
  updateSection
);

/**
 * @todo Borra toda la sección y sus preguntas relacionadas
 */
router.delete("/section/:id", token({ required: true }), deleteSection);

/**
 * @api {get} /questions Consultar todas las preguntas
 * @apiName RetrieveQas
 * @apiGroup Questions
 * @apiPermission admin
 * @apiParam {String} access_token Token de acceso de administrador.
 * @apiParam {String} moduleId  Modulo a consultar (anticorruption,...)
 * @apiParam {String} [typeOf]  Id de cuestionario a consultar (questionnaire1, section1, ...).
 * @apiUse listParams
 * @apiSuccess {Number} count Cantidad total de preguntas.
 * @apiSuccess {Object[]} rows Lista de preguntas.
 * @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 401 Solo acceso a administradores.
 */

// var qaSchema = require('./model');

router.get(
  "/",
  token({ required: true }),
  query({
    moduleId,
    typeOf
  }),
  index
);

/**
 * @api {get} /questions/filter/:organizationId/:module Consultar preguntas por organizacion y modulo
 * @apiName RetrieveQa
 * @apiGroup Questions
 * @apiPermission admin
 * @apiParam {String} access_token Token de accesso de usuario.
 * @apiSuccess {Object} question Datos de la pregunta.
 * @apiError {Object} 400  Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Question no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.get("/filter/:moduleId", token({ required: true }), show);

/**
 * @api {put} /questions/:id Actualizar pregunta
 * @apiName UpdateQa
 * @apiGroup Questions
 * @apiPermission admin
 * @apiParam {String} access_token Token de accesso de usuario.
 * @apiParam {String} title Título de la pregunta.
 * @apiParam {Number} weight Peso de la pregunta.
 * @apiParam {Number} result Resultado de la pregunta. Aplica para typeOf distintos de 'questionnaire'.
 * @apiParam {Object[]} results Resultados. Ver Object Results para mas detalles.
 * @apiParam (Object Results) {String} submodule Submodulo.
 * @apiParam (Object Results) {Numero} value Valor.
 * @apiParamExample [json] Resquest-ejemplo
 *  {
    "access_token": "123......",
    "title": "pregunta3",
    "weight": "3",
    "moduleId":"Anticorrupcion",
    "typeOf":"Tipo",
    "result": 1.1
    "results": {
        "submodule": "submodulo",
        "value": 2
    }
  }
 * @apiError {Object} 400  Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Question no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.put(
  "/:id",
  token({ required: true }),
  body({
    title,
    weight,
    moduleId,
    organization_id,
    result,
    results: { submodule, value },
    typeOf,
    questionNumber,
    impactVars
  }),
  update
);

/**
 * @api {put} /questions/set/results Guardar Respuestas
 * @apiName SetResultsQa
 * @apiGroup Questions
 * @apiPermission user
 * @apiParam {String} access_token Token de accesso de usuario.
 * @apiParam {String}  Titulo de pregunta
 * @apiParam {Object[]} results Resultados. Ver Object Results para mas detalles.
 * @apiParam (Object Results) {String} submodule Submodulo.
 * @apiParam (Object Results) {Numero} value Valor.
 * @apiError {Object} 400  Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Question no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.put(
  "/set/results",
  token({ required: true }),
  body({ results, typeOf }),
  setResponses
);

/**
 * @api {put} /questions/set/results Guardar Respuestas
 * @apiName ResetResultsQa
 * @apiGroup Questions
 * @apiPermission user
 * @apiParam {String} access_token Token de accesso de usuario.
 * @apiError {Object} 400  Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Question no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.put(
  "/reset/results",
  token({ required: true }),
  body({ typeOf }),
  resetResponses
);

/**
 * @api {delete} /questions/:id Eliminar pregunta
 * @apiName DeleteQa
 * @apiGroup Questions
 * @apiPermission admin
 * @apiParam {String} access_token Token de accesso de usuario.
 * @apiSuccess (Success 204) 204 Sin contenido.
 * @apiError 404 Question no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.delete(
  "/:id",
  token({ required: true, roles: ["superadmin", "admin"] }),
  destroy
);

export default router;
