/* eslint-disable no-unused-expressions */
import { success, notFound, notResults } from "../../services/response/";
import { Question } from ".";
import { Organization } from "../organization";
import {
  validationError,
  validationResponse
} from "../../services/response/responsesModules";
import mongoose from "mongoose";

// Create Questions. DONE!
export const create = ({ bodymen: { body }, user }, res, next) =>
  Question.create({ ...body, organization_id: user.organization_id })
    .then(qa => qa.view(true))
    .then(success(res, 201))
    .catch(next);

// Create New Section
export const newSection = async ({ bodymen: { body }, user }, res, next) => {
  let session = await mongoose.startSession();
  session.startTransaction();
  try {
    let organization = await Organization.findByIdAndUpdate(
      user.organization_id,
      {
        $push: {
          questions: {
            description: body.description,
            moduleId: body.moduleId,
            typeOf: body.typeOf,
            title: body.title
          }
        }
      },
      {
        new: true,
        session
      }
    );

    if (!organization) {
      session.abortTransaction();
      res.status(404).end();
      return;
    }
    let sectionAdded =
      organization.questions[organization.questions.length - 1];

    if (body.questions) {
      body.questions.map(question => {
        question.organization_id = user.organization_id;
        question.section_id = sectionAdded._id;
      });
      // TODO: HACER QUE FALLE
      let qa = await Question.insertMany(body.questions, { session });
      session.commitTransaction();
      let response = validationResponse(qa, res);
      response ? res.json({ id: sectionAdded._id }) : null;
    } else {
      session.abortTransaction();
      res.status(422).json({
        valid: false,
        name: "Error validation",
        message: "questions field is required"
      });
    }
  } catch (err) {
    session.abortTransaction();
    validationError(err, res);
  }
};

// Update Section
export const updateSection = async ({ bodymen: { body }, user }, res, next) => {
  let session = await mongoose.startSession();
  session.startTransaction();
  try {
    if (
      !body.questionsToDelete ||
      !body.questionsToUpdate ||
      !body.questionsToAdd
    ) {
      res.status(422).json({
        valid: false,
        name: "Error validation",
        message:
          "questionsToDelete, questionsToUpdate && questionsToAdd are required"
      });
    }
    let bulkOper = [];
    body.questionsToUpdate.map((ques, index) => {
      if (ques.id) {
        let setter = { $set: dotNotate(body.questionsToUpdate[index]) };
        bulkOper.push({
          updateOne: {
            filter: { _id: ques.id },
            update: setter
          }
        });
      }
    });
    body.questionsToDelete.map(id => {
      bulkOper.push({
        deleteOne: {
          filter: { _id: id }
        }
      });
    });
    // TODO: HACER QUE FALLE //PROMISE ALL
    let qa = null;
    if (bulkOper.length > 0) {
      qa = await Question.bulkWrite(bulkOper);
    }
    let addOperation = await Question.insertMany(body.questionsToAdd);
    qa = qa !== null ? qa : addOperation;
    //session.commitTransaction();
    let response = validationResponse(qa, res);
    response ? res.json({ response: qa }) : null;
  } catch (err) {
    //session.abortTransaction();
    validationError(err, res);
  }
};

// Delete Section
export const deleteSection = async ({ params, user }, res, next) => {
  // let session = await mongoose.startSession()
  // await session.startTransaction()
  try {
    let organization = await Organization.findOneAndUpdate(
      {
        _id: user.organization_id,
        "questions._id": params.id
      },
      {
        $pull: { questions: { _id: params.id } }
      }
    );
    if (!organization) {
      // session.abortTransaction()
      res.status(404).end();
      return;
    }
    let deleteOp = await Question.deleteMany({ section_id: params.id });
    // session.commitTransaction()
    let response = validationResponse(deleteOp, res);
    response ? res.json("Delete sucessfully") : null;
    // if (deleteOp.n > 0) {
    //   session.commitTransaction()
    //   let response = validationResponse(deleteOp, res)
    //   response ? res.json('Delete sucessfully') : null
    // } else {
    //   session.abortTransaction()
    //   res.status(404).json({
    //     valid: false,
    //     name: 'No questions to delete',
    //     message: 'section without questions linked'
    //   })
    // }
  } catch (err) {
    // session.abortTransaction()
    validationError(err, res);
  }
};

// Get Questions. DONE! Nota: Solo regresa las preguntas de la organización a la cual pertenece el usuario logueado.
export const index = (
  { querymen: { query, select, cursor }, user },
  res,
  next
) => {
  query.organization_id = user.organization_id;
  Question.count(query)
    .then(count =>
      Question.find(query, select)
        .sort({ questionNumber: "1" })
        .then(qas => ({
          count,
          rows: qas.map(qa =>
            query.typeOf !== "questionnaire1" ? qa.viewParams() : qa.view()
          )
        }))
    )
    .then(success(res))
    .catch(next);
};

export const show = ({ params, user }, res, next) =>
  Question.find({
    organization_id: user.organization_id,
    moduleId: params.moduleId
  })
    .then(notResults(res))
    .then(qas => ({
      count: qas.length,
      rows: qas.map(qa => qa.view())
    }))
    .then(success(res))
    .catch(next);

export const update = ({ bodymen: { body }, params }, res, next) =>
  Question.findById(params.id)
    .then(notFound(res))
    .then(qa => (qa ? Object.assign(qa, body).save() : null))
    .then(qa => (qa ? qa.view(true) : null))
    .then(success(res))
    .catch(next);

export const setResponses = ({ bodymen: { body }, params }, res, next) => {
  if (body.results.length < 1) {
    res.status(200);
    res.send("Success Update");
  } else {
    let bulkOper = [];
    if (body.typeOf !== "questionnaire1") {
      body.results.map(reg => {
        bulkOper.push({
          updateOne: {
            filter: { _id: reg._id },
            update: { $set: { result: reg.result } }
          }
        });
      });
    } else {
      body.results.map(reg => {
        bulkOper.push({
          updateOne: {
            filter: { _id: reg._id },
            update: { $set: { results: reg.results } }
          }
        });
      });
    }
    Question.bulkWrite(bulkOper)
      .then(res1 => {
        res.status(200);
        res.send("Success Update");
      })
      .catch(error => {
        console.log(error);
      });
  }
};

export const resetResponses = (
  { bodymen: { body }, params, user },
  res,
  next
) => {
  Question.find({ organization_id: user.organization_id, typeOf: body.typeOf })
    .then(questions => {
      let bulkOper = [];
      questions.map(ques => {
        bulkOper.push({
          updateOne: {
            filter: { _id: ques._id },
            update: { $set: { results: [] } }
          }
        });
      });
      return bulkOper;
    })
    .then(bulkOper => {
      return Question.bulkWrite(bulkOper);
    })
    .then(res1 => {
      res.status(200);
      res.send("Success Update");
    })
    .catch(error => {
      console.log(error);
      next;
    });
};

export const destroy = ({ params }, res, next) =>
  Question.findById(params.id)
    .then(notFound(res))
    .then(qa => (qa ? qa.remove() : null))
    .then(success(res, 204))
    .catch(next);

const dotNotate = (obj, target, prefix) => {
  target = target || {};
  prefix = prefix || "";
  Object.keys(obj).forEach(function(key) {
    if (typeof obj[key] === "object") {
      dotNotate(obj[key], target, prefix + key + ".");
    } else {
      target[prefix + key] = obj[key];
      return target[prefix + key];
    }
  });
  return target;
};
