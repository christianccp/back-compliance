import mongoose, { Schema } from 'mongoose'

const resultsSchema = new Schema({
  _id: false,
  subrisk_id: {
    type: Schema.Types.ObjectId
  },
  subriskTitle: {
    type: String
  },
  value: {
    type: Number
  }
})

const qaSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  weight: {
    type: Number,
    required: true
  },
  organization_id: {
    type: Schema.ObjectId,
    required: true
  },
  section_id: {
    type: Schema.ObjectId
  },
  moduleId: {
    type: String,
    required: true
  },
  typeOf: {
    type: String,
    required: true
  },
  results: {
    type: [resultsSchema]
  },
  result: {
    type: Number,
    default: -1
  },
  questionNumber: {
    type: Number
  },
  linkedSubrisks: {
    type: Array
  },
  taskDescription: {
    type: String
  },
  isCustomQuestion: {
    type: Boolean
  },
  impactVars: {
    type: [String]
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

qaSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      title: this.title,
      weight: this.weight,
      organization_id: this.organization_id,
      section_id: this.section_id,
      moduleId: this.moduleId,
      questionNumber: this.questionNumber,
      typeOf: this.typeOf,
      results: this.results,
      linkedSubrisks: this.linkedSubrisks,
      taskDescription: this.taskDescription,
      isCustomQuestion: this.isCustomQuestion,
      impactVars: this.impactVars,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  },
  viewParams () {
    const view = {
      id: this.id,
      title: this.title,
      weight: this.weight,
      organization_id: this.organization_id,
      section_id: this.section_id,
      moduleId: this.moduleId,
      questionNumber: this.questionNumber,
      typeOf: this.typeOf,
      result: this.result,
      linkedSubrisks: this.linkedSubrisks,
      taskDescription: this.taskDescription,
      isCustomQuestion: this.isCustomQuestion,
      impactVars: this.impactVars,
      updatedAt: this.updatedAt
    }
    return view
  }
}

const model = mongoose.model('Question', qaSchema)

export const schema = model.schema
export default model
