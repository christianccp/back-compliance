import { Qa } from '.'

let qa

beforeEach(async () => {
  qa = await Qa.create({ title: 'test', content: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = qa.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(qa.id)
    expect(view.title).toBe(qa.title)
    expect(view.content).toBe(qa.content)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = qa.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(qa.id)
    expect(view.title).toBe(qa.title)
    expect(view.content).toBe(qa.content)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
