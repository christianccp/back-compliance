import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Facturation, { schema } from './model'

const router = new Router()
const { uso, razon, nombre, correo, calle, int, ext, cp, estado, municipio, rfc } = schema.tree

/**
 * @api {post} /facturations Create facturation
 * @apiName CreateFacturation
 * @apiGroup Facturation
 * @apiParam nombre Facturation's nombre.
 * @apiParam correo Facturation's correo.
 * @apiParam calle Facturation's calle.
 * @apiParam int Facturation's int.
 * @apiParam ext Facturation's ext.
 * @apiParam cp Facturation's cp.
 * @apiParam estado Facturation's estado.
 * @apiParam municipio Facturation's municipio.
 * @apiSuccess {Object} facturation Facturation's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Facturation not found.
 */
router.post('/',
  token({ required: true }),
  body({ uso, razon, nombre, correo, calle, int, ext, cp, estado, municipio, rfc }),
  create)

/**
 * @api {get} /facturations Retrieve facturations
 * @apiName RetrieveFacturations
 * @apiGroup Facturation
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of facturations.
 * @apiSuccess {Object[]} rows List of facturations.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /facturations/:id Retrieve facturation
 * @apiName RetrieveFacturation
 * @apiGroup Facturation
 * @apiSuccess {Object} facturation Facturation's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Facturation not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /facturations/:id Update facturation
 * @apiName UpdateFacturation
 * @apiGroup Facturation
 * @apiParam nombre Facturation's nombre.
 * @apiParam correo Facturation's correo.
 * @apiParam calle Facturation's calle.
 * @apiParam int Facturation's int.
 * @apiParam ext Facturation's ext.
 * @apiParam cp Facturation's cp.
 * @apiParam estado Facturation's estado.
 * @apiParam municipio Facturation's municipio.
 * @apiSuccess {Object} facturation Facturation's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Facturation not found.
 */
router.put('/:id',
  body({ uso, razon, nombre, correo, calle, int, ext, cp, estado, municipio, rfc }),
  update)

/**
 * @api {delete} /facturations/:id Delete facturation
 * @apiName DeleteFacturation
 * @apiGroup Facturation
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Facturation not found.
 */
router.delete('/:id',
  destroy)

export default router
