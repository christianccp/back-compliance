import { success, notFound } from '../../services/response/'
import { Facturation } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Facturation.create(body)
    .then((facturation) => facturation.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Facturation.count(query)
    .then(count => Facturation.find(query, select, cursor)
      .then((facturations) => ({
        count,
        rows: facturations.map((facturation) => facturation.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Facturation.findById(params.id)
    .then(notFound(res))
    .then((facturation) => facturation ? facturation.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Facturation.findById(params.id)
    .then(notFound(res))
    .then((facturation) => facturation ? Object.assign(facturation, body).save() : null)
    .then((facturation) => facturation ? facturation.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Facturation.findById(params.id)
    .then(notFound(res))
    .then((facturation) => facturation ? facturation.remove() : null)
    .then(success(res, 204))
    .catch(next)
