import mongoose, { Schema } from 'mongoose'

const facturationSchema = new Schema({
  nombre: {
    type: String
  },
  correo: {
    type: String
  },
  calle: {
    type: String
  },
  int: {
    type: String
  },
  ext: {
    type: String
  },
  cp: {
    type: String
  },
  estado: {
    type: String
  },
  municipio: {
    type: String
  },
  rfc: {
    type: String
  },
  razon: {
    type: String
  },
  uso: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

facturationSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      nombre: this.nombre,
      correo: this.correo,
      calle: this.calle,
      int: this.int,
      ext: this.ext,
      cp: this.cp,
      estado: this.estado,
      municipio: this.municipio,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Facturation', facturationSchema)

export const schema = model.schema
export default model
