import { Facturation } from '.'

let facturation

beforeEach(async () => {
  facturation = await Facturation.create({ nombre: 'test', correo: 'test', calle: 'test', int: 'test', ext: 'test', cp: 'test', estado: 'test', municipio: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = facturation.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(facturation.id)
    expect(view.nombre).toBe(facturation.nombre)
    expect(view.correo).toBe(facturation.correo)
    expect(view.calle).toBe(facturation.calle)
    expect(view.int).toBe(facturation.int)
    expect(view.ext).toBe(facturation.ext)
    expect(view.cp).toBe(facturation.cp)
    expect(view.estado).toBe(facturation.estado)
    expect(view.municipio).toBe(facturation.municipio)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = facturation.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(facturation.id)
    expect(view.nombre).toBe(facturation.nombre)
    expect(view.correo).toBe(facturation.correo)
    expect(view.calle).toBe(facturation.calle)
    expect(view.int).toBe(facturation.int)
    expect(view.ext).toBe(facturation.ext)
    expect(view.cp).toBe(facturation.cp)
    expect(view.estado).toBe(facturation.estado)
    expect(view.municipio).toBe(facturation.municipio)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
