import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Facturation } from '.'

const app = () => express(apiRoot, routes)

let facturation

beforeEach(async () => {
  facturation = await Facturation.create({})
})

test('POST /facturations 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ nombre: 'test', correo: 'test', calle: 'test', int: 'test', ext: 'test', cp: 'test', estado: 'test', municipio: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.nombre).toEqual('test')
  expect(body.correo).toEqual('test')
  expect(body.calle).toEqual('test')
  expect(body.int).toEqual('test')
  expect(body.ext).toEqual('test')
  expect(body.cp).toEqual('test')
  expect(body.estado).toEqual('test')
  expect(body.municipio).toEqual('test')
})

test('GET /facturations 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /facturations/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${facturation.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(facturation.id)
})

test('GET /facturations/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /facturations/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${facturation.id}`)
    .send({ nombre: 'test', correo: 'test', calle: 'test', int: 'test', ext: 'test', cp: 'test', estado: 'test', municipio: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(facturation.id)
  expect(body.nombre).toEqual('test')
  expect(body.correo).toEqual('test')
  expect(body.calle).toEqual('test')
  expect(body.int).toEqual('test')
  expect(body.ext).toEqual('test')
  expect(body.cp).toEqual('test')
  expect(body.estado).toEqual('test')
  expect(body.municipio).toEqual('test')
})

test('PUT /facturations/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ nombre: 'test', correo: 'test', calle: 'test', int: 'test', ext: 'test', cp: 'test', estado: 'test', municipio: 'test' })
  expect(status).toBe(404)
})

test('DELETE /facturations/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${facturation.id}`)
  expect(status).toBe(204)
})

test('DELETE /facturations/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
