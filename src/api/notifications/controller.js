import { success, notFound, authorOrAdmin } from '../../services/response/'
import { Notifications } from '.'
import { User } from '../user'

export const create = ({ user, body }, res, next) =>{

  if( body.target[0] === "ALL"){
    User.find({organization_id: user.organization_id}, {_id:1})
    .then( async users=>{
      var allTarget = []
      await users.map(user => {
        allTarget.push(user._id.toString())
      })
      body.target = allTarget
      Notifications.create({ ...body,'organization_id': user.organization_id })
      .then((notifications) => notifications.view(true))
      .then(success(res, 201))
      .catch(next)
    })
  }else{
    Notifications.create({ ...body,'organization_id': user.organization_id })
    .then((notifications) => notifications.view(true))
    .then(success(res, 201))
    .catch(next)
  }
}

export const index = ({ user }, res, next) =>
  Notifications.find({organization_id: user.organization_id, target:user._id.toString()})
  .then(notifications =>{
    notifications = notifications.map(user1 =>{
      var {_id, target, title, bodyN, link, organization_id, createdAt, typeN, optionsN} = user1
      return {_id, target, title, bodyN, link, organization_id, createdAt, typeN, optionsN}
    })
    return notifications
  })
  .then(success(res,200))
  .catch(next)
  

export const show = ({ params }, res, next) =>
  Notifications.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then((notifications) => notifications ? notifications.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ user, body, params }, res, next) =>
  Notifications.findById(params.id)
    .populate('user')
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'user'))
    .then((notifications) => notifications ? Object.assign(notifications, body).save() : null)
    .then((notifications) => notifications ? notifications.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ( { user, params }, res, next) =>
  Notifications.update({_id: params.id},{$pull:{target:user._id.toString()}})
    .then(async()=>{
      return await Notifications.remove({ target: { $exists: true, $size: 0 } })
    })
    .then(success(res,200))
    .catch(next)
