import mongoose, { Schema } from 'mongoose'

const optionsSchema = new Schema({
  _id: false,
  name: {
    type: String
  },
  folderId: {
    type: Schema.Types.ObjectId,
    ref: 'Folder'
  },
  documentId: {
    type: Schema.Types.ObjectId,
    ref: 'File'
  }
})

const notificationsSchema = new Schema({
  title: {
    type: String
  },
  bodyN: {
    type: String
  }, 
  link: {
    type: String
  }, 
  target: {
    type: Array
  }, 
  organization_id: {
    type: String
  }, 
  typeN: {
    type: String
  }, 
  optionsN:{
    type: Object
  }
  // user: {
  //   type: Schema.ObjectId,
  //   ref: 'User',
  //   required: true
  // }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

notificationsSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      //user: this.user.view(full),
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Notifications', notificationsSchema)

export const schema = model.schema
export default model
