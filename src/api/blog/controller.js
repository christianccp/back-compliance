import { success, notFound } from '../../services/response/'
import { validationError, validationResponse } from '../../services/response/responsesModules'
import { Blog } from '.'

export const create = async ({ bodymen: { body } }, res, next) => {
  try {
    let createdBlog = await Blog.create(body)
    let resultValidation = validationResponse(createdBlog, res)

    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}

export const index = async ({ querymen: { query, select, cursor } }, res, next) => {
  try {
    let findBlog = await Blog.find(query, select, cursor)

    if (findBlog.length == 0) {
      let resultValidation = validationResponse(findBlog.length, res)
      resultValidation ? res.json(resultValidation.map((res) => res.view() )  ) : null
    } else {
      let resultValidation = validationResponse(findBlog, res)
      resultValidation ? res.json(resultValidation.map((res) => res.view() )  ) : null
    }
  } catch (err) {
    validationError(err, res)
  }
}
export const show = async ({ querymen: { query, select, cursor } }, res, next) => {
  try {
    console.log(query)
    let findBlog = await Blog.findOne({_id: query.id})
    let resultValidation = validationResponse(findBlog, res)
    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}
export const update = async ({ bodymen: { body }, params }, res, next) => {
  try {
    let setter = { $set: {} }

    if (body.title != null) {
      setter.$set['title'] = body.title
    }
    if (body.content != null) {
      setter.$set['content'] = body.content
    }
    if (body.type != null) {
      setter.$set['type'] = body.type
    }
    if (body.imageurl != null) {
      setter.$set['imageurl'] = body.imageurl
    }
    
    if (body.meta != null) {
      if (body.meta.description != null) {
        setter.$set['meta.description'] = body.meta.description
      }
      if (body.meta.author != null) {
        setter.$set['meta.author'] = body.meta.author
      }
    }
    let updateBlog = await Blog.findOneAndUpdate({ _id: params.id }, setter, {new: true})
    let resultValidation = validationResponse(updateBlog, res)
    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}

export const destroy = async ({ params }, res, next) => {
  try {
    let deleteBlog = await Blog.findOneAndDelete({ _id: params.id })
    let resultValidation = validationResponse(deleteBlog, res)
    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}
