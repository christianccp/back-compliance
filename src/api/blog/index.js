import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Blog, { schema } from './model'

const router = new Router()
const { id, type, title, content, imageurl, meta: { description, author } } = schema.tree

// TODO: add params to methods documentation

/**
 * @api {post} /blogs Create blog
 * @apiName CreateBlog
 * @apiGroup Blog
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiParam title Blog's title.
 * @apiParam desc Blog's desc.
 * @apiSuccess {Object} blog Blog's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Blog not found.
 * @apiError 401 admin access only.
 */
router.post('/',
  token({ required: true, roles: ['superadmin'] }),
  body({ title, content, type, imageurl, meta: { description, author } }),
  create)

/**
 * @api {get} /blogs Retrieve blogs
 * @apiName RetrieveBlogs
 * @apiGroup Blog
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of blogs.
 * @apiSuccess {Object[]} rows List of blogs.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query({type}),
  index)

/**
 * @api {get} /blogs/:id Retrieve blog
 * @apiName RetrieveBlog
 * @apiGroup Blog
 * @apiSuccess {Object} blog Blog's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Blog not found.
 */
router.get('/get/blog',
  query({ id }),
  show)

/**
 * @api {put} /blogs/:id Update blog
 * @apiName UpdateBlog
 * @apiGroup Blog
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiParam title Blog's title.
 * @apiParam desc Blog's desc.
 * @apiSuccess {Object} blog Blog's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Blog not found.
 * @apiError 401 admin access only.
 */
router.put('/:id/update/blog',
  token({ required: true, roles: ['superadmin'] }),
  body({ title, content, type, imageurl, meta: { description, author } }),
  update)

/**
 * @api {delete} /blogs/:id Delete blog
 * @apiName DeleteBlog
 * @apiGroup Blog
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Blog not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  token({ required: true, roles: ['superadmin'] }),
  destroy)

export default router
