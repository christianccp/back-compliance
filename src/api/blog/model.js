import mongoose, { Schema } from 'mongoose'

const blogSchema = new Schema({
  title: {
    type: String
  },
  content: {
    type: String
  },
  type: {
    type: String,
    required: true
  },
  imageurl: {
    type: String
  },
  meta: {
    description: { type: String },
    author: { type: String }
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

blogSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      title: this.title,
      content: this.content,
      imageurl: this.imageurl,
      type: this.type,
      meta: {
        description: this.meta.description,
        author: this.meta.author
      },
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Blog', blogSchema)

export const schema = model.schema
export default model
