// TODO: Update new schema to Test

import { Blog } from '.'

let blog

beforeEach(async () => {
  blog = await Blog.create({ title: 'test', desc: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = blog.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(blog.id)
    expect(view.title).toBe(blog.title)
    expect(view.desc).toBe(blog.desc)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = blog.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(blog.id)
    expect(view.title).toBe(blog.title)
    expect(view.desc).toBe(blog.desc)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
