import { File } from '.'

let file

beforeEach(async () => {
  file = await File.create({ organization_id: 'test', moduleId: 'test', version: 'test', logUsersAccess: 'test', status: 'test', accessRoles: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = file.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(file.id)
    expect(view.organization_id).toBe(file.organization_id)
    expect(view.moduleId).toBe(file.moduleId)
    expect(view.version).toBe(file.version)
    expect(view.logUsersAccess).toBe(file.logUsersAccess)
    expect(view.status).toBe(file.status)
    expect(view.accessRoles).toBe(file.accessRoles)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = file.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(file.id)
    expect(view.organization_id).toBe(file.organization_id)
    expect(view.moduleId).toBe(file.moduleId)
    expect(view.version).toBe(file.version)
    expect(view.logUsersAccess).toBe(file.logUsersAccess)
    expect(view.status).toBe(file.status)
    expect(view.accessRoles).toBe(file.accessRoles)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
