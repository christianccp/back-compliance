import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { updateLogAPI, filesByFolder, deleteFiles, deleteVersionFile, getFile, uploadVersionFile, subirArchivo, saveDataFiles, create, index, show, update, destroy } from './controller'
import { schema } from './model'
export File, { schema } from './model'
const router = new Router()
const { statusDate, activity, detail, validity, areaInvolved, folderId, taskId, keyFile, versionId, usrUploadedFile, uploadDate, fileName, fileUrl, size, dateUpload, dateUpdate, origin, key, organization_id, moduleId, version, logUsersAccess, status, accessRoles } = schema.tree
const multer = require('multer')
const upload = multer()
/**
 * @api {post} /files Create file
 * @apiName CreateFile
 * @apiGroup File
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam organization_id File's organization_id.
 * @apiParam moduleId File's moduleId.
 * @apiParam version File's version.
 * @apiParam logUsersAccess File's logUsersAccess.
 * @apiParam status File's status.
 * @apiParam accessRoles File's accessRoles.
 * @apiSuccess {Object} file File's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 File not found.
 * @apiError 401 user access only.
 */
router.post('/',
  token({ required: true }),
  body({ fileName, fileUrl, size, dateUpload, dateUpdate, origin, key, organization_id, moduleId, version, logUsersAccess, status, accessRoles }),
  create)

/**
 * @api {get} /files Retrieve files
 * @apiName RetrieveFiles
 * @apiGroup File
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of files.
 * @apiSuccess {Object[]} rows List of files.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: true }),
  query({ moduleId, dateUpload }),
  index)

/**
 * @api {get} /files/:id Retrieve file
 * @apiName RetrieveFile
 * @apiGroup File
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} file File's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 File not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /files/:id Update file
 * @apiName UpdateFile
 * @apiGroup File
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam organization_id File's organization_id.
 * @apiParam moduleId File's moduleId.
 * @apiParam version File's version.
 * @apiParam logUsersAccess File's logUsersAccess.
 * @apiParam status File's status.
 * @apiParam accessRoles File's accessRoles.
 * @apiSuccess {Object} file File's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 File not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ fileName, fileUrl, size, dateUpdate, origin, key, moduleId, version, logUsersAccess, status, accessRoles, areaInvolved, validity, statusDate }),
  update)

router.put('/:id/updateLog',
  token({ required: true }),
  body({ activity, detail }),
  updateLogAPI)

/**
 * @api {delete} /files/:id Delete file
 * @apiName DeleteFile
 * @apiGroup File
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 File not found.
 * @apiError 401 user access only.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

const singleUpload = subirArchivo.array('file')
router.post('/upload/file', token({ required: true }), function (req, res) {
  singleUpload(req, res, function (err, some) {
    if (err) {
      return res.status(422).send({ errors: [{title: 'File Upload Error', detail: err.message}] })
    }
    if (req.files.length != 0) {
      let saved = saveDataFiles(req.files, req.body.task_id, req.user.organization_id, req.body.origin, req.body.moduleId, req.user._id, req.body.folderId)
        .then((respuesta) => {
          res.send(respuesta)
        })
        .catch((err) => {
          res.status(409).send({ errors: [{title: 'Conflict to save the file data', detail: err.message}] })
        })
      // return res.json({'uploadedFiles': req.files})
    } else {
      return res.status(409).json({message: 'Conflict upload'})
    }
  })
})

router.put('/update/file',
  token({ required: true }),
  upload.single('file'), (req, res, err) => {
    let resp = uploadVersionFile(req)
      .then((response) => {
        res.send(response)
      })
      .catch((err) => {
        res.status(422).send({ errors: [{title: 'File Upload Error', detail: err.message}] })
      })
  }
)

router.get('/:id/get/File',
  token({ required: true }),
  body({versionId}),
  getFile)

router.delete('/delete/version/file',
  token({ required: true }),
  body({ versionId, keyFile }),
  deleteVersionFile)

router.delete('/delete/files',
  body({ keyFile, moduleId, taskId }),
  token({ required: true }),
  deleteFiles)

router.get('/by/folder',
  query({ folderId }),
  token({ required: true }),
  filesByFolder)
export default router
