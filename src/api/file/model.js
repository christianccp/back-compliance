import mongoose, { Schema } from 'mongoose'
const status = [ 'noStatus', 'draft', 'draftReminder', 'review', 'reviewReminder',
  'conciliation', 'conciliationReminder', 'canceled', 'inApprovalPO', 'inApprovalTL',
  'inApprovalAD', 'published', 'requestDelete', 'requestDeleteReminder', 'conciliationDelete', 'conciliationDeleteReminder', 'deleted' ]
const activities = ['query', 'add', 'modify', 'delete', 'review', 'draft', 'draftReminder', 'reviewReminder', 'conciliation', 'conciliationReminder', 'canceled', 'inApprovalPO', 'inApprovalTL', 'inApprovalAD', 'published', 'requestDelete', 'requestDeleteReminder', 'conciliationDelete', 'conciliationDeleteReminder']
const roles = ['user', 'admin']
const isAuth = ['', 'A', 'NA', 'AE', 'NAE']

const versionSchema = new Schema({
  _id: false,
  versionNumber: {
    type: Number,
    default: 1
  },
  versionId: {
    type: String
  }
})
const fileSchema = new Schema({
  fileName: {
    type: String
  },
  fileUrl: {
    type: String
  },
  size: {
    type: String
  },
  dateUpload: {
    type: Date,
    default: Date.now()
  },
  dateUpdate: {
    type: Date,
    default: Date.now()
  },
  origin: {
    type: String,
    lowercase: true
  },
  key: {
    type: String
  },
  organization_id: {
    type: Schema.Types.ObjectId,
    ref: 'Organization',
    require: true
  },
  moduleId: {
    type: String,
    require: true
  },
  version: {
    type: [versionSchema]
  },
  logUsersAccess: [{
    _id: false,
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    activity: {
      type: String,
      enum: activities
    },
    detail: {
      type: String
    },
    dateAccess: {
      type: Date,
      default: Date.now()
    }
  }],
  status: {
    type: String,
    enum: status
  },
  statusDate: {
    type: Date,
    default: Date.now()
  },
  folder: {
    type: Schema.Types.ObjectId,
    ref: 'Folder'
  },
  accessRoles: {
    modify: {
      type: String,
      enum: roles
    },
    delete: {
      type: String,
      enum: roles
    },
    query: {
      type: String,
      enum: roles
    }
  },
  areaInvolved: [{
    _id: false,
    area: {
      type: String
    },
    isAuthorized: {
      type: String,
      defult: '',
      enum: isAuth
    }
  }],
  validity: {
    type: Date
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

fileSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      task_id: this.task_id,
      fileName: this.fileName,
      fileUrl: this.fileUrl,
      size: this.size,
      dateUpload: this.dateUpload,
      dateUpdate: this.dateUpdate,
      origin: this.origin,
      key: this.key,
      organization_id: this.organization_id,
      moduleId: this.moduleId,
      version: this.version,
      logUsersAccess: this.logUsersAccess,
      status: this.status,
      accessRoles: this.accessRoles,
      areaInvolved: this.areaInvolved,
      validity: this.validity,
      statusDate: this.statusDate,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('File', fileSchema)

export const schema = model.schema
export default model
