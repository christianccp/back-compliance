import { success, notFound } from '../../services/response/'
import { File } from '.'
import { validationError, validationResponse } from '../../services/response/responsesModules'
import { dotNotate } from '../../services/miscellany/functions'
import { secretAccessKeyAWSDevelopment, accessKeyIdAWSDevelopment, secretAccessKeyAWSProduction, accessKeyIdAWSProduction, bucketAWSProduction, bucketAWSDevelepment } from '../../config'
import { Task } from '../../../src/api/task'
import { User } from '../../../src/api/user'
import { Organization } from '../../../src/api/organization'
import { Notifications } from '../../../src/api/notifications'
import { Folder } from '../../../src/api/folder'
import cron from 'node-cron'
const multer = require('multer')
const multerS3 = require('multer-s3')
const aws = require('aws-sdk')
const moment = require('moment')
let secretAccessKeyAWS = null
let accessKeyIdAWS = null
let bucketNameAWS = null
if (process.env.NODE_ENV === 'production') {
  secretAccessKeyAWS = secretAccessKeyAWSProduction
  accessKeyIdAWS = accessKeyIdAWSProduction
  bucketNameAWS = bucketAWSProduction
} else {
  secretAccessKeyAWS = secretAccessKeyAWSDevelopment
  accessKeyIdAWS = accessKeyIdAWSDevelopment
  bucketNameAWS = bucketAWSDevelepment
}

aws.config.update({
  secretAccessKey: secretAccessKeyAWS,
  accessKeyId: accessKeyIdAWS,
  region: 'us-east-1'
})
export const create = async ({ bodymen: { body }, user }, res, next) => {
  try {
    let createF = await File.create(body)
    let resultValidation = validationResponse(createF, res)
    if (resultValidation) {
      let pushAdd = await File.findOneAndUpdate({_id: createF._id}, {$push: { logUsersAccess: {userId: user._id, activity: 'add', 'dateAccess': Date.now()} }}, {new: true})
      let resultValidation2 = validationResponse(pushAdd, res)
      resultValidation2 ? res.json(resultValidation2.view()) : null
    }
  } catch (err) {
    validationError(err, res)
  }
}

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  File.count(query)
    .then(count => File.find(query, select, cursor)
      .then((files) => ({
        count,
        rows: files.map((file) => file.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  File.findById(params.id)
    .then(notFound(res))
    .then((file) => file ? file.view() : null)
    .then(success(res))
    .catch(next)

export const update = async ({ bodymen: { body }, params }, res, next) => {
  try {
    console.log('body A', body)
    if (body.status != null) {
      body.statusDate = Date.now()
      if (body.status == 'published') {
        body.validity = moment().add(1, 'year').format()
      }

    }
    console.log('body D', body)
    let setter = { $set: dotNotate(body) }
    let updateFile = await File.findOneAndUpdate({_id: params.id}, setter, {new: true})
    let resultValidation = validationResponse(updateFile, res)
    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}

export const destroy = async ({ params }, res, next) => {
  File.findById(params.id)
    .then(notFound(res))
    .then((file) => file ? file.remove() : null)
    .then(success(res, 204))
    .catch(next)
}

const s3 = new aws.S3()
export const subirArchivo = multer({
  storage: multerS3({
    s3: s3,
    bucket: bucketNameAWS,
    acl: 'public-read',
    contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata: function (req, file, cb) {
      cb(null, {fieldName: file.originalname, contentType: file.mimetype})
    },
    key: function (req, file, cb) {
      cb(null, Date.now().toString())
    }
  })
})

export const updateLogAPI = async ({ bodymen: { body }, user, params }, res, next) => {
  try {
    if (body.detail != null) {
      let docFile = await File.findOneAndUpdate({_id: params.id}, {'$push': {
        'logUsersAccess': {
          'userId': user._id,
          'activity': body.activity,
          'detail': body.detail,
          'dateAccess': Date.now()
        }
      }}, {new: true})
      let resultValidation = validationResponse(docFile, res)
      resultValidation ? res.json(resultValidation.view()) : null
    } else {
      let docFile = await File.findOneAndUpdate({_id: params.id}, {'$push': {
        'logUsersAccess': {
          'userId': user._id,
          'activity': body.activity,
          'dateAccess': Date.now()
        }
      }}, {new: true})
      let resultValidation = validationResponse(docFile, res)
      resultValidation ? res.json(resultValidation.view()) : null
    }
  } catch (err) {
    validationError(err, res)
  }
}

const funcUpdateLog = async function (idFile, activity) {
  try {
    let docFile = await File.findOneAndUpdate({_id: idFile}, {'$push': {
      'logUsersAccess': {
        'activity': activity
      }
    }}, {new: true})
  } catch (err) {
  }
}
export const saveDataFiles = async function (array, taskId, OrgId, origin, moduleId, userId, folderId, areaInvolved, res, next) {
  try {
    console.log('AreaI: ', areaInvolved)
    let data = []
    var respon
    let docFilesG
    array.map(elem => {
      data.push({'fileName': elem.originalname,
        'fileUrl': elem.location,
        'size': elem.size,
        'origin': origin,
        'key': elem.key,
        'version': [{
          'versionNumber': 1,
          'versionId': elem.versionId
        }],
        'organization_id': OrgId,
        'moduleId': moduleId,
        'logUsersAccess': [{
          'userId': userId,
          'activity': 'add'
        }],
        'status': 'draft',
        'folder': folderId,
        'areaInvolved': areaInvolved
      })
    })
    if (taskId != undefined) {
      data[0].status = 'noStatus'
      let docFiles = await File.create(data[0])
      docFilesG = docFiles
    } else {
      let docFiles = await File.create(data[0])
      docFilesG = docFiles
    }
    //  console.log('DOC: ', docFiles)
    respon = docFilesG
    if (taskId != undefined) {
      let docTask = await Task.findOneAndUpdate({'organization_id': OrgId, 'moduleId': moduleId, 'tasks.data.id': taskId}, {'$push': {'tasks.data.$.filesIdDocument': docFilesG._id}}, {new: true})
      //console.log(docTask)
    }
    // console.log('resp2', respon)
    // return respon
    //  })
    console.log('console1', respon)
    return docFilesG
  } catch (err) {
    console.log(err)
  }
}

export const uploadVersionFile = async function (req, res, next) {
  try {
    var s3 = new aws.S3()
    var respuesta
    var params = {
      Bucket: bucketNameAWS,
      ACL: 'public-read',
      Body: req.file.buffer,
      Key: req.body.key_file,
      ContentType: req.file.mimetype,
      Metadata: {
        contenttype: req.file.mimetype,
        fieldname: req.file.originalname
      }
    }
    let putObt = await s3.putObject(params).promise()
    if (putObt) {
      let findFile = await File.findOne({key: req.body.key_file})
      if (findFile) {
        var version
        if (findFile.version.length > 0) {
          version = (findFile.version[(findFile.version.length) - 1].versionNumber) + 1
        } else {
          version = 1
        }
        let docFile = await File.findOneAndUpdate({key: req.body.key_file}, {'$push': {
          'version': {
            'versionNumber': version,
            'versionId': putObt.VersionId
          },
          'logUsersAccess': {
            'userId': req.user._id,
            'activity': 'modify'
          }
        }}, {new: true})
        return docFile
      }
    }
  } catch (err) {

  }
}

export const getFile = async ({bodymen: { body }, params, user}, res, next) => {
  var s3 = new aws.S3()
  var param = {
    Bucket: bucketNameAWS,
    Key: params.id,
    VersionId: body.versionId
  }
  let docFile = await File.findOneAndUpdate({organization_id: user.organization_id, 'version.versionId': body.versionId}, {'$push': {
    'logUsersAccess': {
      'userId': user._id,
      'activity': 'query'
    }
  }}, {new: true})
  s3.getObject(param)
    .on('httpHeaders', function (statusCode, headers) {
      res.set('Content-Length', headers['content-length'])
      res.set('Content-Type', headers['x-amz-meta-contenttype'])
      res.attachment(headers['x-amz-meta-fieldname'])
      this.response.httpResponse.createUnbufferedStream()
        .pipe(res)
    })
    .send()
}

export const deleteVersionFile = async ({ bodymen: { body }, params, user }, res, next) => {
  try {
    var s3 = new aws.S3()
    var param = {
      Bucket: bucketNameAWS,
      Key: body.keyFile,
      VersionId: body.versionId
    }
    let delres = await s3.deleteObject(param).promise()
    let resAWS = validationResponse(delres, res)
    if (resAWS) {
      let delVersion = await File.findOneAndUpdate({ key: body.keyFile, 'version.versionId': body.versionId }, {'$pull': { 'version': { 'versionId': body.versionId } }}, {new: true})
      if (delVersion) {
        let docFile = await File.findOneAndUpdate({key: body.keyFile}, {'$push': {
          'logUsersAccess': {
            'userId': user._id,
            'activity': 'delete'
          }
        }}, {new: true})
        if (docFile) {
          let resultValidation = validationResponse(docFile, res)
          resultValidation ? res.json(resultValidation) : null
        }
      }
    }
  } catch (err) {
    validationError(err, res)
  }
}

export const deleteFiles = async ({ bodymen: { body }, params, user }, res, next) => {
  try {
    var results1, results2, results3
    var s3 = new aws.S3()
    var param = { Bucket: bucketNameAWS, Key: body.keyFile }
    let resAWS = await s3.deleteObject(param).promise()
    let validAWS = validationResponse(resAWS, res)
    if (validAWS) {
      if (body.moduleId != null & body.taskId != null) {
        results1 = await File.findOne({key: body.keyFile})
        if (results1) {
          results2 = await File.findOneAndDelete({ key: body.keyFile })
          if (results2) {
            results3 = await Task.findOneAndUpdate({organization_id: user.organization_id, moduleId: body.moduleId, 'tasks.data.id': body.taskId}, { '$pull': {'tasks.data.$.filesIdDocument': results1._id} }, {new: true})
            let resultValidation = validationResponse(results3, res)
            resultValidation ? res.json(resultValidation) : null
          }
        }
      } else {
        results2 = await File.findOneAndDelete({ key: body.keyFile })
        let resultValidation = validationResponse(results2, res)
        resultValidation ? res.json(resultValidation) : null
      }
    }
    // let results3 = await Task.findOneAndUpdate({organization_id: user.organization_id, moduleId: body.moduleId, 'tasks.data.id': body.taskId}, { '$pull': {'tasks.data.$.filesIdDocument': results1._id} }, {new: true})
    // console.log('Res3', results3)
  } catch (err) {
    validationError(err, res)
  }
}

export const filesByFolder = async ({ querymen: { query, select, cursor }, user }, res, next) => {
  try {
    let loadFiles = await File.find({ folder: query.folderId })
    let resultValidation = validationResponse(loadFiles, res)
    resultValidation ? res.json(resultValidation) : null
  } catch (err) {
    validationError(err, res)
  }
}

cron.schedule('0 0 1 * * *', async () => {
  try {
    let draftFiles = await File.find({ status: 'draft' })
    draftFiles.map(async element => {
      var ids = []
      var date = new Date(element.statusDate)
      let created = date.toISOString().substring(0, 10)
      
      let date2 = new Date()
      let hoy = date2.toISOString().substring(0, 10)
  
      let a = moment(created)
      let b = moment(hoy)
      var difference = b.diff(a, 'days')
      let findUsr = await User.find({ organization_id: element.organization_id, roleDocMgt: 'lead' }, {_id: 1})
      let findFolder = await Folder.findOne({_id: element.folder})
      findUsr.forEach(item => {
        ids.push(item._id.toString())
      })

      if (difference > 5) {
        let updateStatus = await File.findOneAndUpdate({_id: element._id}, {$set: { status: 'draftReminder', statusDate: Date.now() }}, {new: true})
        await Notifications.create({title: 'Recordatorio', bodyN: 'Archivo ' + element.fileName + ' en estado de BORRADOR', link: 'fileManager', organization_id: element.organization_id, target: ids, 'typeN': 'revision', 'optionsN': {'name': findFolder.name, 'id': findFolder._id, 'idFile': element._id}})
        funcUpdateLog(element._id, 'draftReminder')
      }
    })

    let reviewFiles = await File.find({ status: 'review' })
    reviewFiles.map(async element => {
      var ids = []
      var date = new Date(element.statusDate)
      let created = date.toISOString().substring(0, 10)    
      let date2 = new Date()
      let hoy = date2.toISOString().substring(0, 10) 
      let a = moment(created)
      let b = moment(hoy)
      var difference = b.diff(a, 'days')
      let findUsr = await User.find({ organization_id: element.organization_id, roleDocMgt: 'admin' }, {_id: 1})
      let findFolder = await Folder.findOne({_id: element.folder})
      findUsr.forEach(item => {
        ids.push(item._id.toString())
      })

      if (difference > 3) {
        let updateStatus = await File.findOneAndUpdate({_id: element._id}, {$set: { status: 'reviewReminder', statusDate: Date.now() }}, {new: true})
        await Notifications.create({title: 'Recordatorio', bodyN: 'Archivo ' + element.fileName + ' en estado de REVISIÓN', link: 'fileManager', organization_id: element.organization_id, target: ids, 'typeN': 'revision', 'optionsN': {'name': findFolder.name, 'id': findFolder._id, 'idFile': element._id}})
        funcUpdateLog(element._id, 'reviewReminder')
      }
    })
    
    let concilationFiles = await File.find({ status: 'conciliation' })
    concilationFiles.map(async element => {
      var ids = []
      var date = new Date(element.statusDate)
      let created = date.toISOString().substring(0, 10)
      
      let date2 = new Date()
      let hoy = date2.toISOString().substring(0, 10)
  
      let a = moment(created)
      let b = moment(hoy)
      var difference = b.diff(a, 'days')
      let findAreas = await Organization.find({ _id: element.organization_id }, {areas: 1, _id: 0})
      let findFolder = await Folder.findOne({_id: element.folder})
      let area = findAreas.pop()
      area.areas.forEach(item => {
        if (item.authorizer != 'null') {
          ids.push(item.authorizer)
        }
      })
      if (difference > 9) {
        let updateStatus = await File.findOneAndUpdate({_id: element._id}, {$set: { status: 'conciliationReminder', statusDate: Date.now() }}, {new: true})
        await Notifications.create({title: 'Recordatorio', bodyN: 'Archivo ' + element.fileName + ' en estado de CONCILIACIÓN', link: 'fileManager', organization_id: element.organization_id, target: ids, 'typeN': 'revision', 'optionsN': {'name': findFolder.name, 'id': findFolder._id, 'idFile': element._id}})
        funcUpdateLog(element._id, 'conciliationReminder')
      }
    })

    let applovalPoFiles = await File.find({ status: 'inApprovalPO' })

    applovalPoFiles.map(async element => {
      var ids = []
      var date = new Date(element.statusDate)
      let created = date.toISOString().substring(0, 10)
      
      let date2 = new Date()
      let hoy = date2.toISOString().substring(0, 10)
  
      let a = moment(created)
      let b = moment(hoy)
      var difference = b.diff(a, 'days')
      let findUsr = await User.find({ organization_id: element.organization_id, roleDocMgt: 'lead' }, {_id: 1})
      let findFolder = await Folder.findOne({_id: element.folder})
      findUsr.forEach(item => {
        ids.push(item._id.toString())
      })
      if (difference > 5) {
        let updateStatus = await File.findOneAndUpdate({_id: element._id}, {$set: { status: 'inApprovalTL', statusDate: Date.now() }}, {new: true})
        await Notifications.create({title: 'Recordatorio', bodyN: 'Archivo ' + element.fileName + ' en estado APROBACIÓN DE LIDER TÉCNICO', link: 'fileManager', organization_id: element.organization_id, target: ids, 'typeN': 'revision', 'optionsN': {'name': findFolder.name, 'id': findFolder._id, 'idFile': element._id}})
        funcUpdateLog(element._id, 'inApprovalTL')
      }
    })

    let applovalTlFiles = await File.find({ status: 'inApprovalTL' })
    applovalTlFiles.map(async element => {
      var ids = []
      var date = new Date(element.statusDate)
      let created = date.toISOString().substring(0, 10)
      
      let date2 = new Date()
      let hoy = date2.toISOString().substring(0, 10)
  
      let a = moment(created)
      let b = moment(hoy)
      var difference = b.diff(a, 'days')
      let findUsr = await User.find({ organization_id: element.organization_id, roleDocMgt: 'admin' }, {_id: 1})
      let findFolder = await Folder.findOne({_id: element.folder})
      findUsr.forEach(item => {
        ids.push(item._id.toString())
      })
      if (difference > 3) {
        let updateStatus = await File.findOneAndUpdate({_id: element._id}, {$set: { status: 'inApprovalAD', statusDate: Date.now() }}, {new: true})
        await Notifications.create({title: 'Recordatorio', bodyN: 'Archivo ' + element.fileName + ' en estado APROBACIÓN DE ADMINISTRADOR', link: 'fileManager', organization_id: element.organization_id, target: ids, 'typeN': 'revision', 'optionsN': {'name': findFolder.name, 'id': findFolder._id, 'idFile': element._id}})
        funcUpdateLog(element._id, 'inApprovalAD')
      }
    })

    let applovalAdFiles = await File.find({ status: 'inApprovalAD' })
    applovalAdFiles.map(async element => {
      var ids = []
      var date = new Date(element.statusDate)
      let created = date.toISOString().substring(0, 10)
      
      let date2 = new Date()
      let hoy = date2.toISOString().substring(0, 10)
  
      let a = moment(created)
      let b = moment(hoy)
      var difference = b.diff(a, 'days')
      let findUsr = await User.find({ organization_id: element.organization_id }, {_id: 1})
      let findFolder = await Folder.findOne({_id: element.folder})
      findUsr.forEach(item => {
        ids.push(item._id.toString())
      })
      if (difference > 2) {
        let updateStatus = await File.findOneAndUpdate({_id: element._id}, {$set: { status: 'published', statusDate: Date.now() }}, {new: true})
        await Notifications.create({title: 'Recordatorio', bodyN: 'Archivo ' + element.fileName + ' en estado de PUBLICACIÓN', link: 'fileManager', organization_id: element.organization_id, target: ids, 'typeN': 'revision', 'optionsN': {'name': findFolder.name, 'id': findFolder._id, 'idFile': element._id}})
        funcUpdateLog(element._id, 'published')
      }
    })
    

    let deleteFiles = await File.find({ status: 'requestDelete' })
    deleteFiles.map(async element => {
      var ids = []
      var date = new Date(element.statusDate)
      let created = date.toISOString().substring(0, 10)
      
      let date2 = new Date()
      let hoy = date2.toISOString().substring(0, 10)
  
      let a = moment(created)
      let b = moment(hoy)
      var difference = b.diff(a, 'days')
      let findUsr = await User.find({ organization_id: element.organization_id, roleDocMgt: 'owner' }, {_id: 1})
      let findFolder = await Folder.findOne({_id: element.folder})
      findUsr.forEach(item => {
        ids.push(item._id.toString())
      })
      if (difference > 5) {
        let updateStatus = await File.findOneAndUpdate({_id: element._id}, {$set: { status: 'requestDeleteReminder', statusDate: Date.now() }}, {new: true})
        await Notifications.create({title: 'Recordatorio', bodyN: 'Archivo ' + element.fileName + ' en estado de ELIMINACIÓN', link: 'fileManager', organization_id: element.organization_id, target: ids, 'typeN': 'revision', 'optionsN': {'name': findFolder.name, 'id': findFolder._id, 'idFile': element._id}})
        funcUpdateLog(element._id, 'requestDeleteReminder')
      }
    })

    let deleteRemindFiles = await File.find({ status: 'requestDeleteReminder' })
    deleteRemindFiles.map(async element => {
      var ids = []
      var date = new Date(element.statusDate)
      let created = date.toISOString().substring(0, 10)
      
      let date2 = new Date()
      let hoy = date2.toISOString().substring(0, 10)
  
      let a = moment(created)
      let b = moment(hoy)
      var difference = b.diff(a, 'days')
      element.logUsersAccess.forEach(item => {
        if (item.activity == 'add') {
          ids.push(item.userId.toString())
        }
      })
      let findFolder = await Folder.findOne({_id: element.folder})
      if (difference > 3) {
        let updateStatus = await File.findOneAndUpdate({_id: element._id}, {$set: { status: 'canceled', statusDate: Date.now() }}, {new: true})
        await Notifications.create({title: 'Recordatorio', bodyN: 'Archivo ' + element.fileName + ' en estado de CANCELADO', link: 'fileManager', organization_id: element.organization_id, target: ids, 'typeN': 'revision', 'optionsN': {'name': findFolder.name, 'id': findFolder._id, 'idFile': element._id}})
        funcUpdateLog(element._id, 'canceled')
      }
    })

    let concilationDeleteFiles = await File.find({ status: 'conciliationDelete' })
    concilationDeleteFiles.map(async element => {
      var ids = []
      var date = new Date(element.statusDate)
      let created = date.toISOString().substring(0, 10)
      
      let date2 = new Date()
      let hoy = date2.toISOString().substring(0, 10)
  
      let a = moment(created)
      let b = moment(hoy)
      var difference = b.diff(a, 'days')
      let findAreas = await Organization.find({ _id: element.organization_id }, {areas: 1, _id: 0})
      let area = findAreas.pop()
      area.areas.forEach(item => {
        if (item.authorizer != 'null') {
          ids.push(item.authorizer)
        }
      })
      let findFolder = await Folder.findOne({_id: element.folder})
      if (difference > 9) {
        let updateStatus = await File.findOneAndUpdate({_id: element._id}, {$set: { status: 'conciliationDeleteReminder', statusDate: Date.now() }}, {new: true})
        await Notifications.create({title: 'Recordatorio', bodyN: 'Archivo ' + element.fileName + ' en estado de CONCILIACIÓN', link: 'fileManager', organization_id: element.organization_id, target: ids, 'typeN': 'revision', 'optionsN': {'name': findFolder.name, 'id': findFolder._id, 'idFile': element._id}})
        funcUpdateLog(element._id, 'conciliationDeleteReminder')
      } 
    })

    let concilationDeleteRFiles = await File.find({ status: 'conciliationDeleteReminder' })
    concilationDeleteRFiles.map(async element => {
      var ids = []
      var date = new Date(element.statusDate)
      let created = date.toISOString().substring(0, 10)
      
      let date2 = new Date()
      let hoy = date2.toISOString().substring(0, 10)
  
      let a = moment(created)
      let b = moment(hoy)
      var difference = b.diff(a, 'days')
      element.logUsersAccess.forEach(item => {
        if (item.activity == 'add') {
          ids.push(item.userId.toString())
        }
      })
      let findFolder = await Folder.findOne({_id: element.folder})
      if (difference > 3) {
        let updateStatus = await File.findOneAndUpdate({_id: element._id}, {$set: { status: 'canceled', statusDate: Date.now() }}, {new: true})
        await Notifications.create({title: 'Recordatorio', bodyN: 'Archivo ' + element.fileName + ' en estado de CANCELADO', link: 'fileManager', organization_id: element.organization_id, target: ids, 'typeN': 'revision', 'optionsN': {'name': findFolder.name, 'id': findFolder._id, 'idFile': element._id}})
        funcUpdateLog(element._id, 'canceled')
      }
    })
  } catch (err) {

  }
})
