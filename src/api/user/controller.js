import { success, notFound, authorOrAdmin } from '../../services/response/'
import { User } from '.'
import { Organization } from '../organization'
import { Subrisk } from '../subrisk'
import { Question } from '../question'
import { Task } from '../task'
import * as R from 'ramda'
import { secretAccessKeyAWSDevelopment, accessKeyIdAWSDevelopment, secretAccessKeyAWSProduction, accessKeyIdAWSProduction, bucketAWSProfileProduction, bucketAWSProfileDevelopment } from '../../config'
import mongoose, { Collection } from 'mongoose'
import { validationError, validationResponse } from '../../services/response/responsesModules'
const multer = require('multer')
const multerS3 = require('multer-s3')
const aws = require('aws-sdk')
let secretAccessKeyAWS = null
let accessKeyIdAWS = null
let bucketNameAWS = null

var vigModules = [
  'vigAnticorruption',
  'vigAutodiagnostic',
  'vigPenal',
  'vigMoney',
  'vigContinuidad'
]

if (process.env.NODE_ENV === 'production') {
  secretAccessKeyAWS = secretAccessKeyAWSProduction
  accessKeyIdAWS = accessKeyIdAWSProduction
  bucketNameAWS = bucketAWSProfileProduction
} else {
  secretAccessKeyAWS = secretAccessKeyAWSDevelopment
  accessKeyIdAWS = accessKeyIdAWSDevelopment
  bucketNameAWS = bucketAWSProfileDevelopment
}

aws.config.update({
  secretAccessKey: secretAccessKeyAWS,
  accessKeyId: accessKeyIdAWS,
  region: 'us-east-1'
})

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  User.count(query)
    .then(count => User.find(query, select, cursor)
      .then(users => ({
        rows: users.map((user) => user.view()),
        count
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  User.findById(params.id)
    .then(notFound(res))
    .then((user) => user ? user.view() : null)
    .then(success(res))
    .catch(next)

export const showMe = ({ user }, res) =>
  res.json(user.view(true))

export const myData = async ({ user }, res, next) => {
  try {
    let myRes = await User.findOne({_id: user._id})
    // console.log(myRes)
    res.status(200).json({
      _id: myRes._id,
      pais: myRes.pais,
      name: myRes.name,
      email: myRes.email,
      organization_id: myRes.organization_id
    })
  } catch (err) {
    validationError(err, res)
  }
}

export const create = async ({ bodymen: { body } }, res, next) => {
  let session = null
  console.log(body)
  try {
    let ban = false
    //TODO: make transactions using code below
    //create a session 
    //session = await mongoose.startSession()
    //start a new transaction to make a roll back in case of any error
    //await session.startTransaction()
    //await session.commitTransaction()

    // default organization's organization_id
    const ORGANIZATION_ID_DUMMY = '5c883605e9a21f39b8add912'
    // query to check if user's organization already exists on db
    let queryOrganization = { domain: body.organization.domain }

    let [user, organization] = await Promise.all([ User.create({...body}), Organization.findOne(queryOrganization) ])
    let isNewOrganization = false 
    if (organization == null) {
      isNewOrganization = true 
      let organizationDummy = await Organization.findById(ORGANIZATION_ID_DUMMY)
      // create organization
      vigModules.map(vigencias => {
        // console.log(body.organization.premium.hasOwnProperty(vigencias))
        if (body.organization.premium.hasOwnProperty(vigencias) == true) {
        // console.log('Solo existe: ', vigencias)
          body.organization.premium[vigencias] = new Date(body.organization.premium[vigencias])
        // console.log('---->', body.organization.premium)
        }
      })

      organization = await Organization.create({...body.organization, domain: body.organization.domain, questions: organizationDummy.questions, areas: organizationDummy.areas})
      // set default params to organization just created
      organization.params = organizationDummy.params
      organization.save()
      ban = true
    }
////////////////////
    let existsTasks = await Task.find({organization_id: organization._id})
    if (existsTasks.length == 0) {
      await Task.create({
        organization_id: organization._id, moduleId: 'anticorruption',
        'tasks': {
          'data': [
          ],
          'links': [ ]
        }})
      await Task.create({
        organization_id: organization._id, moduleId: 'penal',
        'tasks': {
          'data': [
          ],
          'links': [ ]
        }})
      await Task.create({
        organization_id: organization._id, moduleId: 'money',
        'tasks': {
          'data': [
          ],
          'links': [ ]
        }})
      await Task.create({
        organization_id: organization._id, moduleId: 'continuidad',
        'tasks': {
          'data': [
          ],
          'links': [ ]
        }})
    }
    user.organization_id = organization._id
    user.save()

    let view = await user.view(true)

    if (user) {
      // TODO: Use services/response to send result
      res.status(201).json(view)
    }

    if (ban == true) {
      console.log('BAN:', user)
      let rolesSet = await User.findOneAndUpdate({_id: user._id}, {role: 'admin', roleDocMgt: 'owner'}, {new: true})
      console.log('RESULT', rolesSet)
    }

    // if organization already exists, dont load dummy data.
    if (isNewOrganization === false) {
      return
    }

    // find subrisks dummy and questions dummy in parallel
    let [subrisksDummy, questionsDummy] = await Promise.all([ Subrisk.find({organization_id: mongoose.Types.ObjectId(ORGANIZATION_ID_DUMMY)}, {'_id': 0}),
                                                              Question.find({organization_id: mongoose.Types.ObjectId(ORGANIZATION_ID_DUMMY)}, {'_id': 0})])
  
    // set organization_id to subrisks created
    for (var i = 0; i < subrisksDummy.length; i++) {
      subrisksDummy[i].organization_id = organization._id
    }

    // insert dummy subrisks on db 
    let insertSubrisksResponse  = await Subrisk.insertMany(subrisksDummy, {rawResult: true} )
    
    if (insertSubrisksResponse.result.n > 0) {
      // build catalog subrisks with following structure { _id, title }
      let catalogSubrisks = insertSubrisksResponse.ops.map(element => {
        return R.pick(['_id', 'title'], element)
      })

      // check if questions array has information
      if (questionsDummy.length === 0) {
        return
      }

      questionsDummy.forEach(question => {
        question.organization_id = organization._id

        // if question.linkedSubrisks is an undefined property, skip to the next question
        if (typeof question.linkedSubrisks == 'undefined') {
          return
        }

        // finally we change text data to and id
        /* let linkedSubriskWithIDs = []
        question.linkedSubrisks.forEach(linkedSubrisk => {
          let objresp = catalogSubrisks.find(item => item.title === linkedSubrisk)
          linkedSubriskWithIDs.push(objresp._id)
        })
        question.linkedSubrisks = linkedSubriskWithIDs */
      })
      Question.insertMany(questionsDummy)
    }
  } catch (err) {
    if (err.name == 'ValidationError') {
      res.status(422).json({
        valid: false,
        name: err.name,
        message: err.message
      })
    }
    /* istanbul ignore else */
    if (err.name === 'MongoError' && err.code === 11000) {
      res.status(409).json({
        valid: false,
        param: 'email',
        message: 'El correo electrónico ya está registrado'
      })
    } else {
      next(err)
    }
  }
}

export const update = ({ bodymen: { body }, params, user }, res, next) =>
  User.findById(params.id === 'me' ? user.id : params.id)
    .then(notFound(res))
    .then((result) => {
      if (!result) return null
      const isAdmin = user.role === 'admin'
      const isOwner = user.role === 'owner'
      const isSuperAdmin = user.role === 'superadmin'
      const isSelfUpdate = user.id === result.id
      if (!isSelfUpdate && !isAdmin && !isOwner && !isSuperAdmin) {
        res.status(401).json({
          valid: false,
          message: 'No puedes cambiar los datos de otro usuario'
        })
        return null
      }

      result.name = (body.name !== null && body.name !== undefined) ? body.name : result.name
      result.phone = (body.phone !== null && body.phone !== undefined) ? body.phone : result.phone
      result.mobile = (body.mobile !== null && body.mobile !== undefined) ? body.mobile : result.mobile
      result.picture = (body.picture !== null && body.picture !== undefined) ? body.picture : result.picture
      result.ocupation = (body.ocupation !== null && body.ocupation !== undefined) ? body.ocupation : result.ocupation
      result.roleDocMgt = (body.roleDocMgt !== null && body.roleDocMgt !== undefined) ? body.roleDocMgt : result.roleDocMgt
      result.authorizer = (body.authorizer !== null && body.authorizer !== undefined) ? body.authorizer : result.authorizer
      result.pais = (body.pais !== null && body.pais !== undefined) ? body.pais : result.pais
      result.isAuditor = (body.isAuditor !== null && body.isAuditor !== undefined) ? body.isAuditor : result.isAuditor
      return result
    })
    .then((user) => user ? user.save() : null)
    .then((user) => user ? user.view(true) : null)
    .then(success(res))
    .catch(next)

export const updatePassword = ({ bodymen: { body }, params, user }, res, next) =>
  User.findById(params.id === 'me' ? user.id : params.id)
    .then(notFound(res))
    .then((result) => {
      if (!result) return null
      const isSelfUpdate = user.id === result.id
      if (!isSelfUpdate) {
        res.status(401).json({
          valid: false,
          param: 'password',
          message: 'No puedes cambiar la contraseña de otro usuario'
        })
        return null
      }
      return result
    })
    .then((user) => user ? user.set({ password: body.password }).save() : null)
    .then((user) => user ? user.view(true) : null)
    .then(success(res))
    .catch(next)

export const setPassword = ({ bodymen: { body }, params, user }, res, next) =>
  User.findById(params.id)
    .then(notFound(res))
    .then((user) => user ? user.set({ password: body.password }).save() : null)
    .then((user) => user ? user.view(true) : null)
    .then(async (user) => {
      let expirationDateLink = new Date(+new Date() - 2*24*60*60*1000)
      return await User.findOneAndUpdate({ email: user.email }, { $set: {'expirationDateLink': expirationDateLink} }, {new: true})
    })
    .then(success(res))
    .catch(next)

export const getOrgMembers = ({ params, user }, res, next) =>{
  User.find({organization_id: user.organization_id, password: {$ne: undefined}})  
    .then(users => {
      users = users.map(user1 => {
        var {_id, name, role, picture, roleDocMgt, ocupation, isAuditor} = user1
        return {_id, name, role, picture, roleDocMgt, ocupation, isAuditor} 
      })
      return {users, idOwner: user._id}
    })
    .then(success(res))
    .catch(next)
}

export const destroy = ({ params, user }, res, next) =>
  User.findById(params.id)
    .populate('create_by')
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'create_by'))
    .then((user) => user ? user.remove() : null)
    .then(success(res, 204))
    .catch(next)


const getTitleAndIdSubrisks = (organizationId) => {
  let query = { organization_id: organizationId }
  return Subrisk.find(query, { _id: 1, title: 1 })
}

export const getProImg = async ({ querymen: { query } }, res, next) => {
  var s3 = new aws.S3()
  console.log(query.id)
  var param = {
    Bucket: bucketNameAWS,
    Key: query.id
  }

  s3.getObject(param)
    .on('httpHeaders', function (statusCode, headers) {
      res.set('Content-Length', headers['content-length'])
      res.set('Content-Type', headers['x-amz-meta-contenttype'])
      res.attachment(headers['x-amz-meta-fieldname'])
      this.response.httpResponse.createUnbufferedStream()
        .pipe(res)
    })
    .send()
}

const s3 = new aws.S3()
export const uploadProfileImage = multer({
  storage: multerS3({
    s3: s3,
    bucket: bucketNameAWS,
    acl: 'public-read',
    contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata: function (req, file, cb) {
      cb(null, {fieldName: file.originalname, contentType: file.mimetype})
    },
    key: function (req, file, cb) {
      cb(null, Date.now().toString())
    }
  })
})

export const saveProfileImage = async (url, id) => {
  try {
    let saveImgUrl = await User.findOneAndUpdate({_id: id}, { $set: {picture: url} }, {new: true})
  } catch (err) {
    return err
  }
}

/**
 * Esta función permite convertir un objecto con estrutura JSON a notación punto.
 * Ejemplo: Si tenemos
 *               { address: { street: 'A', number: 1 } }
 *          pasara á
 *               { 'address.street': 'A', 'address.number': 1 }
 *
 * @param {Object} obj Objeto a convertir
 * @param {Object} target Objecto resultante
 * @param {String} prefix Cadena auxiliar para concatenar las propiedades
 */
const dotNotate = (obj, target, prefix) => {
  target = target || {},
  prefix = prefix || "";
  Object.keys(obj).forEach(function(key) {
    if ( typeof(obj[key]) === "object" ) {
      dotNotate(obj[key],target,prefix + key + ".")
    } else {
      return target[prefix + key] = obj[key]
    }
  })

  return target
}

export const setFlags = async ({ bodymen: { body }, user }, res, next) => {
  try {
    let setter = { $set: dotNotate(body) }

    let updateFlag = await User.findOneAndUpdate({_id: user._id}, setter, {new: true})
    let resultValidation = validationResponse(updateFlag, res)
    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}

export const assignRole = async ({ bodymen: { body }, params }, res, next) => {
  try {
    let setRole = await User.findOneAndUpdate({_id: params.id}, {$set: {role: body.role}}, {new: true})
    let resultValidation = validationResponse(setRole, res)
    resultValidation ? res.json(resultValidation) : null
  } catch (err) {
    validationError(err, res)
  }
}

export const existsMail = async ({ querymen: { query } }, res, next) => {
  try {
    let respMail = await User.findOne({ email: query.email })
    if (respMail) {
      res.status(200).json({id: respMail.id})
    } else {
      res.status(404).json({message: 'Email not found'})
    }
  } catch (err) {
    validationError(err, res)
  }
}

export const expirationDateLink = async ({ querymen: { query } }, res, next) => {
  try {
    let respMail = await User.findById(query.id)
    let resultValidation = validationResponse(respMail, res)
    resultValidation ? res.json(resultValidation.viewIsLinkExpired()) : null
  } catch (err) {
    validationError(err, res)
  }
}

// se ocupa al enviar mail de autenticación
export const setExpiredPass = async ({ email }, res) => {
  try {
    let expirationDateLink = new Date(+new Date() + 2*24*60*60*1000) // link valid up to 48 hours
    let respMail = await User.findOneAndUpdate({ email: email }, { $set: {'expirationDateLink': expirationDateLink} }, {new: true})
    let resultValidation = validationResponse(respMail, res)
    resultValidation ? res.json({response: 'success'}) : null
  } catch (err) {
    throw err
  }
}

export const setExpiredPassinYesterday = async ({ email }, res) => {
  try {
    let expirationDateLink = new Date(+new Date() - 2*24*60*60*1000) // link valid up to 48 hours
    await User.findOneAndUpdate({ email: email }, { $set: {'expirationDateLink': expirationDateLink} }, {new: true})
  } catch (err) {
    throw err
  }
}
