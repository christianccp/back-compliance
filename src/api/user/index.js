import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { password as passwordAuth, master, token } from '../../services/passport'
import { myData, expirationDateLink, setEmptyPass, existsMail, assignRole, setFlags, saveProfileImage, uploadProfileImage, getProImg, index, showMe, show, create, update, updatePassword, destroy, setPassword, getOrgMembers } from './controller'
import { schema } from './model'
import { validationError, validationResponse } from '../../services/response/responsesModules'
// Import schema to trigger the organization's automatic creation
import { schema as orgSchema } from '../organization'

export User, { schema } from './model'

const router = new Router()

// grabs schema fields from the organization model
const { pais, authorizer, roleDocMgt, domain, nameorg, numEmployments, income, url, zipcode, moduleTarget, premium, params } = orgSchema.tree
const { expiredLinkByPass, isAuditor, firstTime, evaluate, hideMessageEditSubrisk, hideTutorialTour, flags, id, email, password, name, phone, mobile, picture, role, ocupation } = schema.tree

/**
 * @api {get} /users Consultar usuarios
 * @apiName RetrieveUsers
 * @apiGroup User
 * @apiPermission admin
 * @apiParam {String} access_token Token de acceso de administrador.
 * @apiUse listParams
 * @apiSuccess {Object[]} users Lista de usuarios. Cada objeto user devolverá los siguientes campos: id, name, phone, mobile, organization_id, picture.
 * @apiError {Object} 400 Algunos parametros contienen valores invalidos.
 * @apiError 401 Solo acceso a administradores.
 */
router.get('/',
  token({ required: true, roles: ['superadmin', 'admin'] }),
  query(),
  index)

/**
 * @api {get} /users/me Consultar usuario actual.
 * @apiName RetrieveCurrentUser
 * @apiGroup User
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiSuccess {Object} user Datos de usuario.
 */
router.get('/me',
  token({ required: true }),
  showMe)

router.get('/myData',
  token({ required: true }),
  myData)

/**
 * @api {get} /users/me Consultar usuario actual.
 * @apiName RetrieveCurrentUser
 * @apiGroup User
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiSuccess {Object} user Datos de usuario.
 */
router.get('/getOrgMembers',
  token({ required: true }),
  getOrgMembers)

/**
 * @api {get} /users/:id Consultar usuario
 * @apiName RetrieveUser
 * @apiGroup User
 * @apiPermission public
 * @apiSuccess {Object} user Datos de usuario.
 * @apiError 404 User no se encuentra.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {post} /users Crear usuario
 * @apiName CreateUser
 * @apiGroup User
 * @apiPermission master
 * @apiParam {String} access_token Master access token.
 * @apiParam {String} email E-mail de usuario.
 * @apiParam {String{6..}} password Contraseña de usuario.
 * @apiParam {String} [name] Nombre de usuario.
 * @apiParam {String} phone Teléfono de usuario.
 * @apiParam {String} mobile Celular de usuario.
 * @apiParam {Object} organization Ver Organization Object para mas detalles.
 * @apiParam {String} [picture] Imagen de usuario.
 * @apiParam {String=user,admin} [role=user] Rol de usuario.
 * @apiParam (Organization Object) {String} domain Dominio de la organización.
 * @apiParam (Organization Object) {String} name Nombre de la organización.
 * @apiParam (Organization Object) {String} numEmployments Número de empleados en la organización.
 * @apiParam (Organization Object) {String} income Ingresos de la organización.
 * @apiParam (Organization Object) {String} url Url de la organización.
 * @apiParam (Organization Object) {Number} zipcode Código postal de la organización.
 * @apiParam (Organization Object) {String} moduleTarget Módulo destino.
 * @apiParam (Organization Object) {Boolean} premium Premium.
 * @apiParamExample [json] Resquest-ejemplo
 * {
 * "access_token": "9.....",
 * "name": "Alex",
 * "email": "ejemplo@ejem.com",
 *   "password": "123456",
 *   "phone": "5560116967",
 *   "mobile": "5560116967",
 *   "ocupation": "-Liv",
 *   "organization": {
 *    "domain": "ejem.com",
 *    "nameorg":"Organizacion Ejemplo",
 *    "numEmployments": 10,
 *    "income":1000,
 *    "url":"www.ejem.com",
 *    "zipcode":3000,
 *    "moduleTarget": "DDD",
 *    "premium": true
 *    }
 * }
 * @apiSuccess (Sucess 201) {Object} user Datos de usuario.
 * @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 401 Solo acceso Maestro.
 * @apiError 409 Email ya registrado.
 */
router.post('/',
  master(),
  body({ pais, email, name, phone, mobile, ocupation, organization: {domain, nameorg, numEmployments, income, url, zipcode, moduleTarget, premium, params}, picture, role, isAuditor, flags: { firstTime, evaluate, hideMessageEditSubrisk, hideTutorialTour } }),
  create)

/**
 * @api {put} /users/:id Actualizar usuario
 * @apiName UpdateUser
 * @apiGroup User
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiParam {String} name Nombre de usuario.
 * @apiParam {String} phone Teléfono de usuario.
 * @apiParam {String} mobile Celular de usuario.
 * @apiParam {String} [picture] Imagen de usuario.
 * @apiSuccess {Object} user Datos de usuario.
 * @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 401 Solo acceso a usuario o administrador actual.
 * @apiError 404 User no se encontró.
 */
router.put('/:id',
  token({ required: true }),
  body({ name, phone, mobile, picture, ocupation, roleDocMgt, authorizer, pais, isAuditor }),
  update)

/**
 * @api {put} /users/:id/password Actualizar contraseña
 * @apiName UpdatePassword
 * @apiGroup User
 * @apiHeader {String} Authorization Autorización basica con correo y contraseña.
 * @apiParam {String{6..}} password Nueva contraseña de usuario.
 * @apiSuccess (Success 201) {Object} user Datos de usuario.
* @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 401 Solo acceso a usuario actual.
 * @apiError 404 User no se encontró.
 */
router.put('/:id/password',
  passwordAuth(),
  body({ password }),
  updatePassword)

router.put('/:id/setPassword',
  body({ password }),
  setPassword)

router.put('/set/Flag',
  token({ required: true }),
  body({ flags }),
  setFlags)

router.put('/assignRole/:id',
  token({required: true, roles: ['superadmin', 'admin']}),
  body({ role }),
  assignRole)

/**
 * @api {delete} /users/:id Eliminar usuario
 * @apiName DeleteUser
 * @apiGroup User
 * @apiPermission admin
 * @apiParam {String} access_token Token de acceso de administrador.
 * @apiSuccess (Success 204) 204 Sin contenido.
 * @apiError 401 Solo acceso de administrador.
 * @apiError 404 User no se encontró.
 */

router.get('/getImage/profile',
  query({ id }),
  token({ required: true }),
  getProImg)

const singleUpload = uploadProfileImage.array('file')
router.post('/profile/image',function (req, res) {
  singleUpload(req, res, function (err, some) {
    if (err) {
      return res.status(422).send({ errors: [{title: 'Image Upload Error', detail: err.message}] })
    } else {
      saveProfileImage(req.files[0].location, req.body._id)
      return res.json({'uploadedFiles': req.files})
    }
  })
})

router.get('/user/exist',
  query({ email }),
  existsMail)

router.get('/expiration/DateLink',
  query({ id }),
  expirationDateLink)


router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
