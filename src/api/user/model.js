import crypto from 'crypto'
import bcrypt from 'bcrypt'
import mongoose, { Schema } from 'mongoose'

import mongooseKeywords from 'mongoose-keywords'
import { env } from '../../config'

const roles = ['user', 'admin', 'superadmin']
const rolesMgt = ['owner', 'lead', 'admin', 'user']

const userSchema = new Schema({
  flags: {
    firstTime: {
      type: Boolean,
      default: false
    }, 
    evaluate: {
      type: Boolean,
      default: false
    }, 
    hideMessageEditSubrisk: {
      type: Boolean,
      default: false
    },
    hideTutorialTour: {
      type: Boolean,
      default: false
    },
    dashboard: {
      taskToDo: {
        label:{
          type: String,
          default: 'Tarjeta Actividades por Realizar'
        },
        show: {
          type: Boolean,
          default: true
        }
      },
      mediumInhRisk: {
        label:{
          type: String,
          default: 'Tarjeta Total de Subriesgos Nivel Inherente Medio'
        },
        show: {
          type: Boolean,
          default: true
        }
      },
      highInhRisk: {
        label:{
          type: String,
          default: 'Tarjeta Total de Subriesgos Nivel Inherente Alto'
        },
        show: {
          type: Boolean,
          default: true
        }
      },
      taskPercentage: {
        label:{
          type: String,
          default: 'Tarjeta Porcentaje Total de Tareas Terminadas'
        },
        show: {
          type: Boolean,
          default: true
        }
      },
      controlPercentage: {
        label:{
          type: String,
          default: 'Tarjeta Porcentaje Total de Control'
        },
        show: {
          type: Boolean,
          default: true
        }
      },
      controlVsNoControl: {
        label:{
          type: String,
          default: 'Tarjeta Porcentaje de Control vs No Control'
        },
        show: {
          type: Boolean,
          default: true
        }
      },
      residualRiskGraph: {
        label:{
          type: String,
          default: 'Gráfica de Riesgo Residual'
        },
        show: {
          type: Boolean,
          default: true
        }
      },
      inherentRiskGraph: {
        label:{
          type: String,
          default: 'Gráfica de Riesgo Inherente'
        },
        show: {
          type: Boolean,
          default: true
        }
      },
      actionPlan: {
        label:{
          type: String,
          default: 'Tablero Plan de acción'
        },
        show: {
          type: Boolean,
          default: true
        }
      },
    }
  },
  email: {
    type: String,
    match: /^\S+@\S+\.\S+$/,
    required: true,
    unique: true,
    trim: true,
    lowercase: true
  },
  password: {
    type: String,
    required: false,
    minlength: 6
  },
  name: {
    type: String,
    index: true,
    trim: true
  },
  phone: {
    type: Number
  },
  mobile: {
    type: Number
  },
  organization_id: {
    type: Schema.Types.ObjectId,
    ref: 'Organization'
  },
  pais: {
    type: String,
    default: 'none'
  },
  isAuditor: {
    type: Boolean,
    default: false
  },
  role: {
    type: String,
    enum: roles,
    default: 'user'
  },
  picture: {
    type: String,
    trim: true
  },
  ocupation: {
    type: String
  },
  expirationDateLink: {
    type: Date
  },
  roleDocMgt: {
    type: String,
    enum: rolesMgt,
    default: 'user'
  }
}, {
  timestamps: true
})

userSchema.path('email').set(function (email) {
  if (!this.picture || this.picture.indexOf('https://gravatar.com') === 0) {
    const hash = crypto.createHash('md5').update(email).digest('hex')
    this.picture = `https://gravatar.com/avatar/${hash}?d=identicon`
  }

  if (!this.name) {
    this.name = email.replace(/^(.+)@.+$/, '$1')
  }

  return email
})

userSchema.pre('save', function (next) {
  console.log('Entre al PRE')
  if (!this.isModified('password')) return next()

  /* istanbul ignore next */
  const rounds = env === 'test' ? 1 : 9

  bcrypt.hash(this.password, rounds).then((hash) => {
    this.password = hash
    next()
  }).catch(next)
})

userSchema.methods = {
  view (full) {
    let view = {}
    let fields = ['flags', 'id', 'role', 'authorizer', 'roleDocMgt', 'name', 'pais', 'phone', 'mobile', 'picture', 'createdAt', 'isAuditor']

    if (full) {
      fields = [...fields, 'email', 'ocupation','organization_id']
    }
    
    fields.forEach((field) => { 
      view[field] = this[field]
    })

    return view
  },

  viewIsLinkExpired() {
    let isLinkExpired = Date.now() > new Date(this['expirationDateLink']) ? true : false 
    let view = { isLinkExpired }
    return view
  },
  authenticate (password) {
    return bcrypt.compare(password, this.password).then((valid) => valid ? this : false)
  }
}

userSchema.statics = {
  roles
}

userSchema.plugin(mongooseKeywords, { paths: ['email', 'name'] })

const model = mongoose.model('User', userSchema)

export const schema = model.schema
export default model
