import mongoose, { Schema } from 'mongoose'

const impactAvgSchema = new Schema({
  value: {
    type: Number,
    default: 0
  },
  label: {
    type: String
  }
})

const levelInhAvgSchema = new Schema({
  value: {
    type: Number,
    default: 0
  },
  label: {
    type: String
  }
})

const probabilityAvgSchema = new Schema({
  value: {
    type: Number,
    default: 0
  },
  label: {
    type: String
  }
})

const levelResidualAvgSchema = new Schema({
  value: {
    type: Number,
    default: 0
  },
  label: {
    type: String
  }
})

const procederSchema = new Schema({
  value: {
    type: Number,
    default: 0
  },
  label: {
    type: String
  }
})

const paramsSchema = new Schema({
  title: {
    type: String
  },
  impact: {
    type: {impactAvgSchema}
  },
  level: {
    type: {levelInhAvgSchema}
  },
  probability: {
    type: {probabilityAvgSchema}
  },
  moduleId: {
    type: String,
    required: true
  }
})

const subriskSchema = new Schema({
  organization_id: {
    type: Schema.ObjectId,
    // ref: 'User',
    required: true
  },
  moduleId: {
    type: String,
    required: true
  },
  active: {
    type: Boolean,
    default: false
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  sanction: {
    type: String,
    required: true
  },
  impactAvg: {
    type: {impactAvgSchema}
  },
  levelInhAvg: {
    type: {levelInhAvgSchema}
  },
  probabilityAvg: {
    type: {probabilityAvgSchema}
  },
  levelResidualAvg: {
    type: {levelResidualAvgSchema}
  },
  proceder: {
    type: {procederSchema}
  },
  params: {
    type: [paramsSchema]
  },
  quizAssigned: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

subriskSchema.methods = {
  view (full) {
    const view = {
      // simple view
      _id: this._id,
      organization_id: this.organization_id,
      moduleId: this.moduleId,
      active: this.active,
      title: this.title,
      description: this.description,
      sanction: this.sanction,
      impactAvg: this.impactAvg,
      levelInhAvg: this.levelInhAvg,
      probabilityAvg: this.probabilityAvg,
      levelResidualAvg: this.levelResidualAvg,
      proceder: this.proceder,
      params: this.params,
      quizAssigned: this.quizAssigned
      // organization_id: this.user.organization_id
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  },
  viewParams () {
    const view = {
      params: this.params
    }
    return view
  }
}

subriskSchema.pre('insertMany', function(next) {
  console.log('pre insertMany .... ', this.doc)
/*   var now = new Date()

  if(!this.createdAt) {
    this.createdAt = now
  } */

  next()
})

const model = mongoose.model('Subrisk', subriskSchema)

export const schema = model.schema
export default model
