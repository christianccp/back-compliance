import request from 'supertest'
import { apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Risk } from '.'

const app = () => express(apiRoot, routes)

let userSession, risk

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  userSession = signSync(user.id)
  risk = await Risk.create({})
})

test('POST /risk 201 (user)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession, subrisk: 'test', description: 'test', sanction: 'test', impact: 'test', probability: 'test', level: 'test', active: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.subrisk).toEqual('test')
  expect(body.description).toEqual('test')
  expect(body.sanction).toEqual('test')
  expect(body.impact).toEqual('test')
  expect(body.probability).toEqual('test')
  expect(body.level).toEqual('test')
  expect(body.active).toEqual('test')
})

test('POST /risk 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /risk 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /risk 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /risk/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${risk.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(risk.id)
})

test('GET /risk/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${risk.id}`)
  expect(status).toBe(401)
})

test('GET /risk/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /risk/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${risk.id}`)
    .send({ access_token: userSession, subrisk: 'test', description: 'test', sanction: 'test', impact: 'test', probability: 'test', level: 'test', active: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(risk.id)
  expect(body.subrisk).toEqual('test')
  expect(body.description).toEqual('test')
  expect(body.sanction).toEqual('test')
  expect(body.impact).toEqual('test')
  expect(body.probability).toEqual('test')
  expect(body.level).toEqual('test')
  expect(body.active).toEqual('test')
})

test('PUT /risk/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${risk.id}`)
  expect(status).toBe(401)
})

test('PUT /risk/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: userSession, subrisk: 'test', description: 'test', sanction: 'test', impact: 'test', probability: 'test', level: 'test', active: 'test' })
  expect(status).toBe(404)
})

test('DELETE /risk/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${risk.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /risk/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${risk.id}`)
  expect(status).toBe(401)
})

test('DELETE /risk/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})
