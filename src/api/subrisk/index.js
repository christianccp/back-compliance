import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, showParams, update, updateParams, setResidualLevel, setProceder, destroy, addParams, updateParameters, deleteParams } from './controller'
import { schema } from './model'
export Subrisk, { schema } from './model'

const router = new Router()
const { _id, moduleId, title, active, sanction, description, impactAvg, levelInhAvg, probabilityAvg, levelResidualAvg, proceder, params, impact, level, probability, quizAssigned } = schema.tree
const { value, label } = schema.tree
/**
 * @api {post} /subrisk Crear subriesgo
 * @apiName CreateSubrisk
 * @apiGroup Subrisk
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiParam {String} title Titulo de subriesgo.
 * @apiParam {Boolean} active Activo.
 * @apiParam {String} sanction Sanción.
 * @apiParam {String} description Descripción del subriesgo.
 * @apiParam {String} moduleId Módulo del subriesgo.
 * @apiParam {Object} impactAvg Ver Impact Object para mas detalles.
 * @apiParam {Object} probabilityAvg Ver Probability Object para mas detalles.
 * @apiParam {Object} levelAvg Ver Level Object para mas detalles.
 * @apiParam {Object} levelResidualAvg Ver Level Residual Object para mas detalles.
 * @apiParam {Object[]} params Ver Params Object para mas detalles.
 * @apiParam (Impact Object) {String} label Título de impacto.
 * @apiParam (Impact Object) {Number} value Valor de impacto.
 * @apiParam (Probability Object) {String} label Títutlo de probalilidad.
 * @apiParam (Probability Object) {Number} value Valor de probabilidad.
 * @apiParam (Level Object) {String} label Título de nivel.
 * @apiParam (Level Object) {Number} value Valor de nivel.
 * @apiParam (Level Residual Object) {String} label Título de nivel.
 * @apiParam (Level Residual Object) {Number} value Valor de nivel.
 * @apiParam (Params Object) {String} title Título de parámetro.
 * @apiParam (Params Object) {Number} impact Impacto del parámetro.
 * @apiParam (Params Object) {Number} level Nivel de parametro.
 * @apiParam (Params Object) {Number} probability Probabilidad de parametro.
 * @apiParamExample [json] Resquest-ejemplo
 *  {
 *    "access_token": "123-...",
 *      "title": "Soborno",
 *      "moduleId": "anticurrupcion",
 *      "description": "El soborno se considera una mala práctica",
 *      "sanction": "Carcel",
 *      "active": false,
 *      "impactAvg" : {
 *        "value": 2,
 *        "label": "Bajo"
 *      },
 *      "probabilityAvg": {
 *        "value": 2,
 *        "label": "Bajo"
 *      },
 *      "levelInhAvg": {
 *       "value": 2,
 *       "label": "Bajo"
 *      },
 *      "levelResidualAvg": {
 *       "value": 2,
 *       "label": "Bajo"
 *      },
 *      "params": {
 *          "title": "Reputación de la empresa",
 *          "impact": 1,
 *          "probability": 1,
 *          "level": 12
 *        }
 *  }
 * @apiSuccess {Object} subrisk Datos de subriesgo.
 * @apiError {Object} 400  Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Question no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.post('/',
  token({ required: true }),
  body({moduleId, title, active, sanction, description, impactAvg, levelInhAvg, probabilityAvg, levelResidualAvg, params, quizAssigned}),
  create)

/**
 * @api {get} /subrisk Consultar subriesgos
 * @apiName RetrieveSubrisks
 * @apiGroup Subrisk
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiUse listParams
 * @apiSuccess {Object[]} risks Lista de subriesgos.
 * @apiError {Object} 400  Algunos parametros pueden contener valores invalidos.
 * @apiError 401 Solo acceso a usuarios.
 */
router.get('/',
  token({ required: true }),
  query({
    moduleId
  }),
  index)

/**
 * @api {get} /risk/:id Consultar subriesgo
 * @apiName RetrieveSubrisk
 * @apiGroup Subrisk
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiSuccess {Object} risk Datos de subriesgo.
 * @apiError {Object} 400  Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Question no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.get('/filter/:moduleId',
  token({ required: true }),
  show)

/**
 * @api {get} /subrisk/params/:id Consultar parametros de subriesgo
 * @apiName GetSubrisk'sParams
 * @apiGroup Subrisk
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiSuccess {Object[]} params Parametros de subriesgo
 * @apiError {Object} 400  Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Question no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.get('/params/:id',
  token({ required: true }),
  showParams)

/**
 * @api {put} /subrisk/:id Actualizar subriesgo
 * @apiName UpdateSubrisk
 * @apiGroup Subrisk
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiParam {String} title Titulo de subriesgo.
 * @apiParam {Boolean} active Activo.
 * @apiParam {String} sanction Sanción.
 * @apiParam {String} description Descripción del subriesgo.
 * @apiParam {String} moduleId Módulo del subriesgo.
 * @apiParam {Object} impactAvg Ver Impact Object para mas detalles.
 * @apiParam {Object} probabilityAvg Ver Probability Object para mas detalles.
 * @apiParam {Object} levelAvg Ver Level Object para mas detalles.
 * @apiParam {Object} levelResidualAvg Ver Level Residual Object para mas detalles.
 * @apiParam {Object[]} params Ver Params Object para mas detalles.
 * @apiParam (Impact Object) {String} label Título de impacto.
 * @apiParam (Impact Object) {Number} value Valor de impacto.
 * @apiParam (Probability Object) {String} label Títutlo de probalilidad.
 * @apiParam (Probability Object) {Number} value Valor de probabilidad.
 * @apiParam (Level Object) {String} label Título de nivel.
 * @apiParam (Level Object) {Number} value Valor de nivel.
 * @apiParam (Level Residual Object) {String} label Título de nivel.
 * @apiParam (Level Residual Object) {Number} value Valor de nivel.
 * @apiParam (Params Object) {String} title Título de parámetro.
 * @apiParam (Params Object) {Number} impact Impacto del parámetro.
 * @apiParam (Params Object) {Number} level Nivel de parametro.
 * @apiParam (Params Object) {Number} probability Probabilidad de parametro.
 * @apiParamExample [json] Resquest-ejemplo
 *  {
 *    "access_token": "123-...",
 *      "title": "Soborno",
 *      "moduleId": "anticurrupcion",
 *      "description": "El soborno se considera una mala práctica",
 *      "sanction": "Carcel",
 *      "active": false,
 *      "impactAvg" : {
 *        "value": 2,
 *        "label": "Bajo"
 *      },
 *      "probabilityAvg": {
 *        "value": 2,
 *        "label": "Bajo"
 *      },
 *      "levelInhAvg": {
 *       "value": 2,
 *       "label": "Bajo"
 *      },
 *      "levelResidualAvg": {
 *       "value": 2,
 *       "label": "Bajo"
 *      },
 *      "params": {
 *          "title": "Reputación de la empresa",
 *          "impact": 1,
 *          "probability": 1,
 *          "level": 12
 *        }
 *  }
 * @apiSuccess {Object} subrisk Datos de subriesgo.
 * @apiError {Object} 400  Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Question no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.put('/:id',
  token({ required: true }),
  body({ title, active, sanction, description, impactAvg, levelInhAvg, probabilityAvg, levelResidualAvg, quizAssigned }),
  update)

router.put('/:id/addParams',
  token({ required: true }),
  body({ impactAvg, levelInhAvg, probabilityAvg, params }),
  addParams)

router.put('/:id/updateParameters',
  token({ required: true }),
  body({ impactAvg, levelInhAvg, probabilityAvg, params: { _id, impact, level, probability, title, moduleId } }),
  updateParameters)

router.put('/:id/deleteParams',
  token({ required: true }),
  body({ impactAvg, levelInhAvg, probabilityAvg, params: { _id, impact, level, probability, title, moduleId } }),
  deleteParams)
/**
 * @api {put} /risk/:organizationId/subrisks Actualizar subriesgo por OrgId
 * @apiName UpdateSubRisks
 * @apiGroup Subrisk
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiParam subrisk Risk's subrisk array
 * @apiSuccess {Object} risk Risk's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Risk not found.
 * @apiError 401 user access only.
 */
/* router.put('/:organizationId/subrisks',
  token({ required: true }),
 // body({ subrisk }),
  updateSubrisks) */

/**
 * @api {put} /risk/:subrisk/subrisk Update subrisk params
 * @apiName UpdateParams by subrisk
 * @apiGroup Subrisk
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam {Object[]}subrisk Risk's subrisk array
 * @apiSuccess {Na} na No data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Risk not found.
 * @apiError 401 user access only.
 */
/* router.put('/:subriskId/subrisk',
  token({ required: true }),
  //body({subrisk: subrisk}),
  updateSubrisk) */

/**
 * @api {put} /subrisk/:organizationId/addsubrisk Agregar subriesgo a una organización
 * @apiName AddSubrisk by organizationId
 * @apiGroup Subrisk
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam {Object} subrisk Subrisk object data. See Subrisk Object for more details below.
 * @apiParam (Subrisk Object) {String} title Subrisk's title.
 * @apiParam (Subrisk Object) {String} description Subrisk's description.
 * @apiParam (Subrisk Object) {String} sanction Subrisk's sanction.
 * @apiParam (Subrisk Object) {Boolean} [active="false"] Indicate if the subrisk will be evaluated.
 * @apiParam (Subrisk Object) {String} [levelResidualAvg] Subrisk's avegare residual level.
 * @apiParam (Subrisk Object) {Object} [impactAvg] Subrisk's avegare impacts. This value should be got the sum of value of each impact in Params Object, divided by total of impacts. See Impact Object for more details below.
 * @apiParam (Subrisk Object) {Object} [probabilityAvg] Subrisk's avegare probabilities. This value should be got the sum of value of each probability in Params Object, divided by total of probabilities. See Probability Object for more details below.
 * @apiParam (Subrisk Object) {Object} [levelInhAvg] Subrisk's avegare levels. This value should be got the sum of value of each level in Params Object, divided by total of levels. See Level Object for more details below.
 * @apiParam (Subrisk Object) {Object[]} [params] Subrisk's params object. See Params Object for more details below.
 * @apiParam (Impact Object) {String} label Impact's title.
 * @apiParam (Impact Object) {Number} value Impact's value.
 * @apiParam (Probability Object) {String} label Probability's title.
 * @apiParam (Probability Object) {Number} value Probability's value.
 * @apiParam (Level Object) {String} label Level's title.
 * @apiParam (Level Object) {Number} value Level's value.
 * @apiParam (Params Object) {String} title Param's title.
 * @apiParam (Params Object) {Number} impact Param's impact.
 * @apiParam (Params Object) {Number} probability Param's probability.
 * @apiParam (Params Object) {Number} level Param's level.
 * @apiParamExample [json] Resquest-ejemplo
 *  {
 *    "access_token": "123-...",
 *    "subrisk": {
 *      "id": "123",
 *      "title": "Soborno",
 *      "description": "El soborno se considera una mala práctica",
 *      "sanction": "Carcel",
 *      "levelResidualAvg": 12,
 *      "active": false,
 *      "impactAvg" : {
 *        "value": 2,
 *        "label": "Bajo"
 *      },
 *      "probabilityAvg": {
 *        "value": 2,
 *        "label": "Bajo"
 *      },
 *      "levelInhAvg": {
 *       "value": 2,
 *       "label": "Bajo"
 *      }
 *      "params": [
 *        {
 *          "title": "Reputación de la empresa",
 *          "impact": 1,
 *          "probability": 1,
 *          "level": 12
 *        },
 *      ]
 *    }
 *  }
 * @apiSuccess {Na} na No data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Risk not found.
 * @apiError 401 user access only.
 */
/* router.put('/:organizationId/addsubrisk',
  token({ required: true }),
  //body({subrisk: {title, description, sanction, levelResidualAvg, impactAvg: {}, probabilityAvg: {}, levelInhAvg: {}, active}}),
  addSubrisk) */

// TODO: Update documentation. Params Object Change
/**
 * @api {put} /subrisk/:subrisk/params Actualizar parámetros de subriesgo
 * @apiName UpdateSubrisk'sParams
 * @apiGroup Subrisk
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiParam {Object} levelInhAvg Ver objecto Level Object para mas detalles.
 * @apiParam {Object} impactAvg Ver objecto Impact Object para mas detalles.
 * @apiParam {Object} probabilityAvg Ver Probability Object para mas detalles.
 * @apiParam {Object[]} params Ver objecto Params Object para mas detalles.
 * @apiParam (Impact Object) {String} label Título de impacto.
 * @apiParam (Impact Object) {Number} value Valor de impacto.
 * @apiParam (Probability Object) {String} label Títutlo de probalilidad.
 * @apiParam (Probability Object) {Number} value Valor de probabilidad.
 * @apiParam (Level Object) {String} label Título de nivel.
 * @apiParam (Level Object) {Number} value Valor de nivel.
 * @apiParam (Params Object) {String} title Título de parámetro.
 * @apiParam (Params Object) {Number} impact Impacto del parámetro.
 * @apiParam (Params Object) {Number} level Nivel de parametro.
 * @apiParam (Params Object) {Number} probability Probabilidad de parametro.
 * @apiSuccess {Na} na Sin datos.
 * @apiError {Object} 400  Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Question no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.put('/updateparams/:id',
  token({ required: true }),
  body({ levelInhAvg: { value }, impactAvg: { value }, probabilityAvg: { value }, params: [] }),
  updateParams)


  /**
 * @api {put} /subrisk/set/residualLevel Actualizar valor residual de los subriesgos. Los subriesgos deben de ser enviados dentro de un arreglo.
 * @apiName SetResidualLevels
 * @apiGroup Subrisk
 * @apiPermission user
 * @apiParam {String} access_token Token de accesso de usuario.
 * @apiParam {Object[]} subrisks Subriesgos a actualizar. Ver Object Subrisks para mas detalles.
 * @apiParam (Object Subrisks) {ObjectId} id id del documento a actualizar en subriesgos.
 * @apiParam (Object Subrisks) {Object} levelResidualAvg Objeto nivel residual del subriesgo.  Ver Object LevelResidualAvg para mas detalles.
 * @apiParam (Object LevelResidualAvg) {String} label Etiqueta del nivel residual del subriesgo.
 * @apiParam (Object LevelResidualAvg) {Number} value Valor del nivel residual del subriesgo.
 * @apiError {Object} 400  Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Subrisks no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.put('/set/residualLevels',
  token({ required: true }),
  body({subrisks:[{subrisk_id: _id, levelResidualAvg }]}),
  setResidualLevel)

  /**
 * @api {put} /subrisk/set/proceder Actualizar valor proceder de los subriesgos. Los subriesgos deben de ser enviados dentro de un arreglo.
 * @apiName SetProceder
 * @apiGroup Subrisk
 * @apiPermission user
 * @apiParam {String} access_token Token de accesso de usuario.
 * @apiParam {Object[]} subrisks Subriesgos a actualizar. Ver Object Subrisks para mas detalles.
 * @apiParam (Object Subrisks) {ObjectId} id id del documento a actualizar en subriesgos.
 * @apiParam (Object Subrisks) {Object} Proceder Objeto del subriesgo.  Ver Object Proceder para mas detalles.
 * @apiParam (Object Proceder) {String} label Etiqueta del objeto proceder del subriesgo.
 * @apiParam (Object Proceder) {Number} value Valor del objeto proceder del subriesgo.
 * @apiError {Object} 400  Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Subrisks no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.put('/set/proceder',
  token({ required: true }),
  body({subrisks:[{subrisk_id: _id, proceder }]}),
  setProceder)

/**
 * @api {delete} /subrisk/:id Eliminar Subrisk
 * @apiName DeleteSubrisk
 * @apiGroup Subrisk
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiSuccess (Success 204) 204 Sin contenido.
 * @apiError 404 Subrisk no encontrado.
 * @apiError 401 Solo acceso a usuarios.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
