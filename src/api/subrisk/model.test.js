import { Risk } from '.'

let risk

beforeEach(async () => {
  risk = await Risk.create({ subrisk: 'test', description: 'test', sanction: 'test', impact: 'test', probability: 'test', level: 'test', active: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = risk.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(risk.id)
    expect(view.subrisk).toBe(risk.subrisk)
    expect(view.description).toBe(risk.description)
    expect(view.sanction).toBe(risk.sanction)
    expect(view.impact).toBe(risk.impact)
    expect(view.probability).toBe(risk.probability)
    expect(view.level).toBe(risk.level)
    expect(view.active).toBe(risk.active)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = risk.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(risk.id)
    expect(view.subrisk).toBe(risk.subrisk)
    expect(view.description).toBe(risk.description)
    expect(view.sanction).toBe(risk.sanction)
    expect(view.impact).toBe(risk.impact)
    expect(view.probability).toBe(risk.probability)
    expect(view.level).toBe(risk.level)
    expect(view.active).toBe(risk.active)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
