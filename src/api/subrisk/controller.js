import { success, notFound, notUpdate } from '../../services/response/'
import { validationError, validationResponse } from '../../services/response/responsesModules'
import { Subrisk } from '.'
  
// Create Subrisk. DONE!
export const create = async ({ user, bodymen: { body } }, res, next) => {
  try {
    let updateVars = null
    let createSub = await Subrisk.create({ ...body, 'organization_id': user.organization_id })
    if (createSub) {
      updateVars = await Subrisk.findOneAndUpdate({_id: createSub._id}, {quizAssigned: createSub._id}, {new: true})
    }
    
    if (updateVars) {
      let consul = await Subrisk.findOne({ _id: createSub._id })
      let resultValidation = validationResponse(consul, res)
      resultValidation ? res.json(resultValidation.view()) : null
    }
  } catch (err) {
    validationError(err, res)
  }
}
/* Subrisk.create({ ...body, 'organization_id': user.organization_id })
    .then((subrisk) => subrisk.view(true))
    .then(success(res, 201))
    .catch(next) */

// Get Subrisks. DONE! Nota: Solo regresa los riesgos de la organización a la cual pertenece el usuario logueado.
export const index = ({ querymen: { query, select, cursor }, user }, res, next) => {
  query.organization_id = user.organization_id
  Subrisk.count(query)
    .then(count => Subrisk.find(query, select, cursor).sort({createdAt: 1})
      .then((subrisks) => ({
        count,
        rows: subrisks.map((subrisk) => subrisk.view())
      }))
    )
    .then(success(res))
    .catch(next)
}

// Get One Subrisk. DONE!
export const show = ({ params, user }, res, next) =>
  Subrisk.find({ organization_id: user.organization_id, moduleId: params.moduleId })
    .then(notFound(res))
    .then((subrisk) => (
      {
        count: subrisk.length,
        rows: subrisk.map((sub) => sub.view())
      }
    ))
    .then(success(res))
    .catch(next)

// Get Subrisk's params. DONE!
export const showParams = ({ params }, res, next) =>
  Subrisk.findOne({_id: params.id}, {_id: 0, 'params': 1})
    .then(notFound(res))
    .then((subrisk) => subrisk ? subrisk.viewParams() : null)
    .then(success(res))
    .catch(next)

// Update Subrisk.  DONE!
export const update = ({ bodymen: { body }, params }, res, next) =>
  Subrisk.findById(params.id)
    .then(notFound(res))
    .then((subrisk) => subrisk ? Object.assign(subrisk, body).save() : null)
    .then((subrisk) => subrisk ? subrisk.view(true) : null)
    .then(success(res))
    .catch(next)

export const updateParams = ({ bodymen: { body }, params }, res, next) =>
  Subrisk.updateOne(
    {
      _id: params.id
    },
    {
      $set: {
        'impactAvg.value': body.impactAvg.value,
        'probabilityAvg.value': body.probabilityAvg.value,
        'levelInhAvg.value': body.levelInhAvg.value,
        'params': body.params
      }
    })
    .then(notUpdate(res))
    .then(success(res))
    .catch(next)

export const setResidualLevel = async ({ bodymen: { body }, params }, res, next) =>{
  try { 
    if(body.subrisks.length < 1){
      res.status(204)
      res.send('No information to update')
    } else {
      let bulkOper = []
      body.subrisks.map( reg =>{
        bulkOper.push({
          updateOne:{
            'filter' : {'_id': reg.subrisk_id},
            'update' : { $set : {'levelResidualAvg': reg.levelResidualAvg}}
          }
        })
      })

      let response = await Subrisk.bulkWrite(bulkOper)
      if (response.modifiedCount === 0 ) {
        res.status(201)
        res.send('No documents updated')
      } else {
        res.status(200)
        res.send('Success Update')
      }
    }
  }
  catch (err) {
    if (err.name == 'ValidationError') {
      res.status(422).json({
        valid: false,
        name: err.name,
        message: err.message
      })
    } else {
      res.status(500).json({
        valid: false,
        name: err.name,
        message: err.message
      })
    }
  }
  
  
}

export const setProceder = ({ bodymen: { body }, params }, res, next) =>{
  if(body.subrisks.length < 1){
    res.status(200)
    res.send('Success Update')
  }else{
    let bulkOper = []
    body.subrisks.map(reg =>{
      bulkOper.push({
        updateOne:{
          'filter' : {'_id': reg.subrisk_id},
          'update' : { $set : {'proceder': reg.proceder}}
        }
      })
    }) 
    Subrisk.bulkWrite(bulkOper)
    .then( res1 => {
      res.status(200)
      res.send('Success Update')
    })
    .catch(error => {
      console.log(error)
    })
  }
  
}

export const addParams = async ({ bodymen: { body }, params }, res, next) => {
  try {
    console.log('BODY-', body.params)
    let addP = await Subrisk.findOneAndUpdate({ '_id': params.id }, {
      $push: {
        'params': body.params
      },
      $set: {
        'impactAvg.value': body.impactAvg.value,
        'impactAvg.label': body.impactAvg.label,
        'levelInhAvg.value': body.levelInhAvg.value,
        'levelInhAvg.label': body.levelInhAvg.label,
        'probabilityAvg.value': body.probabilityAvg.value,
        'probabilityAvg.label': body.probabilityAvg.label
      }
    }, { new: true })
    let resultValidation = validationResponse(addP, res)

    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}

export const updateParameters = async ({ bodymen: { body }, params }, res, next) => {
  try {
    let setter = { $set: {} }
    let setterAvg = { $set: {} }
    if (body.params != null) {
      if (body.params.impact != null) {
        if (body.params.impact.value != null) {
          setter.$set['params.$.impact.value'] = body.params.impact.value
        }
        if (body.params.impact.label != null) {
          setter.$set['params.$.impact.label'] = body.params.impact.label
        }
      }
      if (body.params.level != null) {
        if (body.params.level.value != null) {
          setter.$set['params.$.level.value'] = body.params.level.value
        }
        if (body.params.level.label != null) {
          setter.$set['params.$.level.label'] = body.params.level.label
        }
      }
      if (body.params.probability != null) {
        if (body.params.probability.value != null) {
          setter.$set['params.$.probability.value'] = body.params.probability.value
        }
        if (body.params.probability.label != null) {
          setter.$set['params.$.probability.label'] = body.params.probability.label
        }
      }
      if (body.params.moduleId != null) {
        setter.$set['params.$.moduleId'] = body.params.moduleId
      }
      if (body.params.title != null) {
        setter.$set['params.$.title'] = body.params.title
      }
    }
    if (body.impactAvg != null) {
      if (body.impactAvg.value != null) {
        setterAvg.$set['impactAvg.value'] = body.impactAvg.value
      }
      if (body.impactAvg.label != null) {
        setterAvg.$set['impactAvg.label'] = body.impactAvg.label
      }
    }
    if (body.levelInhAvg != null) {
      if (body.levelInhAvg.value != null) {
        setterAvg.$set['levelInhAvg.value'] = body.levelInhAvg.value
      }
      if (body.levelInhAvg.label != null) {
        setterAvg.$set['levelInhAvg.label'] = body.levelInhAvg.label
      }
    }
    if (body.probabilityAvg != null) {
      if (body.probabilityAvg.value != null) {
        setterAvg.$set['probabilityAvg.value'] = body.probabilityAvg.value
      }
      if (body.probabilityAvg.label != null) {
        setterAvg.$set['probabilityAvg.label'] = body.probabilityAvg.label
      }
    }
    let updateP = await Subrisk.findOneAndUpdate({ '_id': params.id, 'params._id': body.params._id }, setter, {new: true})
    let resultValidation = validationResponse(updateP, res)
    if (resultValidation) {
      let updateAvg = await Subrisk.findOneAndUpdate({ '_id': params.id }, setterAvg, {new: true})
      let resultValidation2 = validationResponse(updateAvg, res)
      resultValidation ? res.json(resultValidation2.view()) : null
    }
  } catch (err) {
    validationError(err, res)
  }
}

export const deleteParams = async ({ bodymen: { body }, params }, res, next) => {
  try {
    let setterAvg = { $set: {} }

    if (body.impactAvg != null) {
      if (body.impactAvg.value != null) {
        setterAvg.$set['impactAvg.value'] = body.impactAvg.value
      }
      if (body.impactAvg.label != null) {
        setterAvg.$set['impactAvg.label'] = body.impactAvg.label
      }
    }
    if (body.levelInhAvg != null) {
      if (body.levelInhAvg.value != null) {
        setterAvg.$set['levelInhAvg.value'] = body.levelInhAvg.value
      }
      if (body.levelInhAvg.label != null) {
        setterAvg.$set['levelInhAvg.label'] = body.levelInhAvg.label
      }
    }
    if (body.probabilityAvg != null) {
      if (body.probabilityAvg.value != null) {
        setterAvg.$set['probabilityAvg.value'] = body.probabilityAvg.value
      }
      if (body.probabilityAvg.label != null) {
        setterAvg.$set['probabilityAvg.label'] = body.probabilityAvg.label
      }
    }


    let delParams = await Subrisk.findOneAndUpdate({ '_id': params.id, 'params._id': body.params._id },
      { '$pull': { 'params': { '_id': body.params._id } } }, {new: true})
      let resultValidation = validationResponse(delParams, res)
    if (resultValidation) {
      let updateAvg = await Subrisk.findOneAndUpdate({ '_id': params.id }, setterAvg, {new: true})
      let resultValidation = validationResponse(updateAvg, res)
      resultValidation ? res.json(resultValidation.view()) : null
    }
  } catch (err) {
    validationError(err, res)
  }
}
// Delete Subrisk.  DONE!
export const destroy = ({ params }, res, next) =>
  Subrisk.findById(params.id)
    .then(notFound(res))
    .then((subrisk) => subrisk ? subrisk.remove() : null)
    .then(success(res, 204))
    .catch(next)

    /* export const updateSubrisks = ({ bodymen: { body }, params }, res, next) =>
  Subrisk.findOne({organizationId: params.organizationId})
    .then(notFound(res))
    .then((subrisk) => subrisk ? Object.assign(subrisk, body).save() : null)
    .then((subrisk) => subrisk ? subrisk.view(true) : null)
    .then(success(res))
    .catch(next) */

// EXAMPLE: Add element inside an array. DON'T DELETE!
// TODO: Cambiar el elementMatch por id
/* export const addSubrisk = ({ bodymen: { body }, params }, res, next) => {
  let active = body.subrisk.active
  // change value in order to use default scheme value for 'active' field
  if (active === '' || active === null) {
    body.subrisk.active = undefined
  }

  Subrisk.update({ organizationId: params.organizationId },
    { $push:
      {'subrisk': body.subrisk}
    })
    .then(() => {
      Subrisk.findOne({organizationId: params.organizationId}, {'subrisk': {$elemMatch: {'title': body.subrisk.title}}})
        .then(notFound(res))
        .then((subrisk) => subrisk.viewSubrisk())
        .then(success(res))
        .catch(next)
    })
    .then(success(res, 200))
    .catch(next)
} */

// EXAMPLE: Update inside an array. DON'T DELETE!
/* export const updateSubrisk = ({ bodymen: { body }, params }, res, next) => {
  Subrisk.update({ 'subrisk._id': params.subriskId },
    { $set:
      {
        'subrisk.$.title': body.subrisk.title,
        'subrisk.$.description': body.subrisk.description,
        'subrisk.$.sanction': body.subrisk.sanction,
        'subrisk.$.levelResidualAvg': body.subrisk.levelResidualAvg,
        'subrisk.$.impactAvg': body.subrisk.impactAvg,
        'subrisk.$.probabilityAvg': body.subrisk.probabilityAvg,
        'subrisk.$.levelInhAvg': body.subrisk.levelInhAvg,
        'subrisk.$.active': body.subrisk.active
      }
    })
    .then(notUpdate(res))
    .then(success(res))
    .catch(next)
} */

// EXAMPLE: Update inside a nested array. DON'T DELETE!
/* export const updateParams = ({ bodymen: { body }, params }, res, next) => {
  Subrisk.updateOne({ 'subrisk._id': params.subriskId }, { $set: { 'subrisk.$.params': body.params } })
    .then(notUpdate(res))
    .then(success(res))
    .catch(next)
} */

// EXAMPLE: Delete inside an array. DON'T DELETE!
/* export const destroySubrisk = ({ params }, res, next) => {
  Subrisk.findOne({'subrisk._id': params.id})
    .then(notFound(res))
    .then((risk) => {
      if (risk) {
        risk.subrisk.id(params.id).remove()
        return risk.save()
      } else {
        return null
      }
    })
    .then(success(res, 204))
    .catch(next)
} */
