import { Router } from 'express'
import user from './user'
import auth from './auth'
import blog from './blog'
import question from './question'
import subrisk from './subrisk'
import organization from './organization'
import notifications from './notifications'
import controls from './controls'
import task from './task'
import mail from './mail'
import deleteDomain from './deleteDomain'
import file from './file'
import folder from './folder'
import systemlog from './systemlog'
import newsletter from './newsletter'
import paymentPaypal from './paymentPaypal'
import logPayments from './logPayments'
import mercadopago from './mercadoPago'
import facturation from './facturation'
const router = new Router()

/**
 * @apiDefine master Master access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine admin Admin access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine user User access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine listParams
 * @apiParam {String} [q] Query to search.
 * @apiParam {Number{1..30}} [page=1] Page number.
 * @apiParam {Number{1..100}} [limit=30] Amount of returned items.
 * @apiParam {String[]} [sort=-createdAt] Order of returned items.
 * @apiParam {String[]} [fields] Fields to be returned.
 */
router.use('/users', user)
router.use('/auth', auth)
router.use('/blogs', blog)
router.use('/subrisk', subrisk)
router.use('/questions', question)
router.use('/organizations', organization)
router.use('/notifications', notifications)
router.use('/controls', controls)
router.use('/tasks', task)
router.use('/mail', mail)
router.use('/deleteDomains', deleteDomain)
router.use('/files', file)
router.use('/folders', folder)
router.use('/systemlog', systemlog)
router.use('/newsletters', newsletter)
router.use('/paymentPaypal', paymentPaypal)
router.use('/logPayments', logPayments)
router.use('/mercadopagos', mercadopago)
router.use('/facturations', facturation)
export default router
