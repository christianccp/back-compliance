import request from 'supertest'
import { apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Controls } from '.'

const app = () => express(apiRoot, routes)

let userSession, controls

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  userSession = signSync(user.id)
  controls = await Controls.create({})
})

test('POST /controls 201 (user)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession, section: 'test', title: 'test', organizationId: 'test', questions: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.section).toEqual('test')
  expect(body.title).toEqual('test')
  expect(body.organizationId).toEqual('test')
  expect(body.questions).toEqual('test')
})

test('POST /controls 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /controls 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /controls 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /controls/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${controls.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(controls.id)
})

test('GET /controls/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${controls.id}`)
  expect(status).toBe(401)
})

test('GET /controls/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /controls/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${controls.id}`)
    .send({ access_token: userSession, section: 'test', title: 'test', organizationId: 'test', questions: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(controls.id)
  expect(body.section).toEqual('test')
  expect(body.title).toEqual('test')
  expect(body.organizationId).toEqual('test')
  expect(body.questions).toEqual('test')
})

test('PUT /controls/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${controls.id}`)
  expect(status).toBe(401)
})

test('PUT /controls/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: userSession, section: 'test', title: 'test', organizationId: 'test', questions: 'test' })
  expect(status).toBe(404)
})

test('DELETE /controls/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${controls.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /controls/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${controls.id}`)
  expect(status).toBe(401)
})

test('DELETE /controls/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})
