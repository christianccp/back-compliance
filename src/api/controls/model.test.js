import { Controls } from '.'

let controls

beforeEach(async () => {
  controls = await Controls.create({ section: 'test', title: 'test', organizationId: 'test', questions: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = controls.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(controls.id)
    expect(view.section).toBe(controls.section)
    expect(view.title).toBe(controls.title)
    expect(view.organizationId).toBe(controls.organizationId)
    expect(view.questions).toBe(controls.questions)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = controls.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(controls.id)
    expect(view.section).toBe(controls.section)
    expect(view.title).toBe(controls.title)
    expect(view.organizationId).toBe(controls.organizationId)
    expect(view.questions).toBe(controls.questions)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
