import { success, notFound } from '../../services/response/'
import { Controls } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Controls.create(body)
    .then((controls) => controls.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Controls.find(query, select, cursor)
    .then((controls) => controls.map((controls) => controls.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Controls.findById(params.id)
    .then(notFound(res))
    .then((controls) => controls ? controls.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Controls.findById(params.id)
    .then(notFound(res))
    .then((controls) => controls ? Object.assign(controls, body).save() : null)
    .then((controls) => controls ? controls.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Controls.findById(params.id)
    .then(notFound(res))
    .then((controls) => controls ? controls.remove() : null)
    .then(success(res, 204))
    .catch(next)
