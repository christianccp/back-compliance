import mongoose, { Schema } from 'mongoose'

const questionsSchema = new Schema({
  question: {
    type: String
  },
  value: {
    type: Number
  },
  weight: {
    type: Number
  },
  evidence: {
    type: String
  }
})

const controlsSchema = new Schema({
  section: {
    type: String
  },
  title: {
    type: String
  },
  organizationId: {
    type: String
  },
  questions: {
    type: [questionsSchema]
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})
controlsSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      section: this.section,
      title: this.title,
      organizationId: this.organizationId,
      questions: this.questions,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Controls', controlsSchema)

export const schema = model.schema
export default model
