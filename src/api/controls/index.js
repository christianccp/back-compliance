import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Controls, { schema } from './model'

const router = new Router()
const { section, title, organizationId, questions } = schema.tree
const { question, value, weight, evidence } = schema.tree
/**
 * @api {post} /controls Agregar control
 * @apiName Agregar control
 * @apiGroup Controls
 * @apiPermission user
 * @apiParam {String} access_token Token de accesso de usuario.
 * @apiParam {String} section Sección del control.
 * @apiParam {String} title Títutlo del control.
 * @apiParam {String} organizationId Nombre de la organizacion.
 * @apiParam {Object[]} questions Preguntas de control. Ver Object Preguntas para mas detalles.
 * @apiParam (Object Preguntas) {String} question Pregunta.
 * @apiParam (Object Preguntas) {Decimal} value Valor.
 * @apiParam (Object Preguntas) {Number} weight Peso.
 * @apiParam (Object Preguntas) {String} evidence Evidencia.
 * @apiParamExample [json] Resquest-ejemplo
 *  {
      "access_token": "123...",
      "section": "seccion",
      "organizationId" : "PAD",
      "questions": {
      "question":"pregunta",
      "value":224.6,
      "weight":23,
      "evidence":"evidencia"
      }
    }
 * @apiSuccess {Object} controls Datos de control.
 * @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Controls no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.post('/',
  token({ required: true }),
  body({ section, title, organizationId, questions: {question, value, weight, evidence} }),
  create)

/**
 * @api {get} /controls Consultar controles
 * @apiName Consultar controles
 * @apiGroup Controls
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiUse listParams
 * @apiSuccess {Object[]} controls Lista de controles.
 * @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 401 Solo acceso a usuarios.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /controls/:id Consultar control por Id
 * @apiName Consultar control
 * @apiGroup Controls
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiSuccess {Object} controls Datos de control.
 * @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Controls no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /controls/:id Actualizar control
 * @apiName Actualizar control
 * @apiGroup Controls
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiParam {String} section Sección de control.
 * @apiParam {String} title Títutlo del control.
 * @apiParam {String} organizationId Nombre de la organización.
 * @apiParam {Object[]} questions Preguntas de control. Ver Object Preguntas para mas detalles.
 * @apiSuccess {Object} controls Datos de control.
 * @apiParam (Object Preguntas) {String} question Pregunta.
 * @apiParam (Object Preguntas) {Decimal} value Valor.
 * @apiParam (Object Preguntas) {Number} weight Peso.
 * @apiParam (Object Preguntas) {String} evidence Evidencia.
 * @apiParamExample [json] Resquest-ejemplo
 *  {
      "access_token": "123...",
      "section": "seccion",
      "organizationId" : "PAD",
      "questions": {
      "question":"pregunta",
      "value":224.6,
      "weight":23,
      "evidence":"evidencia"
      }
    }
 * @apiError {Object} 400 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Controls no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.put('/:id',
  token({ required: true }),
  body({ section, title, organizationId, questions }),
  update)

/**
 * @api {delete} /controls/:id Eliminar control
 * @apiName Eliminar control
 * @apiGroup Controls
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiSuccess (Success 204) 204 Sin contenido.
 * @apiError 404 Controls no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
