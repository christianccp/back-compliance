import { LogPayments } from '.'

let logPayments

beforeEach(async () => {
  logPayments = await LogPayments.create({ idPay: 'test', email: 'test', type: 'test', info: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = logPayments.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(logPayments.id)
    expect(view.idPay).toBe(logPayments.idPay)
    expect(view.email).toBe(logPayments.email)
    expect(view.type).toBe(logPayments.type)
    expect(view.info).toBe(logPayments.info)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = logPayments.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(logPayments.id)
    expect(view.idPay).toBe(logPayments.idPay)
    expect(view.email).toBe(logPayments.email)
    expect(view.type).toBe(logPayments.type)
    expect(view.info).toBe(logPayments.info)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
