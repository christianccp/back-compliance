import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
import { schema as schemaOrg } from '../organization/model'
export LogPayments, { schema } from './model'
export {create} from './controller'
const router = new Router()
const { type, email, info } = schema.tree
//const { premium } = schemaOrg.tree
var premium
/**
 * @api {post} /logPayments Create log payments
 * @apiName CreateLogPayments
 * @apiGroup LogPayments
 * @apiParam idPay Log payments's idPay.
 * @apiParam email Log payments's email.
 * @apiParam type Log payments's type.
 * @apiParam info Log payments's info.
 * @apiSuccess {Object} logPayments Log payments's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Log payments not found.
 */
router.post('/',
  body({ type, email, info, premium }),
  create)

/**
 * @api {get} /logPayments Retrieve log payments
 * @apiName RetrieveLogPayments
 * @apiGroup LogPayments
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of log payments.
 * @apiSuccess {Object[]} rows List of log payments.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /logPayments/:id Retrieve log payments
 * @apiName RetrieveLogPayments
 * @apiGroup LogPayments
 * @apiSuccess {Object} logPayments Log payments's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Log payments not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /logPayments/:id Update log payments
 * @apiName UpdateLogPayments
 * @apiGroup LogPayments
 * @apiParam idPay Log payments's idPay.
 * @apiParam email Log payments's email.
 * @apiParam type Log payments's type.
 * @apiParam info Log payments's info.
 * @apiSuccess {Object} logPayments Log payments's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Log payments not found.
 */
router.put('/:id',
  body({ type, email, info, premium }),
  update)

/**
 * @api {delete} /logPayments/:id Delete log payments
 * @apiName DeleteLogPayments
 * @apiGroup LogPayments
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Log payments not found.
 */
router.delete('/:id',
  destroy)

export default router
