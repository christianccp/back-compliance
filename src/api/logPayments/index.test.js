import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { LogPayments } from '.'

const app = () => express(apiRoot, routes)

let logPayments

beforeEach(async () => {
  logPayments = await LogPayments.create({})
})

test('POST /logPayments 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ idPay: 'test', email: 'test', type: 'test', info: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.idPay).toEqual('test')
  expect(body.email).toEqual('test')
  expect(body.type).toEqual('test')
  expect(body.info).toEqual('test')
})

test('GET /logPayments 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /logPayments/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${logPayments.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(logPayments.id)
})

test('GET /logPayments/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /logPayments/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${logPayments.id}`)
    .send({ idPay: 'test', email: 'test', type: 'test', info: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(logPayments.id)
  expect(body.idPay).toEqual('test')
  expect(body.email).toEqual('test')
  expect(body.type).toEqual('test')
  expect(body.info).toEqual('test')
})

test('PUT /logPayments/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ idPay: 'test', email: 'test', type: 'test', info: 'test' })
  expect(status).toBe(404)
})

test('DELETE /logPayments/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${logPayments.id}`)
  expect(status).toBe(204)
})

test('DELETE /logPayments/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
