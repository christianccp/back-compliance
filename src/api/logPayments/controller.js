import { success, notFound } from '../../services/response/'
import { LogPayments } from '.'
import { validationError, validationResponse } from '../../services/response/responsesModules'
import {sendMailBuy} from '../mail/controller'
import {setPremium} from '../organization/controller'
import { User } from '../user'


export const create = async ({ bodymen: { body } }, res, next) => {
  try {
    var user = await User.findOne({email: body.email})
    body.idPay = body.info.id
    var createdPayment = await LogPayments.create(body)
    if (res !== null) {
      await setPremium({bodymen: {body: {premium: body.premium}}, user}, null, null)
      let email = await sendMailBuy({bodymen: {body: {correo: body.email}}}, null, null)
      if (email == 200) {
        let resultValidation = validationResponse(createdPayment, res)
        resultValidation ? res.json(resultValidation.view()) : null
      } else {
        res.status(400)
      }
    } else {
      await setPremium({bodymen: {body: {premium: body.premium}}, user}, null, null)
      let email = await sendMailBuy({bodymen: {body: {correo: body.email}}}, null, null)
      if (email == 200) {
        return 200
      } else {
        return 400
      }
    }
  } catch (err) {
    if (res !== null) {
      validationError(err, res)
    } else {
      if (createdPayment.email == body.email) {
        return 400
      } else {
        return 500
      }
    }
  }
}

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  LogPayments.count(query)
    .then(count => LogPayments.find(query, select, cursor)
      .then((logPayments) => ({
        count,
        rows: logPayments.map((logPayments) => logPayments.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  LogPayments.findById(params.id)
    .then(notFound(res))
    .then((logPayments) => logPayments ? logPayments.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  LogPayments.findById(params.id)
    .then(notFound(res))
    .then((logPayments) => logPayments ? Object.assign(logPayments, body).save() : null)
    .then((logPayments) => logPayments ? logPayments.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  LogPayments.findById(params.id)
    .then(notFound(res))
    .then((logPayments) => logPayments ? logPayments.remove() : null)
    .then(success(res, 204))
    .catch(next)

    export const Testing = ({ params }, res, next) =>{

    }
