import mongoose, { Schema } from 'mongoose'

const logPaymentsSchema = new Schema({
  type: {
    type: String
  },
  email: {
    type: String
  },
  idPay: {
    type: String
  },
  info: {
    type: Object
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

logPaymentsSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      link: this.link,
      recovery: this.recovery,
      type: this.type,
      info: this.info,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('LogPayments', logPaymentsSchema)

export const schema = model.schema
export default model
