import mongoose, { Schema } from 'mongoose'

const paramSchema = new Schema({
  _id: false,
  value: {
    type: Number
  },
  label: {
    type: String
  }
})

const variablesSchema = new Schema({
  _id: false,
  moduleId:   {
    type: String
  },
  titleOne:   {
    type: String
  },
  titleTwo:   {
    type: String
  },
  varsOne: {
    type: [paramSchema]
  },
  varsTwo: {
    type: [paramSchema]
  },
  levels: {
    type: [{
      _id: false,
      value: {
        type: Number
      },
      label: {
        type: String
      },
      color: {
        type: String
      }
    }]
  },
})

const paramsSchema = new Schema({
  _id: false,
  moduleId: {
    type: String
  },
  impacts: {
    type: [paramSchema]
  },
  probabilities: {
    type: [paramSchema]
  },
  levels: {
    type: [{
      _id: false,
      value: {
        type: Number
      },
      label: {
        type: String
      },
      color: {
        type: String
      }
    }]
  }
})

const questionsSchema = new Schema({
  title: {
    type: String
  },
  description: {
    type: String
  },
  typeOf: {
    type: String
  },
  moduleId: {
    type: String
  },
  subTypeOf: {
    type: String
  }
})

const areasSchema = new Schema({
  _id: false,
  name: {
    type: String
  },
  value: {
    type: String
  },
  authorizer: {
    type: String
  }
})

const premiumSchema = new Schema({
  _id: false,
  moduleName: {
    type: String
  },
  value: {
    type: Boolean
  },
  serviceDocumentalMgt: {
    type: Boolean
  }
})

const organizationSchema = new Schema({
  domain: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    lowercase: true
  /*   ref: 'User',
    required: true */
  },
  name: {
    type: String
  },
  numEmployments: {
    type: String
  },
  income: {
    type: String
  },
  url: {
    type: String
  },
  zipcode: {
    type: String
  },
  city: {
    type: String
  },
  country: {
    type: String
  },
  moduleTarget: {
    type: String
  },
  premium: {
    type: Object
  },
  params: {
    type: [paramsSchema]
  },
  impactVars: {
    type: [variablesSchema]
  },
  probabilityVars: {
    type: [variablesSchema]
  },
  questions: {
    type: [questionsSchema]
  },
  areas: {
    type: [areasSchema]
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

organizationSchema.methods = {
  view (full) {
    const view = {
      // simple view
      _id: this._id,
      domain: this.domain,
      name: this.name,
      numEmployments: this.numEmployments,
      income: this.income,
      url: this.url,
      zipcode: this.zipcode,
      city: this.city,
      country: this.country,
      moduleTarget: this.moduleTarget,
      premium: this.premium,
      params: this.params,
      areas: this.areas,
      questions: this.questions,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view,
      // add properties for a full view
    } : view
  },
  viewParams() {
    const view = {
      params: this.params[0],
    }
    return view
  },
  viewVars(type) {
    let view = null

    const fullview = {
      impactVars: this.impactVars[0],
      probabilityVars: this.probabilityVars[0]
    }

    if (type === 'impact' ){
      view = { impactVars: this.impactVars[0] }
    } else if (type === 'probability' ){
      view = { probabilityVars: this.probabilityVars[0] }
    } else {
      view = fullview
    }

    return view
  }
}

const model = mongoose.model('Organization', organizationSchema)

export const schema = model.schema
export default model
