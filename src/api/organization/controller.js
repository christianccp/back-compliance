import { success, notFound, authorOrAdmin } from '../../services/response/'
import { Organization } from '.'
import { User } from '../user'
import mongoose from 'mongoose'
import { dotNotate } from '../../services/miscellany/functions'
import { validationError, validationResponse } from '../../services/response/responsesModules'
import { jwtSecret, emailApi, passApi } from '../../config'
import { URL_FRONT_CLIENT } from '../../variableProd'
import cron from 'node-cron'
let ENDPOINT = URL_FRONT_CLIENT
var nodemailer = require('nodemailer')
var jwt = require('jwt-simple')

var vigModules = [
  'vigAnticorruption',
  'vigAutodiagnostic',
  'vigPenal',
  'vigMoney',
  'vigContinuidad'
]

export const create = ({ user, bodymen: { body } }, res, next) => {
  console.log('CREATING ORG')
  Organization.create({ ...body, organizationId: user.name })
    .then((organization) => organization.view(true))
    .then(success(res, 201))
    .catch(next)
}

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Organization.count(query)
    .then(count => Organization.find(query, select, cursor)
      .populate('create_by')
      .then((organizations) => ({
        count,
        rows: organizations.map((organization) => organization.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Organization.findById(params.id)
    .populate('create_by')
    .then(notFound(res))
    .then((organization) => organization ? organization.view() : null)
    .then(success(res))
    .catch(next)

export const showParams = async ({ params, user, querymen: { query, select, cursor } }, res, next) => {
  try {
    let organization = await Organization.findOne({_id: user.organization_id, 'params.moduleId': query.moduleId})
    if (organization) {
      let result = organization.params.find(module => module.moduleId === query.moduleId)
      let resultValidation = validationResponse(result, res)
      resultValidation ? res.json(resultValidation) : null
    } else {
      let resultValidation = validationResponse(null, res)
      resultValidation ? res.json(resultValidation) : null
    }
  } catch (err) {
    validationError(err, res)
  }
}

export const showVars = ({ params, user }, res, next) => {
  Organization.findById(user.organization_id)
    .then(notFound(res))
    .then((organization) => organization ? organization.viewVars() : null)
    .then(success(res))
    .catch(next)
}

export const showSectionsInfo = ({ querymen: { query, select, cursor},user }, res, next) =>{
  Organization.aggregate(
    [
      {
        '$match': {
          '_id': mongoose.Types.ObjectId(user.organization_id),
          'questions.moduleId': query.moduleId
        }
      }, {
        '$unwind': {
          'path': '$questions'
        }
      }, {
        '$match': {
          'questions.moduleId': query.moduleId,
          'questions.typeOf': new RegExp('^'+ query.subTypeOf)
        }
      }, {
        '$lookup': {
          'from': 'questions',
          'let': {
            'myId': '$_id',
            'myModule': '$questions.moduleId',
            'myType': '$questions.typeOf'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$organization_id', '$$myId'
                      ]
                    }, {
                      '$eq': [
                        '$typeOf', '$$myType'
                      ]
                    },  {
                      '$eq': [
                        '$moduleId', '$$myModule'
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'questions.total'
        }
      }, {
        '$addFields': {
          'questions.progress': {
            '$sum': {
              '$map': {
                'input': '$questions.total',
                'as': 'ques',
                'in': {
                  '$cond': [
                    {
                      '$gt': [
                        '$$ques.result', -1
                      ]
                    }, 1, 0
                  ]
                }
              }
            }
          }
        }
      }, {
        '$project': {
          'questions.typeOf': 1,
          'questions.moduleId': 1,
          'questions.title': 1,
          'questions._id': 1,
          'questions.description': 1,
          'questions.total': {
            '$size': '$questions.total'
          },
          'questions.progress': 1
        }
      }
    ]
  )
    .then(success(res))
    .catch(next)
}

export const updateSectionsInfo = async ({ bodymen: { body }, params, user }, res, next) => {
  try {
    let organization = await Organization.findOneAndUpdate(
      {
        _id: user.organization_id,
        'questions._id': params.id
      },
      {
        $set: { 'questions.$.description': body.questions.description }
      },
      {
        new: true
      })

    let resultValidation = validationResponse(organization, res)
    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}

export const update = ({ user, bodymen: { body }, params }, res, next) =>
  Organization.findById(params.id)
    .populate('create_by')
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'create_by'))
    .then((organization) => organization ? Object.assign(organization, body).save() : null)
    .then((organization) => organization ? organization.view(true) : null)
    .then(success(res))
    .catch(next)

export const updateProfile = ({ user, bodymen: { body }, params }, res, next) =>
  Organization.findById(params.id)
    .populate('create_by')
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'create_by'))
    .then((organization) => organization ? Object.assign(organization, body).save() : null)
    .then((organization) => organization ? organization.view(true) : null)
    .then(success(res))
    .catch(next)

export const updateParams = async ({ bodymen: { body }, user, params }, res, next) => {
  try {
    let updateParam = await Organization.findOneAndUpdate({_id: user.organization_id, 'params.moduleId': body.params.moduleId}, {$set: {'params.$': body.params}}, {new: true})
    let resultValidation = validationResponse(updateParam, res)
    resultValidation ? res.json(resultValidation) : null
  } catch (err) {
    validationError(err, res)
  }
}

// TODO: Actualizar solo el moduleId que se requiere
// TODO: Use enum for type
export const updateVars = async ({ bodymen: { body }, user, params }, res, next) => {
  try {
    let organization = await Organization.findById(user.organization_id)

    if (!organization) {
      res.status(404).end()
      return null
    }

    if (params.type ==='impact'){
      organization.impactVars = body.impactVars
      await organization.save()
    } else {
      organization.probabilityVars = body.probabilityVars
      await organization.save()
    }

    let view = organization.viewVars(params.type)
    res.status(200).json(view)
  } catch (err) {
    next(err)
  }
}

export const updateAreas = async ({ bodymen: { body }, user, params }, res, next) => {
  try {
    let setter = { $set: dotNotate(body) }
    let actAreas = await Organization.findOneAndUpdate({_id: user.organization_id, 'areas.value': body.value}, {$set: {'areas.$.authorizer': body.authorizer}}, {new: true})
    let resultValidation = validationResponse(actAreas, res)
    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}

export const setPremium = async ({ bodymen: { body }, user, params }, res, next) => {
  try {
    // console.log(body)
    let orgPremium = await Organization.findOne({_id: user.organization_id})
    let fusion = await Object.assign(orgPremium.premium, body.premium)
    // console.log('FUSION', fusion)

    vigModules.map(vigencias => {
      // console.log(fusion.hasOwnProperty(vigencias))
      if (fusion.hasOwnProperty(vigencias) == true) {
        // console.log('Solo existe: ', vigencias)
        fusion[vigencias] = new Date(fusion[vigencias])
        // console.log('---->', fusion)
      }
    })

    // console.log('OBEJECTO A GUARDAR: ', fusion)
    let updatePremium = await Organization.findOneAndUpdate({ _id: user.organization_id }, { $set: {premium: fusion} }, {new: true})
    let resultValidation = validationResponse(updatePremium, res)
    resultValidation ? res.json(resultValidation.view()) : null
  } catch (err) {
    validationError(err, res)
  }
}

export const destroy = ({ user, params }, res, next) =>
  Organization.findById(params.id)
    .then(notFound(res))
    .then(authorOrAdmin(res, user, 'create_by'))
    .then((organization) => organization ? organization.remove() : null)
    .then(success(res, 204))
    .catch(next)

export const consultaModulo = async ({ querymen: { query, select, cursor } }, res, next) => {
  try {
    var tokenQuery = query.token
    var decoded = jwt.decode(tokenQuery, jwtSecret)
    let dataOrg = await Organization.findOne({_id: decoded.organization_id})
    if (dataOrg.premium[decoded.module] == true) {
      // console.log('dentro')
      decoded.vigContinuidad = dataOrg.premium[decoded.vigencia]
    }
    let resultValidation = validationResponse(decoded, res)
    resultValidation ? res.json(resultValidation) : null
  } catch (err) {
    validationError(err, res)
  }
}

export const pruebaToken = async ({ bodymen: { body }, user, params }, res, next) => {
  try {
    if (body.email == emailApi && body.pass == passApi) {
      let organizationsTest = await Organization.find({ 'domain': body.dominios, 'premium.vigContinuidad': new Date('2020-06-30') })
     // console.log(organizationsTest)
    // let modulosOrg = await Organization.find({ 'premium.vigContinuidad': new Date('2020-06-30') })
      await organizationsTest.map(async organization => {
        //console.log('---', organization)
        // console.log('ORGANIZACION: ', organization.domain)
        let usrsAdmin = await User.find({organization_id: organization._id, role: 'admin'})
        //console.log(usrsAdmin)
        usrsAdmin.map(async usuarios => {
          try {
            var payload = {}
            payload._id = usuarios._id
            payload.name = usuarios.name
            payload.email = usuarios.email
            payload.pais = usuarios.pais
            payload.module = 'moduleContinuidad'
            payload.vigencia = 'vigContinuidad'
            payload.organization_id = usuarios.organization_id
            var token = jwt.encode(payload, jwtSecret)
            var link = ENDPOINT + '/buyPricing' + '?' + 'token=' + token
            // console.log('Info Token: ', link)
            // console.log('        ', usuarios.email)
            // arrayUsrs.push({linkButton: link, mail: usuarios.email})
            var transporter = nodemailer.createTransport({
              host: 'smtp.hostinger.com',
              port: 465,
              secure: true,
              auth: {
                user: 'non-reply@padcompliance.mx',
                pass: 'ck307S@J'
              }
            })
            // var codigohtml = "<p>La cuenta de correo :</p><br>"+body.correo+", ha sido bloqueada por pasar los 10 intentos de inicio de sesion. Continue en <a href="+body.link+">LINK</a> para iniciar el proceso de recuperación de contraseña"
            var codigohtml =
    `<p style="text-align:center;font-size:1.3em;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
    <strong style="color:#0f5dac;"><i>PAD Compliance </i></strong>
    </p>
    <img style="img-align:center;display:block;margin:auto;border-radius:50%;" width="50%" height="50%" src="https://pad1-auditoria.s3.us-east-2.amazonaws.com/compraModulo.png">
    <p style="text-align:center;font-size:1.3em; color:#285ea6;">
    <b>¡Licencia de PAD Compliance! </b>
    </p>
    <p style="text-align:center;color:#0f5dac;font-size:1em;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
    <strong>Gracias por probar el sistema PAD Compliance.</strong>
    </p>
    <p style="text-align:center;font-size:13px;color:#0f5dac;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
    <strong>Le informamos que su licencia del modulo C DE NEGOCIO COVID-19 esta a punto de expirar</strong></p>
    </p>
    <p style="text-align:center;font-size:13px;color:#0f5dac;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
    <strong>Lo invitamos a realizar la compra del módulo presionando el siguiente boton.</strong></p>
    </p>
    <a href='${link}' style="box-shadow: 0px 10px 14px -7px #276873;background-color:#285ea6;border-radius:8px;
    display:block;cursor:pointer;color:#ffffff;font-family:Helvetica;font-weight:bold;padding:13px 32px;text-decoration:none;text-shadow:0px 1px 0px #3d768a;text-align:center;
    margin: 0 auto;width:200px;">
    ¡Realizar compra!
    </a>
    <br><br>
    <p style="color: #285ea6;"> <br> 
    PRYVACY LAWYERS S.A.S. DE C.V <br>
    Concepción Mendez #49,
    Col. Atenor Salas, Benito Juarez,
    03010, Ciudad de México, CDMX. <br>
    Contacto: 55-45-00-68-74
    </p>`
            var mailOptions = {
              from: 'PAD <non-reply@padcompliance.mx>',
              to: usuarios.email,
              subject: 'Modulo continuidad de negocio COVID-19 próximo a expirar ',
              text: 'Su modulo de continuidad de negocio COVID-19 está proximo a expirar, haga click en el siguiente boton para realizar su compra',
              html: codigohtml
            }
            let resSend = await transporter.sendMail(mailOptions)
            let resultValidation = validationResponse(resSend, res)
            resultValidation ? res.json(resultValidation) : null
          } catch (ERR_HTTP_HEADERS_SENT) {
            // console.log('ERRORRRR: ')
          }
        })
      })
      // console.log(body.dominios)
    } else {
      res.status(409).json('ERROR DE CREDENCIALES')
    }
  } catch (err) {
    validationError(err, res)
  }
}
cron.schedule('0 5 20 6 *', async () => {
  let modulosOrg = await Organization.find({ 'premium.vigContinuidad': new Date('2020-06-30') })
  await modulosOrg.map(async organization => {
    // console.log('ORGANIZACION: ', organization.domain)
    let usrsAdmin = await User.find({organization_id: organization._id, role: 'admin'})
    usrsAdmin.map(async usuarios => {
      try {
        var payload = {}
        payload._id = usuarios._id
        payload.name = usuarios.name
        payload.email = usuarios.email
        payload.pais = usuarios.pais
        payload.module = 'moduleContinuidad'
        payload.vigencia = 'vigContinuidad'
        payload.organization_id = usuarios.organization_id
        var token = jwt.encode(payload, jwtSecret)
        var link = ENDPOINT + '/buyPricing' + '?' + 'token=' + token
        // console.log('Info Token: ', link)
        // console.log('        ', usuarios.email)
        // arrayUsrs.push({linkButton: link, mail: usuarios.email})
        var transporter = nodemailer.createTransport({
          host: 'smtp.hostinger.com',
          port: 465,
          secure: true,
          auth: {
            user: 'non-reply@padcompliance.mx',
            pass: 'ck307S@J'
          }
        })
        // var codigohtml = "<p>La cuenta de correo :</p><br>"+body.correo+", ha sido bloqueada por pasar los 10 intentos de inicio de sesion. Continue en <a href="+body.link+">LINK</a> para iniciar el proceso de recuperación de contraseña"
        var codigohtml =
    `<p style="text-align:center;font-size:1.3em;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
    <strong style="color:#0f5dac;"><i>PAD Compliance </i></strong>
    </p>
    <img style="img-align:center;display:block;margin:auto;border-radius:50%;" width="50%" height="50%" src="https://pad1-auditoria.s3.us-east-2.amazonaws.com/compraModulo.png">
    <p style="text-align:center;font-size:1.3em; color:#285ea6;">
    <b>¡Licencia de PAD Compliance! </b>
    </p>
    <p style="text-align:center;color:#0f5dac;font-size:1em;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
    <strong>Gracias por probar el sistema PAD Compliance.</strong>
    </p>
    <p style="text-align:center;font-size:13px;color:#0f5dac;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
    <strong>Le informamos que su licencia del modulo CONTINUIDAD DE NEGOCIO COVID-19 esta a punto de expirar</strong></p>
    </p>
    <p style="text-align:center;font-size:13px;color:#0f5dac;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
    <strong>Lo invitamos a realizar la compra del módulo presionando el siguiente boton.</strong></p>
    </p>
    <a href='${link}' style="box-shadow: 0px 10px 14px -7px #276873;background-color:#285ea6;border-radius:8px;
    display:block;cursor:pointer;color:#ffffff;font-family:Helvetica;font-weight:bold;padding:13px 32px;text-decoration:none;text-shadow:0px 1px 0px #3d768a;text-align:center;
    margin: 0 auto;width:200px;">
    ¡Realizar compra!
    </a>
    <br><br>
    <p style="color: #285ea6;"> <br> 
    PRYVACY LAWYERS S.A.S. DE C.V <br>
    Concepción Mendez #49,
    Col. Atenor Salas, Benito Juarez,
    03010, Ciudad de México, CDMX. <br>
    Contacto: 55-45-00-68-74
    </p>`
        var mailOptions = {
          from: 'PAD <non-reply@padcompliance.mx>',
          to: usuarios.email,
          subject: 'Modulo continuidad de negocio COVID-19 próximo a expirar ',
          text: 'Su modulo de continuidad de negocio COVID-19 está proximo a expirar, haga click en el siguiente boton para realizar su compra',
          html: codigohtml
        }
        let resSend = await transporter.sendMail(mailOptions)
        let resultValidation = validationResponse(resSend, res)
        resultValidation ? res.json(resultValidation) : null
      } catch (ERR_HTTP_HEADERS_SENT) {
        // console.log('ERRORRRR: ')
      }
    })
  })
})

cron.schedule('0 5 24 6 *', async () => {
  let modulosOrg = await Organization.find({ 'premium.vigContinuidad': new Date('2020-06-30') })
  await modulosOrg.map(async organization => {
    // console.log('ORGANIZACION: ', organization.domain)
    let usrsAdmin = await User.find({organization_id: organization._id, role: 'admin'})
    usrsAdmin.map(async usuarios => {
      try {
        var payload = {}
        payload._id = usuarios._id
        payload.name = usuarios.name
        payload.email = usuarios.email
        payload.pais = usuarios.pais
        payload.module = 'moduleContinuidad'
        payload.vigencia = 'vigContinuidad'
        payload.organization_id = usuarios.organization_id
        var token = jwt.encode(payload, jwtSecret)
        var link = ENDPOINT + '/buyPricing' + '?' + 'token=' + token
        // console.log('Info Token: ', link)
        // console.log('        ', usuarios.email)
        // arrayUsrs.push({linkButton: link, mail: usuarios.email})
        var transporter = nodemailer.createTransport({
          host: 'smtp.hostinger.com',
          port: 465,
          secure: true,
          auth: {
            user: 'non-reply@padcompliance.mx',
            pass: 'ck307S@J'
          }
        })
        // var codigohtml = "<p>La cuenta de correo :</p><br>"+body.correo+", ha sido bloqueada por pasar los 10 intentos de inicio de sesion. Continue en <a href="+body.link+">LINK</a> para iniciar el proceso de recuperación de contraseña"
        var codigohtml =
    `<p style="text-align:center;font-size:1.3em;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
    <strong style="color:#0f5dac;"><i>PAD Compliance </i></strong>
    </p>
    <img style="img-align:center;display:block;margin:auto;border-radius:50%;" width="50%" height="50%" src="https://pad1-auditoria.s3.us-east-2.amazonaws.com/compraModulo.png">
    <p style="text-align:center;font-size:1.3em; color:#285ea6;">
    <b>¡Licencia de PAD Compliance! </b>
    </p>
    <p style="text-align:center;color:#0f5dac;font-size:1em;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
    <strong>Gracias por probar el sistema PAD Compliance.</strong>
    </p>
    <p style="text-align:center;font-size:13px;color:#0f5dac;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
    <strong>Le informamos que su licencia del modulo CONTINUIDAD DE NEGOCIO COVID-19 esta a punto de expirar</strong></p>
    </p>
    <p style="text-align:center;font-size:13px;color:#0f5dac;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
    <strong>Lo invitamos a realizar la compra del módulo presionando el siguiente boton.</strong></p>
    </p>
    <a href='${link}' style="box-shadow: 0px 10px 14px -7px #276873;background-color:#285ea6;border-radius:8px;
    display:block;cursor:pointer;color:#ffffff;font-family:Helvetica;font-weight:bold;padding:13px 32px;text-decoration:none;text-shadow:0px 1px 0px #3d768a;text-align:center;
    margin: 0 auto;width:200px;">
    ¡Realizar compra!
    </a>
    <br><br>
    <p style="color: #285ea6;"> <br> 
    PRYVACY LAWYERS S.A.S. DE C.V <br>
    Concepción Mendez #49,
    Col. Atenor Salas, Benito Juarez,
    03010, Ciudad de México, CDMX. <br>
    Contacto: 55-45-00-68-74
    </p>`
        var mailOptions = {
          from: 'PAD <non-reply@padcompliance.mx>',
          to: usuarios.email,
          subject: 'Modulo continuidad de negocio COVID-19 próximo a expirar ',
          text: 'Su modulo de continuidad de negocio COVID-19 está proximo a expirar, haga click en el siguiente boton para realizar su compra',
          html: codigohtml
        }
        let resSend = await transporter.sendMail(mailOptions)
        let resultValidation = validationResponse(resSend, res)
        resultValidation ? res.json(resultValidation) : null
      } catch (ERR_HTTP_HEADERS_SENT) {
        // console.log('ERRORRRR: ')
      }
    })
  })
})

cron.schedule('0 5 27 6 *', async () => {
  let modulosOrg = await Organization.find({ 'premium.vigContinuidad': new Date('2020-06-30') })
  await modulosOrg.map(async organization => {
    // console.log('ORGANIZACION: ', organization.domain)
    let usrsAdmin = await User.find({organization_id: organization._id, role: 'admin'})
    usrsAdmin.map(async usuarios => {
      try {
        var payload = {}
        payload._id = usuarios._id
        payload.name = usuarios.name
        payload.email = usuarios.email
        payload.pais = usuarios.pais
        payload.module = 'moduleContinuidad'
        payload.vigencia = 'vigContinuidad'
        payload.organization_id = usuarios.organization_id
        var token = jwt.encode(payload, jwtSecret)
        var link = ENDPOINT + '/buyPricing' + '?' + 'token=' + token
        // console.log('Info Token: ', link)
        // console.log('        ', usuarios.email)
        // arrayUsrs.push({linkButton: link, mail: usuarios.email})
        var transporter = nodemailer.createTransport({
          host: 'smtp.hostinger.com',
          port: 465,
          secure: true,
          auth: {
            user: 'non-reply@padcompliance.mx',
            pass: 'ck307S@J'
          }
        })
        // var codigohtml = "<p>La cuenta de correo :</p><br>"+body.correo+", ha sido bloqueada por pasar los 10 intentos de inicio de sesion. Continue en <a href="+body.link+">LINK</a> para iniciar el proceso de recuperación de contraseña"
        var codigohtml =
    `<p style="text-align:center;font-size:1.3em;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
    <strong style="color:#0f5dac;"><i>PAD Compliance </i></strong>
    </p>
    <img style="img-align:center;display:block;margin:auto;border-radius:50%;" width="50%" height="50%" src="https://pad1-auditoria.s3.us-east-2.amazonaws.com/compraModulo.png">
    <p style="text-align:center;font-size:1.3em; color:#285ea6;">
    <b>¡Licencia de PAD Compliance! </b>
    </p>
    <p style="text-align:center;color:#0f5dac;font-size:1em;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
    <strong>Gracias por probar el sistema PAD Compliance.</strong>
    </p>
    <p style="text-align:center;font-size:13px;color:#0f5dac;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
    <strong>Le informamos que su licencia del modulo CONTINUIDAD DE NEGOCIO COVID-19 esta a punto de expirar</strong></p>
    </p>
    <p style="text-align:center;font-size:13px;color:#0f5dac;font-family:'Merriweather', 'Helvetica Neue', Arial, sans-serif;">
    <strong>Lo invitamos a realizar la compra del módulo presionando el siguiente boton.</strong></p>
    </p>
    <a href='${link}' style="box-shadow: 0px 10px 14px -7px #276873;background-color:#285ea6;border-radius:8px;
    display:block;cursor:pointer;color:#ffffff;font-family:Helvetica;font-weight:bold;padding:13px 32px;text-decoration:none;text-shadow:0px 1px 0px #3d768a;text-align:center;
    margin: 0 auto;width:200px;">
    ¡Realizar compra!
    </a>
    <br><br>
    <p style="color: #285ea6;"> <br> 
    PRYVACY LAWYERS S.A.S. DE C.V <br>
    Concepción Mendez #49,
    Col. Atenor Salas, Benito Juarez,
    03010, Ciudad de México, CDMX. <br>
    Contacto: 55-45-00-68-74
    </p>`
        var mailOptions = {
          from: 'PAD <non-reply@padcompliance.mx>',
          to: usuarios.email,
          subject: 'Modulo continuidad de negocio COVID-19 próximo a expirar ',
          text: 'Su modulo de continuidad de negocio COVID-19 está proximo a expirar, haga click en el siguiente boton para realizar su compra',
          html: codigohtml
        }
        let resSend = await transporter.sendMail(mailOptions)
        let resultValidation = validationResponse(resSend, res)
        resultValidation ? res.json(resultValidation) : null
      } catch (ERR_HTTP_HEADERS_SENT) {
        // console.log('ERRORRRR: ')
      }
    })
  })
}) 
