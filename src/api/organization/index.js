import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { consultaModulo, pruebaToken, setPremium, updateAreas, create, index, show, showParams, showVars, update, updateParams,updateVars, destroy, showSectionsInfo, updateSectionsInfo, updateProfile } from './controller'
import { schema } from './model'
export Organization, { schema } from './model'

const router = new Router()

const { dominios, email, pass, organizationId, premium, value, authorizer, name, domain, numEmployments, income, url, zipcode, city, country, params, impactVars, probabilityVars, questions: { moduleId }, subTypeOf, questions } = schema.tree
export {premium} from './model'
const org_id = {type:String, default:"5c86c4b11928a90c059d4a95"}

/**
 * @api {post} /organizations Crear organización
 * @apiName CreateOrganization
 * @apiGroup Organization
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiParam {String} name Nombre de la organización.
 * @apiParam {String} numEmployments Numero de empleados de la organización.
 * @apiParam {String} income Ingresos de la organización.
 * @apiParam {String} url Url de la organización.
 * @apiParam {String} zipcode Código postal de la organización.
 * @apiParam params Parametros de la organización.
 * @apiSuccess {Object} organization Datos de la organización.
 * @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Organización no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.post('/',
  token({ required: true }),
  body({ name, domain, numEmployments, income, url, zipcode, params }),
  create)

/**
 * @api {get} /organizations Consultar organizaciones
 * @apiName RetrieveOrganizations
 * @apiGroup Organization
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiUse listParams
 * @apiSuccess {Number} count Cantidad total de organizaciones.
 * @apiSuccess {Object[]} rows Lista de organizaciones.
 * @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 401 Solo acceso a usuarios.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /organizations/:id Consultar organización
 * @apiName RetrieveOrganization
 * @apiGroup Organization
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiSuccess {Object} organization Datos de la organización.
 * @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Organización no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {get} /organizations/domainurl Organization's URL and Domain
 * @apiName RetrieveOrganizationInfo
 * @apiGroup Organization
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiSuccess {Object} organization Datos de la organización.
 * @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 404 domainurl no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.put('/profile/:id',
  token({ required: true }),
  body({ name, url, zipcode, city, country }),
  updateProfile)

/*
 * @api {get} /organizations/my/params Regresa los parametros de la organización
 * @apiName RetrieveOrganizationParams
 * @apiGroup Organization
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiSuccess {Object} organization Datos de la organización.
 * @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Organización no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.get('/my/params',
  query({ moduleId }),
  token({ required: true }),
  showParams)

/*
 * @api {get} /my/vars Regresa las variables de la organización
 * @apiName RetrieveOrganizationVars
 * @apiGroup Organization
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiSuccess {Object} organization Datos de la organización.
 * @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Organización no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.get('/my/vars',
  token({ required: true }),
  showVars)

/**
 * @api {get} /my/questionsInfo Regresa la información de los secciones que tiene cada organización dependiendo del moduleId
 * @apiName RetrieveSectionsInfo
 * @apiGroup Organization
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiParam {String} org_id Id de la Organization
 * @apiSuccess {Object} organization Datos de la organización.
 * @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Organización no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.get('/my/questionsInfo',
token({ required: true }),
query({
  moduleId,
  subTypeOf
}),
showSectionsInfo)


router.put('/update/questionsInfo/:id',
  token({ required: true }),
  body({ questions }),
  updateSectionsInfo)

/**
 * @api {put} /organizations/:organizationId/params Actualizar parametros de la oprganización del usuario
 * @apiName UpdateOrganizationParams
 * @apiGroup Organization
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiParam params Objeto parametros de la organización.
 * @apiSuccess {Object} organization Datos de la organización.
 * @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Organización no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */

router.put('/my/params',
  token({ required: true }),
  body({ params }),
  updateParams)

//TODO: Cambio el modelo de impactVars y probabilityVars.
/**
 * @api {put} /my/vars Actualizar variables de parametros
 * @apiName UpdateOrganizationVars
 * @apiGroup Organization
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuarios.
 * @apiParam params Objeto parametros de la organización.
 * @apiSuccess {Object} organization Datos de la organización.
 * @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Organización no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */

router.put('/my/vars/:type',
  token({ required: true }),
  body({ impactVars, probabilityVars }),
  updateVars)

/**
 * @api {put} /organizations/:id Actualizar organización
 * @apiName UpdateOrganization
 * @apiGroup Organization
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuarios.
 * @apiParam {String} name Nombre  de la organización.
 * @apiParam {String} numEmployments Numero de empleados de la organización.
 * @apiParam {String} income Ingresos de la organización.
 * @apiParam {String} url Url de la organización.
 * @apiParam {String} zipcode Código postal de la organización.
 * @apiParam params PArametros de la organización.
 * @apiSuccess {Object} organization Datos de la organización.
 * @apiError {Object} 400 Algunos parametros pueden contener valores invalidos.
 * @apiError 404 Organización no se encontró.
 * @apiError 401 Solo acceso a usuarios.
 */
router.put('/:id',
  token({ required: true }),
  body({ name, numEmployments, income, url, zipcode, params }),
  update)

router.put('/update/areas',
  token({ required: true }),
  body({ value, authorizer }),
  updateAreas)

router.put('/update/premium',
  token({ required: true }),
  body({ premium }),
  setPremium)

/**
 * @api {delete} /organizations/:id Eliminar organización
 * @apiName DeleteOrganization
 * @apiGroup Organization
 * @apiPermission user
 * @apiParam {String} access_token Token de acceso de usuario.
 * @apiSuccess (Success 204) 204 Sin contenido.
 * @apiError 404 Oraganización no encontrada.
 * @apiError 401 Solo acceso a administradores.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

router.get('/get/module',
  query({ token }),
  consultaModulo)

router.get('/prueba/token',
  body({ organizationId, email, pass, dominios: [] }),
  pruebaToken)

export default router
