import { Router } from 'express'
import { destroy } from './controller'
import { middleware as body } from 'bodymen'
import { schema as orgSchema } from '../organization'
const router = new Router()

/**
 * @api {delete} /deleteDomains/:id Delete delete domain
 * @apiName DeleteDeleteDomain
 * @apiGroup DeleteDomain
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Delete domain not found.
 */
// router.delete('/:domain',
// destroy)

router.delete('/',
  body({ save: [] }),
  destroy)
export default router
