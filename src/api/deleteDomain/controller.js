import { success } from '../../services/response/'
import { Organization } from '../organization'
import { User } from '../user'
import { Subrisk } from '../subrisk'
import { Question } from '../question'
import { Task } from '../task'
import { Folder } from '../folder'
import { File } from '../file'

export const destroy = ({ bodymen: { body } }, res, next) => {
  Organization.find({ domain: body.save }, {_id: 1})
    .then(async (orgId) => {
      /* for (var i = 0; i < orgId.lenght; i++) {
        console.log(orgId[ i ])
      } */

      try {
        let delSub = await Subrisk.bulkWrite([
          {deleteMany: {filter: {organization_id: {$nin: orgId}}}}
        ])

        let delUsr = await User.bulkWrite([
          {deleteMany: {filter: {organization_id: {$nin: orgId}}}}
        ])
        let delOrg = await Organization.bulkWrite([
          {deleteMany: {filter: {_id: {$nin: orgId}}}}
        ])

        let delQas = await Question.bulkWrite([
          {deleteMany: {filter: {organization_id: {$nin: orgId}}}}
        ])
        let delTask = await Task.bulkWrite([
          {deleteMany: {filter: {organization_id: {$nin: orgId}}}}
        ])
        let delFolder = await Folder.bulkWrite([
          {deleteMany: {filter: {organization_id: {$nin: orgId}}}}
        ])
        let delFile = await File.bulkWrite([
          {deleteMany: {filter: {organization_id: {$nin: orgId}}}}
        ])
        res.status(204).json({
        })
      } catch (error) {
        console.log(error)
      }
    })
}
