export const dotNotate = (obj, target, prefix) => {
  target = target || {},
prefix = prefix || ''
  Object.keys(obj).forEach(function (key) {
    if (typeof (obj[key]) === 'object') {
      dotNotate(obj[key], target, prefix + key + '.')
    } else {
      if (typeof obj[key] !== "undefined" ) {
        return target[prefix + key] = obj[key]
      }
    }
  })
  return target
}
