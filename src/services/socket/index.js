import socket from 'socket.io'

let io = null

export const init_socket = (server) =>{

    io =  socket(server)

    io.on("connection", function(socket){
        console.log("-Socket Connection Established with ID :"+ socket.id)
        socket.on("notificacion", async function(orgId){
            // chat.created = new Date()
            // let response = await new message(chat).save()
            io.emit("org"+orgId)
        }),
        socket.on('riskTableEvent', (data, orgId, action) => {
            socket.broadcast.emit('riskTable'+orgId, data, action);
        });
        socket.on('gestorFolder', (data, orgId, action) => {
            socket.broadcast.emit('gestorFolder'+orgId, data, action);
        });
        socket.on('gestorFile', (data, orgId, action) => {
            socket.broadcast.emit('gestorFile'+orgId, data, action);
        });
        socket.on('memberRoles', (data, orgId, action) => {
            socket.broadcast.emit('memberRoles'+orgId, data, action);
        })
        socket.on('ganttEvent', (data, action, orgId, idTask) => {
            socket.broadcast.emit('gantt'+orgId, data, action, idTask);
        })
    })
}

export { io } 