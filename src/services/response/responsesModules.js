export const validationError = (err, res) => {
  var errV
  switch (err.name) {
    case 'CastError':
      errV = {
        result: {
          valid: false,
          name: err.name,
          message: err.message
        },
        status: 409
      }
      break
    case 'ValidationError':
      errV = {
        result: {
          valid: false,
          name: err.name,
          message: err.message
        },
        status: 422
      }
      break
    default:
      errV = {
        result: {
          valid: false,
          name: err.name,
          message: err.message
        },
        status: 500
      }
      break
  }
  res.status(errV.status).json(errV.result).end()
  return null
}

export const validationResponse = (response, res) => {
  var  objRes
  switch (response) {
    case null:
      objRes = {
        result: {
          valid: false,
          name: 'Not Found',
          message: 'No data for this request'
        },
        status: 404
      }
      break
    case 0:
      objRes = {
        result: {
          valid: false,
          name: 'Not Found',
          message: 'No data for this request'
        },
        status: 404
      }
      break
    default:
      objRes = {
        status: 200
      }
      break
  }

  if (objRes.status < 299 ){
    res.status(objRes.status)
    return response
  } else {
    res.status(objRes.status).json(objRes.result).end()
    return null
  }

  
}
