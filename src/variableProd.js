const IS_PROD = process.env.NODE_ENV === 'production'
const IS_QA = process.env.IS_QA === 'true'
console.log('--', process.env.IS_QA, '--')
// const IS_QA = true
let URL_FRONT_DASH
let URL_FRONT_CLIENT
let URL_BACK
if (IS_PROD) {
  URL_FRONT_DASH = IS_QA ? 'https://pad-dashboard.herokuapp.com/#' : 'https://data.padcompliance.mx/#'
  URL_FRONT_CLIENT = IS_QA ? 'https://pad-main.herokuapp.com' : 'https://padcompliance.mx'
  URL_BACK = IS_QA ? 'https://pad-server.herokuapp.com' : 'https://padserver.herokuapp.com'
} else {
  URL_FRONT_DASH = 'http://localhost:8080/#'
  URL_FRONT_CLIENT = 'http://localhost:8081'
  URL_BACK = 'http://localhost:8087'
}
export {URL_FRONT_DASH}
export {URL_FRONT_CLIENT}
export {URL_BACK}
