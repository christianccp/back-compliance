# pad-back v0.0.0



- [Auth](#auth)
	- [Authenticate](#authenticate)
	
- [Blog](#blog)
	- [Create blog](#create-blog)
	- [Delete blog](#delete-blog)
	- [Retrieve blog](#retrieve-blog)
	- [Retrieve blogs](#retrieve-blogs)
	- [Update blog](#update-blog)
	
- [Controls](#controls)
	- [Actualizar control](#actualizar-control)
	- [Agregar control](#agregar-control)
	- [Consultar control por Id](#consultar-control-por-id)
	- [Consultar controles](#consultar-controles)
	- [Eliminar control](#eliminar-control)
	
- [DeleteDomain](#deletedomain)
	- [Delete delete domain](#delete-delete-domain)
	
- [News](#news)
	- [Create news](#create-news)
	- [Delete news](#delete-news)
	- [Retrieve news](#retrieve-news)
	- [Update news](#update-news)
	
- [Notifications](#notifications)
	- [Create notifications](#create-notifications)
	- [Delete notifications](#delete-notifications)
	- [Retrieve notifications](#retrieve-notifications)
	- [Update notifications](#update-notifications)
	
- [Organization](#organization)
	- [Crear organización](#crear-organización)
	- [Eliminar organización](#eliminar-organización)
	- [Consultar organización](#consultar-organización)
	- [Organization&#39;s URL and Domain](#organization&#39;s-url-and-domain)
	- [Consultar organizaciones](#consultar-organizaciones)
	- [Regresa la información de los secciones que tiene cada organización dependiendo del moduleId](#regresa-la-información-de-los-secciones-que-tiene-cada-organización-dependiendo-del-moduleid)
	- [Actualizar organización](#actualizar-organización)
	- [Actualizar parametros de la oprganización del usuario](#actualizar-parametros-de-la-oprganización-del-usuario)
	- [Actualizar variables de parametros](#actualizar-variables-de-parametros)
	
- [Questions](#questions)
	- [Crear pregunta](#crear-pregunta)
	- [Eliminar pregunta](#eliminar-pregunta)
	- [Consultar preguntas por organizacion y modulo](#consultar-preguntas-por-organizacion-y-modulo)
	- [Consultar todas las preguntas](#consultar-todas-las-preguntas)
	- [Guardar Respuestas](#guardar-respuestas)
	- [Actualizar pregunta](#actualizar-pregunta)
	
- [Subrisk](#subrisk)
	- [Agregar subriesgo a una organización](#agregar-subriesgo-a-una-organización)
	- [Crear subriesgo](#crear-subriesgo)
	- [Eliminar Subrisk](#eliminar-subrisk)
	- [Consultar parametros de subriesgo](#consultar-parametros-de-subriesgo)
	- [Consultar subriesgo](#consultar-subriesgo)
	- [Consultar subriesgos](#consultar-subriesgos)
	- [Actualizar valor proceder de los subriesgos. Los subriesgos deben de ser enviados dentro de un arreglo.](#actualizar-valor-proceder-de-los-subriesgos.-los-subriesgos-deben-de-ser-enviados-dentro-de-un-arreglo.)
	- [Actualizar valor residual de los subriesgos. Los subriesgos deben de ser enviados dentro de un arreglo.](#actualizar-valor-residual-de-los-subriesgos.-los-subriesgos-deben-de-ser-enviados-dentro-de-un-arreglo.)
	- [Update subrisk params](#update-subrisk-params)
	- [Actualizar subriesgo por OrgId](#actualizar-subriesgo-por-orgid)
	- [Actualizar subriesgo](#actualizar-subriesgo)
	- [Actualizar parámetros de subriesgo](#actualizar-parámetros-de-subriesgo)
	
- [Task](#task)
	- [Update Data task](#update-data-task)
	- [Update Data task](#update-data-task)
	- [Delete task](#delete-task)
	- [Update Data task](#update-data-task)
	- [Update Data task](#update-data-task)
	- [Retrieve task](#retrieve-task)
	- [Update task](#update-task)
	- [Update Data task](#update-data-task)
	- [Update Data task](#update-data-task)
	
- [User](#user)
	- [Crear usuario](#crear-usuario)
	- [Eliminar usuario](#eliminar-usuario)
	- [Consultar usuario actual.](#consultar-usuario-actual.)
	- [Consultar usuario](#consultar-usuario)
	- [Consultar usuarios](#consultar-usuarios)
	- [Actualizar contraseña](#actualizar-contraseña)
	- [Actualizar usuario](#actualizar-usuario)
	
- [_Users_alejandroxochihua_Developer_DevPages_PAD_server_src_api_task_index_js](#_users_alejandroxochihua_developer_devpages_pad_server_src_api_task_index_js)
	- [Para documentar](#para-documentar)
	


# Auth

## Authenticate



	POST /auth

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|

# Blog

## Create blog



	POST /blogs


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| title			| 			|  <p>Blog's title.</p>							|
| desc			| 			|  <p>Blog's desc.</p>							|

## Delete blog



	DELETE /blogs/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Retrieve blog



	GET /blogs/:id


## Retrieve blogs



	GET /blogs


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update blog



	PUT /blogs/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|
| title			| 			|  <p>Blog's title.</p>							|
| desc			| 			|  <p>Blog's desc.</p>							|

# Controls

## Actualizar control



	PUT /controls/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|
| section			| String			|  <p>Sección de control.</p>							|
| title			| String			|  <p>Títutlo del control.</p>							|
| organizationId			| String			|  <p>Nombre de la organización.</p>							|
| questions			| Object[]			|  <p>Preguntas de control. Ver Object Preguntas para mas detalles.</p>							|

## Agregar control



	POST /controls


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de accesso de usuario.</p>							|
| section			| String			|  <p>Sección del control.</p>							|
| title			| String			|  <p>Títutlo del control.</p>							|
| organizationId			| String			|  <p>Nombre de la organizacion.</p>							|
| questions			| Object[]			|  <p>Preguntas de control. Ver Object Preguntas para mas detalles.</p>							|

## Consultar control por Id



	GET /controls/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|

## Consultar controles



	GET /controls


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Eliminar control



	DELETE /controls/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|

# DeleteDomain

## Delete delete domain



	DELETE /deleteDomains/:id


# News

## Create news



	POST /news


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| title			| 			|  <p>News's title.</p>							|
| content			| 			|  <p>News's content.</p>							|
| image			| 			|  <p>News's image.</p>							|

## Delete news



	DELETE /news/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve news



	GET /news


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update news



	PUT /news/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| title			| 			|  <p>News's title.</p>							|
| content			| 			|  <p>News's content.</p>							|
| image			| 			|  <p>News's image.</p>							|

# Notifications

## Create notifications



	POST /notifications


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Delete notifications



	DELETE /notifications/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve notifications



	GET /notifications


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update notifications



	PUT /notifications/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

# Organization

## Crear organización



	POST /organizations


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|
| name			| String			|  <p>Nombre de la organización.</p>							|
| numEmployments			| String			|  <p>Numero de empleados de la organización.</p>							|
| income			| String			|  <p>Ingresos de la organización.</p>							|
| url			| String			|  <p>Url de la organización.</p>							|
| zipcode			| String			|  <p>Código postal de la organización.</p>							|
| params			| 			|  <p>Parametros de la organización.</p>							|

## Eliminar organización



	DELETE /organizations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|

## Consultar organización



	GET /organizations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|

## Organization&#39;s URL and Domain



	GET /organizations/domainurl


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|

## Consultar organizaciones



	GET /organizations


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Regresa la información de los secciones que tiene cada organización dependiendo del moduleId



	GET /my/questionsInfo


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|
| org_id			| String			|  <p>Id de la Organization</p>							|

## Actualizar organización



	PUT /organizations/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuarios.</p>							|
| name			| String			|  <p>Nombre  de la organización.</p>							|
| numEmployments			| String			|  <p>Numero de empleados de la organización.</p>							|
| income			| String			|  <p>Ingresos de la organización.</p>							|
| url			| String			|  <p>Url de la organización.</p>							|
| zipcode			| String			|  <p>Código postal de la organización.</p>							|
| params			| 			|  <p>PArametros de la organización.</p>							|

## Actualizar parametros de la oprganización del usuario



	PUT /organizations/:organizationId/params


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|
| params			| 			|  <p>Objeto parametros de la organización.</p>							|

## Actualizar variables de parametros



	PUT /my/vars


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuarios.</p>							|
| params			| 			|  <p>Objeto parametros de la organización.</p>							|

# Questions

## Crear pregunta



	POST /questions


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de accesso de usuario.</p>							|
| title			| String			|  <p>Título de la pregunta.</p>							|
| weight			| Number			|  <p>Peso de la pregunta.</p>							|
| result			| Number			|  <p>Resultado de la pregunta. Aplica para typeOf distintos de 'questionnaire'.</p>							|
| results			| Object[]			|  <p>Results. Ver Object Results para mas detalles.</p>							|

## Eliminar pregunta



	DELETE /questions/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de accesso de usuario.</p>							|

## Consultar preguntas por organizacion y modulo



	GET /questions/filter/:organizationId/:module


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de accesso de usuario.</p>							|

## Consultar todas las preguntas



	GET /questions


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de administrador.</p>							|
| moduleId			| String			|  <p>Modulo a consultar (anticorruption,...)</p>							|
| typeOf			| String			| **optional** <p>Id de cuestionario a consultar (questionnaire1, section1, ...).</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Guardar Respuestas



	PUT /questions/set/results


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de accesso de usuario.</p>							|
| Titulo			| String			|  <p>de pregunta</p>							|
| results			| Object[]			|  <p>Resultados. Ver Object Results para mas detalles.</p>							|

## Actualizar pregunta



	PUT /questions/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de accesso de usuario.</p>							|
| title			| String			|  <p>Título de la pregunta.</p>							|
| weight			| Number			|  <p>Peso de la pregunta.</p>							|
| result			| Number			|  <p>Resultado de la pregunta. Aplica para typeOf distintos de 'questionnaire'.</p>							|
| results			| Object[]			|  <p>Resultados. Ver Object Results para mas detalles.</p>							|

# Subrisk

## Agregar subriesgo a una organización



	PUT /subrisk/:organizationId/addsubrisk


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| subrisk			| Object			|  <p>Subrisk object data. See Subrisk Object for more details below.</p>							|

## Crear subriesgo



	POST /subrisk


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|
| title			| String			|  <p>Titulo de subriesgo.</p>							|
| active			| Boolean			|  <p>Activo.</p>							|
| sanction			| String			|  <p>Sanción.</p>							|
| description			| String			|  <p>Descripción del subriesgo.</p>							|
| moduleId			| String			|  <p>Módulo del subriesgo.</p>							|
| impactAvg			| Object			|  <p>Ver Impact Object para mas detalles.</p>							|
| probabilityAvg			| Object			|  <p>Ver Probability Object para mas detalles.</p>							|
| levelAvg			| Object			|  <p>Ver Level Object para mas detalles.</p>							|
| levelResidualAvg			| Object			|  <p>Ver Level Residual Object para mas detalles.</p>							|
| params			| Object[]			|  <p>Ver Params Object para mas detalles.</p>							|

## Eliminar Subrisk



	DELETE /subrisk/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|

## Consultar parametros de subriesgo



	GET /subrisk/params/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|

## Consultar subriesgo



	GET /risk/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|

## Consultar subriesgos



	GET /subrisk


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Actualizar valor proceder de los subriesgos. Los subriesgos deben de ser enviados dentro de un arreglo.



	PUT /subrisk/set/proceder


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de accesso de usuario.</p>							|
| subrisks			| Object[]			|  <p>Subriesgos a actualizar. Ver Object Subrisks para mas detalles.</p>							|

## Actualizar valor residual de los subriesgos. Los subriesgos deben de ser enviados dentro de un arreglo.



	PUT /subrisk/set/residualLevel


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de accesso de usuario.</p>							|
| subrisks			| Object[]			|  <p>Subriesgos a actualizar. Ver Object Subrisks para mas detalles.</p>							|

## Update subrisk params



	PUT /risk/:subrisk/subrisk


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| subrisk			| Object[]			|  <p>Risk's subrisk array</p>							|

## Actualizar subriesgo por OrgId



	PUT /risk/:organizationId/subrisks


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|
| subrisk			| 			|  <p>Risk's subrisk array</p>							|

## Actualizar subriesgo



	PUT /subrisk/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|
| title			| String			|  <p>Titulo de subriesgo.</p>							|
| active			| Boolean			|  <p>Activo.</p>							|
| sanction			| String			|  <p>Sanción.</p>							|
| description			| String			|  <p>Descripción del subriesgo.</p>							|
| moduleId			| String			|  <p>Módulo del subriesgo.</p>							|
| impactAvg			| Object			|  <p>Ver Impact Object para mas detalles.</p>							|
| probabilityAvg			| Object			|  <p>Ver Probability Object para mas detalles.</p>							|
| levelAvg			| Object			|  <p>Ver Level Object para mas detalles.</p>							|
| levelResidualAvg			| Object			|  <p>Ver Level Residual Object para mas detalles.</p>							|
| params			| Object[]			|  <p>Ver Params Object para mas detalles.</p>							|

## Actualizar parámetros de subriesgo



	PUT /subrisk/:subrisk/params


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|
| levelInhAvg			| Object			|  <p>Ver objecto Level Object para mas detalles.</p>							|
| impactAvg			| Object			|  <p>Ver objecto Impact Object para mas detalles.</p>							|
| probabilityAvg			| Object			|  <p>Ver Probability Object para mas detalles.</p>							|
| params			| Object[]			|  <p>Ver objecto Params Object para mas detalles.</p>							|

# Task

## Update Data task



	PUT /tasks/:id/addData


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| text			| 			|  <p>Task's text.</p>							|
| start_date			| 			|  <p>Task's start_date.</p>							|
| end_date			| 			|  <p>Task's end_date.</p>							|
| progress			| 			|  <p>Task's progress.</p>							|
| assigned			| 			|  <p>Task's assigned.</p>							|
| comments			| 			|  <p>Task's comments.</p>							|
| color			| 			|  <p>Task's color.</p>							|
| organization_id			| 			|  <p>Task's organization_id.</p>							|
| moduleId			| 			|  <p>Task's moduleId.</p>							|

## Update Data task



	PUT /tasks/:id/addLinks


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| source			| 			|  <p>Task source</p>							|
| target			| 			|  <p>Task target</p>							|
| type			| 			|  <p>Type.</p>							|

## Delete task



	DELETE /tasks/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Update Data task



	PUT /tasks/:id/deleteData


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Update Data task



	PUT /tasks/:id/deleteLinks


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve task



	GET /tasks/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Update task



	PUT /tasks/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| text			| 			|  <p>Task's text.</p>							|
| start_date			| 			|  <p>Task's start_date.</p>							|
| end_date			| 			|  <p>Task's end_date.</p>							|
| progress			| 			|  <p>Task's progress.</p>							|
| assigned			| 			|  <p>Task's assigned.</p>							|
| comments			| 			|  <p>Task's comments.</p>							|
| color			| 			|  <p>Task's color.</p>							|
| organization_id			| 			|  <p>Task's organization_id.</p>							|
| moduleId			| 			|  <p>Task's moduleId.</p>							|

## Update Data task



	PUT /tasks/:id/updateData


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| text			| 			|  <p>Task's text.</p>							|
| start_date			| 			|  <p>Task's start_date.</p>							|
| end_date			| 			|  <p>Task's end_date.</p>							|
| progress			| 			|  <p>Task's progress.</p>							|
| assigned			| 			|  <p>Task's assigned.</p>							|
| comments			| 			|  <p>Task's comments.</p>							|
| color			| 			|  <p>Task's color.</p>							|
| organization_id			| 			|  <p>Task's organization_id.</p>							|
| moduleId			| 			|  <p>Task's moduleId.</p>							|

## Update Data task



	PUT /tasks/:id/updateLinks


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| source			| 			|  <p>Task source</p>							|
| target			| 			|  <p>Task target</p>							|
| type			| 			|  <p>Type.</p>							|

# User

## Crear usuario



	POST /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access token.</p>							|
| email			| String			|  <p>E-mail de usuario.</p>							|
| password			| String			|  <p>Contraseña de usuario.</p>							|
| name			| String			| **optional** <p>Nombre de usuario.</p>							|
| phone			| String			|  <p>Teléfono de usuario.</p>							|
| mobile			| String			|  <p>Celular de usuario.</p>							|
| organization			| Object			|  <p>Ver Organization Object para mas detalles.</p>							|
| picture			| String			| **optional** <p>Imagen de usuario.</p>							|
| role			| String			| **optional** <p>Rol de usuario.</p>							|

## Eliminar usuario



	DELETE /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de administrador.</p>							|

## Consultar usuario actual.



	GET /users/me


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|

## Consultar usuario



	GET /users/:id


## Consultar usuarios



	GET /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de administrador.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Actualizar contraseña



	PUT /users/:id/password

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Autorización basica con correo y contraseña.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| password			| String			|  <p>Nueva contraseña de usuario.</p>							|

## Actualizar usuario



	PUT /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Token de acceso de usuario.</p>							|
| name			| String			|  <p>Nombre de usuario.</p>							|
| phone			| String			|  <p>Teléfono de usuario.</p>							|
| mobile			| String			|  <p>Celular de usuario.</p>							|
| picture			| String			| **optional** <p>Imagen de usuario.</p>							|

# _Users_alejandroxochihua_Developer_DevPages_PAD_server_src_api_task_index_js

## Para documentar



	GET /TODO



